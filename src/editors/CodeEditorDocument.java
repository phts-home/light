package editors;

import javax.swing.Icon;


/**
 * Interface for <code>CodeEditor</code>'s document.
 * 
 * @author Phil Tsarik
 * @author Sergey Tuchina
 *
 */
public interface CodeEditorDocument {
	
	/**
	 * Executes this document.
	 */
	public void run();
	
	/**
	 * Compiles this document.
	 */
	public void compile();
	
	/**
	 * Compiles and executes this document.
	 */
	public void compileAndRun();
	
	/**
	 * Tests whether this document can be executed.
	 * 
	 * @return <code>true</code> if this document can run; <code>false</code> otherwise
	 */
	public boolean isCanRun();
	
	/**
	 * Tests whether this document can be compiled.
	 * 
	 * @return <code>true</code> if this document can be compiled; <code>false</code> otherwise
	 */
	public boolean isCanCompile();
	
	/**
	 * Tests whether this document can be compiles and executed.
	 * 
	 * @return <code>true</code> if this document can be compiled and executed; <code>false</code> otherwise
	 */
	public boolean isCanCompileAndRun();
	
	/**
	 * Gets document type.
	 * 
	 * @return The document type
	 */
	public String getType();
	
	/**
	 * Gets the first file extension of this document type.
	 * 
	 * @return The first file extension of this document type or "" if no extensions is associated
	 */
	public String getFirstExtension();
	
	/**
	 * Sets the document type.
	 * 
	 * @param type The document type
	 */
	public void setType(String type);
	
	/**
	 * Sets the document type by specified filename extension.
	 * 
	 * @param extension Filename extension
	 */
	public void setTypeByExtension(String extension);
	
	/**
	 * Gets document icon.
	 * 
	 * @return The document icon
	 */
	public Icon getIcon();
	
}
