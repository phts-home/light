package editors;

import javax.swing.*;
import javax.swing.undo.*;
import javax.swing.text.*;
import javax.swing.plaf.*;
import javax.swing.event.*;

import editors.event.*;
import editors.highlight.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;


/**
 * The text component witch can be set a document as <code>CodeEditorDocument</code>
 * and text highlighter as <code>CodeEditorHighlighter</code> by methods 
 * <code>setCodeEditorDocument</code> and <code>setCodeEditorHighlighter</code>
 * respectively.<br>
 * This component extends the <code>AdvancedTextPane</code> class and so it inherits
 * all <code>AdvancedTextPane</code>'s methods, specifically you can place 
 * <code>CodeEditor</code> to the <code>LineNumbersPane</code>
 * 
 * @see panes.LineNumbersPane
 * 
 * @author Phil Tsarik
 * @author Sergey Tuchina
 * @version 0.9.1
 * 
 */
public class CodeEditor extends AdvancedTextPane {
	
	/**
	 * Constructs a new instance of the <code>CodeEditor</code> class.
	 */
	public CodeEditor() {
		super();
		this.addKeyListener(new CodeEditorKeyListener());
		
		codeEditorListenerList = new EventListenerList();
		
		// init some variables
		changed = false;
		autoDetectType = true;
		codeEditorDocument = null;
		documentHighlighter = null;
		popupMenuHandler = new PopupMenuHandler();
		
		// add document listener
		codeEditorDocumentEventsHandler = new CodeEditorDocumentEventsHandler();
		this.getDocument().addDocumentListener(codeEditorDocumentEventsHandler);
		
		// make editor undoable
		undoManager = new CodeEditorUndoManager();
		this.getDocument().addUndoableEditListener(undoManager);
		
		setReadOnly(false);
		setInsertSpaces(false);
	}
	
	/**
	 * Gets <code>CodeEditor<code>'s document.
	 * 
	 * @return <code>CodeEditor<code>'s document
	 */
	public CodeEditorDocument getCodeEditorDocument() {
		return codeEditorDocument;
	}
	
	/**
	 * Sets <code>CodeEditor<code>'s document.
	 * 
	 * @param codeEditorDocument <code>CodeEditor<code>'s document
	 */
	public void setCodeEditorDocument(CodeEditorDocument codeEditorDocument) {
		this.codeEditorDocument = codeEditorDocument;
	}
	
	/**
	 * Gets <code>CodeEditor<code>'s highlighter.
	 * 
	 * @return <code>CodeEditor<code>'s highlighter
	 */
	public DocumentHighlighter getCodeEditorHighlighter() {
		return documentHighlighter;
	}
	
	/**
	 * Sets <code>CodeEditor<code>'s highlighter.
	 * 
	 * @param documentHighlighter <code>CodeEditor<code>'s highlighter
	 */
	public void setCodeEditorHighlighter(DocumentHighlighter documentHighlighter) {
		this.documentHighlighter = documentHighlighter;
		if (documentHighlighter == null) {
			getDocument().removeDocumentListener(highlightDocumentListener);
		} else {
			getDocument().removeDocumentListener(highlightDocumentListener);
			highlightDocumentListener = new HighlightDocumentListener(this, documentHighlighter);
			getDocument().addDocumentListener(highlightDocumentListener);
		}
	}
	
	/**
	 * Updates <code>CodeEditor<code>'s highlighter.
	 */
	public void updateCodeEditorHighlighter() {
		if (documentHighlighter != null) {
			if (highlightDocumentListener != null) {
				getDocument().removeDocumentListener(highlightDocumentListener);
				getDocument().addDocumentListener(highlightDocumentListener);
				highlightDocumentListener.update();
			}
		}
	}
	
	/**
	 * Tests whether insert spaces except tabs is selected.
	 * 
	 * @return <code>true</code> if insert spaces except tabs is selected; <code>false</code> otherwise (keep tabs)
	 */
	public boolean isInsertSpaces() {
		return insertSpaces;
	}
	
	/**
	 * Sets <code>insertSpaces</code> property, which determines whether to insert spaces except tabs or not.
	 * 
	 * @param insertSpaces <code>true</code> to specify to insert spaces except tabs; <code>false</code> to specify to keep tabs
	 */
	public void setInsertSpaces(boolean insertSpaces) {
		this.insertSpaces = insertSpaces;
	}
	
	/**
	 * Tests whether the text of the instance of the <code>CodeEditor</code> is changed.
	 * 
	 * @return <code>true</code> if the text is changed; <code>false</code> otherwise
	 */
	public boolean isChanged() {
		return changed;
	}
	
	/**
	 * Tests whether the type of the document is auto detected when open or write file.
	 * 
	 * @return <code>true</code> if auto detection is enabled; <code>false</code> otherwise
	 */
	public boolean isAutoDetectType() {
		return autoDetectType;
	}
	
	/**
	 * Sets <code>autoDetectType</code> property, which determines whether the type of the document is auto detected when open or write file.
	 * 
	 * @param autoDetectType <code>true</code> to specify that the type of the document is auto detected; <code>false</code> to specify that the type of the document is not auto detected
	 */
	public void setAutoDetectType(boolean autoDetectType) {
		this.autoDetectType = autoDetectType;
	}
	
	/**
	 * Tests whether the document is read only.
	 * 
	 * @return <code>true</code> if the document is read only; <code>false</code> otherwise
	 */
	public boolean isReadOnly() {
		return readOnly;
	}
	
	/**
	 * Sets <code>readOnly</code> property, which determines whether the document is read only.
	 * 
	 * @param readOnly <code>true</code> to specify that the document is read only; <code>false</code> to specify that it is not read only
	 */
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
		setEditable(!readOnly);
	}
	
	/**
	 * Returns true if this <code>CodeEditor</code> may be undone.
	 * 
	 * @return <code>true</code> if this <code>CodeEditor</code> may be undone, <code>false</code> otherwise
	 */
	public boolean canUndo() {
		return undoManager.canUndo();
	}
	
	/**
	 * Returns true if this <code>CodeEditor</code> may be redone.
	 * 
	 * @return <code>true</code> if this <code>CodeEditor</code> may be redone, <code>false</code> otherwise
	 */
	public boolean canRedo() {
		return undoManager.canRedo();
	}
	
	/**
	 * Undoes made changes if it's possible.
	 */
	public void undo() {
		if (undoManager.canUndo()) {
			undoManager.undo();
		}
	}
	
	/**
	 * Redoes made changes if it's possible.
	 */
	public void redo() {
		if (undoManager.canRedo()) {
			undoManager.redo();
		}
	}
	
	/**
	 * Creates a document of the specified type.
	 * 
	 * @param type Type of a new document
	 */
	public void newDocument(String type) {
		this.setText("");
		changed = false;
		if (codeEditorDocument != null) {
			codeEditorDocument.setType(type);
		}
		updateCodeEditorHighlighter();
		undoManager.discardAllEdits();
	}
	
	/**
	 * Reads data from the specified file.
	 *  
	 * @param f Source file
	 * @return <code>true</code> if it was a success to read the file; <code>false</code> otherwise
	 */
	public boolean readFromFile(File f) {
		boolean read = true;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(f.getAbsolutePath()));
			this.read(reader, null);
			reader.close();
			this.getDocument().removeDocumentListener(lineNumbersPaneHandler);
			this.getDocument().addDocumentListener(lineNumbersPaneHandler);
			this.getDocument().removeDocumentListener(codeEditorDocumentEventsHandler);
			this.getDocument().addDocumentListener(codeEditorDocumentEventsHandler);
			this.getDocument().removeUndoableEditListener(undoManager);
			this.getDocument().addUndoableEditListener(undoManager);
			changed = false;
			setTabSize(getTabSize());
		}
		catch (IOException e) {
			read = false;
		}
		if (read) {
			// auto detect document type
			if (codeEditorDocument != null) {
				if (autoDetectType) {
					String name = f.getName();
					int index = name.lastIndexOf(".");
					if (index != -1) {
						String ext = name.substring(index);
						codeEditorDocument.setTypeByExtension(ext);
						updateCodeEditorHighlighter();
					}
				}
			}
			undoManager.discardAllEdits();
		}
		return read;
	}
	
	/**
	 * Writes data to the specified file.
	 * 
	 * @param f Source file
	 * @return <code>true</code> if it was a success to write the file; <code>false</code> otherwise
	 */
	public boolean writeToFile(File f) {
		boolean writen = true;
		try {
			PrintWriter writer = new PrintWriter(new FileWriter(f.getAbsolutePath()));
			this.write(writer);
			writer.close();
			this.getDocument().removeDocumentListener(lineNumbersPaneHandler);
			this.getDocument().addDocumentListener(lineNumbersPaneHandler);
			this.getDocument().removeDocumentListener(codeEditorDocumentEventsHandler);
			this.getDocument().addDocumentListener(codeEditorDocumentEventsHandler);
			this.getDocument().removeUndoableEditListener(undoManager);
			this.getDocument().addUndoableEditListener(undoManager);
			changed = false;
		}
		catch (IOException e){
			writen = false;
		}
		if (writen) {
			// auto detect document type
			if (codeEditorDocument != null) {
				if (autoDetectType) {
					String name = f.getName();
					int index = name.lastIndexOf(".");
					if (index != -1) {
						String ext = name.substring(index);
						codeEditorDocument.setTypeByExtension(ext);
						updateCodeEditorHighlighter();
					}
				}
			}
		}
		return writen;
	}
	
	/**
	 * Indents the selected block of text.
	 */
	public void indent() {
		int startSelectionOffset = this.getSelectionStart();
		int endSelectionOffset = this.getSelectionEnd();
		int firstSelectedLine = getLineOfOffset(startSelectionOffset);
		int lastSelectedLine = getLineOfOffset(endSelectionOffset);
		indent(startSelectionOffset, endSelectionOffset);
		if (lastSelectedLine != getLineCount()-1) {
			this.select(getLineStartOffset(firstSelectedLine), getLineEndOffset(lastSelectedLine)-1);
		} else {
			this.select(getLineStartOffset(firstSelectedLine), getLineEndOffset(lastSelectedLine));
		}
	}
	
	/**
	 * Indents the specified block of text.
	 * 
	 * @param startOffset The start offset >= 0
	 * @param endOffset The end offset >= <code>startOffset</code>
	 */
	public void indent(int startOffset, int endOffset) {
		if (endOffset < startOffset) {
			return;
		}
		int firstSelectedLine = getLineOfOffset(startOffset);
		int lastSelectedLine = getLineOfOffset(endOffset);
		for (int line = firstSelectedLine; line <= lastSelectedLine; line++) {
			insertTab(getLineStartOffset(line));
		}
	}
	
	/**
	 * Outdets the selected block of text.
	 */
	public void outdent() {
		int startSelectionOffset = this.getSelectionStart();
		int endSelectionOffset = this.getSelectionEnd();
		int firstSelectedLine = getLineOfOffset(startSelectionOffset);
		int lastSelectedLine = getLineOfOffset(endSelectionOffset);
		outdent(startSelectionOffset, endSelectionOffset);
		if (lastSelectedLine != getLineCount()-1) {
			this.select(getLineStartOffset(firstSelectedLine), getLineEndOffset(lastSelectedLine)-1);
		} else {
			this.select(getLineStartOffset(firstSelectedLine), getLineEndOffset(lastSelectedLine));
		}
	}
	
	/**
	 * Outdets the specified block of text.
	 * 
	 * @param startOffset The start offset >= 0
	 * @param endOffset The end offset >= <code>startOffset</code>
	 */
	public void outdent(int startOffset, int endOffset) {
		if (endOffset < startOffset) {
			return;
		}
		int firstSelectedLine = getLineOfOffset(startOffset);
		int lastSelectedLine = getLineOfOffset(endOffset);
		Document doc = getDocument();
		int offs;
		for (int line = firstSelectedLine; line <= lastSelectedLine; line++) {
			int i = 0;
			offs = getLineStartOffset(line);
			String str = getLine(line);
			while ( (str.length() > 0) && ( (str.charAt(0) == ' ') || (str.charAt(0) == '\t') ) && (i < getTabSize()) ) {
				boolean t = false;
				if (str.charAt(0) == '\t') {
					t = true;
				}
				try {
					doc.remove(offs, 1);
				}
				catch (BadLocationException e) {}
				if (t) {
					break;
				}
				i++;
				str = getLine(line);
			}
		}
	}
	
	/**
	 * Inserts a tab or spaces depending on the <code>insertSpaces</code> property.
	 * 
	 * @param offset The offset >= 0
	 */
	public void insertTab(int offset) {
		if (insertSpaces) {
			int lineStartOffset = getLineStartOffset(getCurrentLineIndex());
			int tabSize = getTabSize();
			String str = "";
			try {
				str = getText(lineStartOffset, offset-lineStartOffset);
			}
			catch (BadLocationException e) {
			}
			int cnt = 0;
			for (int i = 0; i < str.length(); i++) {
				if (str.charAt(i) != '\t') {
					cnt++;
				} else {
					cnt = 0;
				}
				cnt %= tabSize;
			}
			cnt = tabSize - cnt;
			String spaces = "";
			for (int i = 0; i < cnt; i++) {
				spaces += " ";
			}
			insert(spaces, offset);
		} else {
			insert("\t", offset);
		}
	}
	
	/**
	 * Sets pop up menu.
	 * 
	 * @param p Pop up menu. If <code>p</code> is <code>null</code> the pop up menu listener will be removed
	 */
	public void setPopupMenu(JPopupMenu p) {
		if (p != null) {
			popup = p;
			this.addMouseListener(popupMenuHandler);
		} else {
			this.removeMouseListener(popupMenuHandler);
		}
	}
	
	/**
	 * Sets the given observer to begin receiving notifications when changes are made in the editor.
	 * 
	 * @param l The observer to register
	 */
	public void addCodeEditorChangedListener(CodeEditorChangeListener l) {
		codeEditorListenerList.add(CodeEditorChangeListener.class, l);
	}
	
	/**
	 * Unregisters the given observer so it will no longer receive events.
	 * 
	 * @param l The observer to unregister
	 */
	public void removeCodeEditorChangedListener(CodeEditorChangeListener l) {
		codeEditorListenerList.remove(CodeEditorChangeListener.class, l);
	}
	
	/**
	 * Gets all listeners that have registered.
	 * 
	 * @return all listeners that have registered
	 */
	public CodeEditorChangeListener[] getCodeEditorChangeListeners() {
		return codeEditorListenerList.getListeners(CodeEditorChangeListener.class);
	}
	
	/**
	 * Notifies all listeners that have registered interest for notification on
	 * this event type.
	 */
	protected void fireEditorChanged() {
		// guaranteed to return a non-null array
		Object[] listeners = codeEditorListenerList.getListenerList();
		for (Object i : listeners) {
			if (i instanceof CodeEditorChangeListener) {
				((CodeEditorChangeListener)i).editorChanged();
			}
		}
	}
	
	/**
	 * Notifies all listeners that have registered interest for notification on
	 * this event type.
	 */
	protected void fireUndoableEditHappened() {
		// guaranteed to return a non-null array
		Object[] listeners = codeEditorListenerList.getListenerList();
		for (Object i : listeners) {
			if (i instanceof CodeEditorChangeListener) {
				((CodeEditorChangeListener)i).undoableEditHappened();
			}
		}
	}
	
	private EventListenerList codeEditorListenerList;
	private CodeEditorDocument codeEditorDocument;
	private DocumentHighlighter documentHighlighter;
	private CodeEditorUndoManager undoManager;
	private boolean changed;
	private boolean readOnly;
	private PopupMenuHandler popupMenuHandler;
	private JPopupMenu popup;
	private CodeEditorDocumentEventsHandler codeEditorDocumentEventsHandler;
	private boolean autoDetectType;
	private boolean insertSpaces;
	private HighlightDocumentListener highlightDocumentListener;
	
	
	/**
	 * Provides pop up menu showing.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class PopupMenuHandler extends MouseAdapter {
		
		@Override
		public void mousePressed(MouseEvent evt) {
			// if right mouse button was pressed
			if (evt.isPopupTrigger()) {
				// show pop up menu
				popup.show(evt.getComponent(), evt.getX(), evt.getY());
			}
		}
		
		@Override
		public void mouseReleased(MouseEvent evt) {
			// if right mouse button was pressed
			if (evt.isPopupTrigger()) {
				// show pop up menu
				popup.show(evt.getComponent(), evt.getX(), evt.getY());
			}
		}
		
	}
	
	/**
	 * Provides to use the undo manager.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class CodeEditorUndoManager extends UndoManager {
		
		@Override
		public void undoableEditHappened(UndoableEditEvent evt) {
			super.undoableEditHappened(evt);
			fireUndoableEditHappened();
		}
		
	}
	
	/**
	 * Provides to listen key events.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class CodeEditorKeyListener extends KeyAdapter {
		
		@Override
		public void keyTyped(KeyEvent evt) {
			// if key == ENTER then paste remembered range to leave the caret on the same level
			if (evt.getKeyChar() == KeyEvent.VK_ENTER) {
				insert(strToIns, CodeEditor.this.getCaretPosition());
			}
		}
		
		@Override
		public void keyPressed(KeyEvent evt) {
			// if key == SHIFT + TAB then unindent
			if ( (evt.getKeyChar() == KeyEvent.VK_TAB) && (evt.getModifiers() == KeyEvent.SHIFT_MASK) ) {
				evt.setKeyCode(0);
				outdent();
				return;
			}
			// if key == TAB
			if ( (evt.getKeyChar() == KeyEvent.VK_TAB) && (evt.getModifiers() == 0) ) {
				evt.setKeyCode(0);
				// and selection is not empty then indent
				if (getSelectionStart() != getSelectionEnd()) {
					indent();
					return;
				}
				// insert tab
				try {
					int selectionStart = getSelectionStart();
					int selectionLenght = getSelectionEnd() - selectionStart;
					getDocument().remove(selectionStart, selectionLenght);
				}
				catch (BadLocationException e) {}
				insertTab(getCaretPosition());
			}
			// if key == ENTER then remember range from 0 to first letter in the current string
			if (evt.getKeyChar() == KeyEvent.VK_ENTER) {
				String line = getLine();
				strToIns = "";
				for (int i = 0; i < line.length(); i++) {
					if ( (line.charAt(i) == ' ') || (line.charAt(i) == '\t') ) {
						strToIns += line.charAt(i);
					} else {
						break;
					}
				}
			}
		}
		
		private String strToIns;
		
	}
	
	/**
	 * Provides receiving document events.
	 * 
	 * @author Phil Tsarik
	 * 
	 */
	private class CodeEditorDocumentEventsHandler implements DocumentListener {
		
		@Override
		public void changedUpdate(DocumentEvent evt) {}
		
		@Override
		public void insertUpdate(DocumentEvent evt) {
			changed = true;
			fireEditorChanged();
		}
		
		@Override
		public void removeUpdate(DocumentEvent evt) {
			changed = true;
			fireEditorChanged();
		}
		
	}
	
}
