package editors.event;

import java.util.EventListener;


/**
 * The listener interface for receiving <code>CodeEditor</code>'s document events.<br><br>
 * 
 * Example:<br>
 * <code><pre>
 * CodeEditor codeEditor = new CodeEditor();
 * codeEditor.setCodeEditorChangedListener(new MyEditorHandler());
 * </pre></code>
 * <code>...</code>
 * <code><pre>
 * private class MyEditorHandler implements CodeEditorChangedListener {
 *      public void editorChanged() {
 *           // do something
 *      }
 *      public void undoableEditHappened() {
 *           // do something
 *      }
 * }
 * </pre></code>
 * 
 * @author Phil Tsarik
 *
 */
public interface CodeEditorChangeListener extends EventListener {
	
	/**
	 * Invoked when a <code>CodeEditor</code>'s document has been changed.
	 */
	public void editorChanged();
	
	/**
	 * Invoked when the undoable edit has been happened.
	 */
	public void undoableEditHappened();
	
}
