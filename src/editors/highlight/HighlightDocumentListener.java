package editors.highlight;

import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;


/**
 * This class provides to highlight the specified words in the specified text pane.
 * 
 * @author Sergey Tuchina
 * @version 0.7.1
 *
 */
public class HighlightDocumentListener implements DocumentListener {
	
	/**
	 * Constructs a new instance of the <code>HighlightDocumentListener</code> class.
	 * 
	 * @param target the target text pane
	 * @param documentHighlighter the highlighter
	 */
	public HighlightDocumentListener(JTextPane target, DocumentHighlighter documentHighlighter) { //List <String> _hWords, List <Character> _whiteSpaces, SimpleAttributeSet _hText, SimpleAttributeSet _defText) {
		this.documentHighlighter = documentHighlighter;
		this.owner = target;
	}
	
	/**
	 * Updates all highlightings in the text.
	 */
	public void update() {
		new Highlighter(0, owner.getText().length()).run();
	}
	
	@Override
	public void changedUpdate(DocumentEvent evt) {
		if (toUpdate) {
			doUpdate(evt);
		} else {
			toUpdate = true;
		}
	}

	@Override
	public void insertUpdate(DocumentEvent evt) {
		doUpdate(evt);
	}

	@Override
	public void removeUpdate(DocumentEvent evt) {
		doUpdate(evt);
	}
	
	
	private DocumentHighlighter documentHighlighter;
	private boolean toUpdate = false;
	private JTextPane owner = null;
	private List <String> hWords = null;
	private List <Character> whiteSpaces = null;
	private SimpleAttributeSet hText = null;
	private SimpleAttributeSet defText = null;
	

	private void doUpdate(DocumentEvent evt) {
		SwingUtilities.invokeLater(new Highlighter(evt.getOffset(), evt.getLength()));
	}
	
	private class Highlighter implements Runnable {
		
		private int chStart;
		private int chEnd;
		private StyledDocument doc = null;
		
		public Highlighter(int _chStart, int _length) {
			chStart = _chStart;
			chEnd = _chStart + _length-1;
			doc = owner.getStyledDocument();
		}
		
		private boolean isWhiteSpace(char _value) {
			whiteSpaces = documentHighlighter.getDividers();
			if (whiteSpaces == null) {
				return true;
			}
			if (whiteSpaces.size() == 0) {
				return true;
			}
			for (int i=0; i<whiteSpaces.size(); i++) {
				if (_value==whiteSpaces.get(i).charValue())
					return true;
			}
			return false;
		}
		
		private boolean isHWord(String _value) {
			hWords = documentHighlighter.getWords();
			for (int i=0; i<hWords.size(); i++) {
				if (documentHighlighter.isCaseSensitive()) {
					if (_value.equals(hWords.get(i))) {
						return true;
					}
				} else {
					if (_value.equalsIgnoreCase(hWords.get(i))) {
						return true;
					}
				}
			}
			return false;
		}
		
		private int findCHStart(int _chStart, String _text) {
			int result = _chStart;
			if (result>=_text.length())
				result = _text.length() - 1;
			if (result<0)
				result = 0;
			if (result!=0)
				if (isWhiteSpace(_text.charAt(result)))
					result--;
			if (result<0)
				result = 0;
			while (result!=0){
				if (!isWhiteSpace(_text.charAt(result)))
					result--;
				else
					break;
			}
			while (result<_text.length()) {
				if (isWhiteSpace(_text.charAt(result)))
					result++;
				else
					break;
			}
			if ((result>=_text.length())&(result!=0))
				result = _text.length() - 1;
			return result;
		}
		
		private int findCHEnd(int _chStart, int _chEnd, String _text) {
			int result = _chEnd;
			if (result>=_text.length())
				result = _text.length() - 1;
			if (result<_chStart)
				result = _chStart;
			if ((result!=_text.length() - 1)&(_text.length()!=0))
				if (isWhiteSpace(_text.charAt(result)))
					result++;
			while (result<_text.length()) {
				if (!isWhiteSpace(_text.charAt(result)))
					result++;
				else
					break;
			}
			if (result>=_text.length())
				result = _text.length() - 1;
			while (result>_chStart) {
				if (isWhiteSpace(_text.charAt(result)))
					result--;
				else
					break;
			}
			return result;
		}
		
		@Override
		public void run() {
			if (documentHighlighter == null) {
				return;
			}
			hWords = documentHighlighter.getWords();
			whiteSpaces = documentHighlighter.getDividers();
			hText = documentHighlighter.getHighlightedAttributeSet();
			defText = documentHighlighter.getDefaultAttributeSet();
			if ( (hWords == null) || (hText == null) || (defText == null) ) {
				return;
			}
			String text = null;
			try {
				text = doc.getText(0, doc.getLength());
			} catch (Exception e) {
			}
			if (text == null) {
				return;
			}
			chStart = findCHStart(chStart, text);
			chEnd = findCHEnd(chStart, chEnd, text);
			while (chStart<chEnd+1) {
				String res = "";
				int a = chStart;
				int b = chStart;
				for (int i = chStart; i<chEnd+1; i++) {
					if (!isWhiteSpace(text.charAt(i))) {
						res = res+text.charAt(i);
						b++;
					} else {
						break;
					}
				}
				chStart = chStart+res.length()+1;
				toUpdate = false;
				if (isHWord(res)) {
					if (documentHighlighter == null) {
						return;
					}
					doc = owner.getStyledDocument();
					hText = documentHighlighter.getHighlightedAttributeSet();
					doc.setCharacterAttributes(a, b-a, hText, false);
				} else {
					if (documentHighlighter == null) {
						return;
					}
					doc = owner.getStyledDocument();
					defText = documentHighlighter.getDefaultAttributeSet();
					//try {
						doc.setCharacterAttributes(a, b-a, defText, false);
					/*} 
					catch (Exception e) {
						return;
					}*/
				}
			}
		}
		
	}
}
