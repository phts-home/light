package editors.highlight;

import java.util.List;
import javax.swing.text.SimpleAttributeSet;


/**
 * The highlighter interface which determines parameters for text highlight
 * 
 * @author Sergey Tuchina
 * @author Phil Tsarik
 *
 */
public interface DocumentHighlighter {
	
	/**
	 * Gets an attribute set for default text.
	 * 
	 * @return An attribute set for default text
	 */
	public SimpleAttributeSet getDefaultAttributeSet();
	
	/**
	 * Gets an attribute set for highlighted text.
	 * 
	 * @return An attribute set for highlighted text
	 */
	public SimpleAttributeSet getHighlightedAttributeSet();
	
	/**
	 * Returns <code>true</code> if highlight is case sensitive.
	 * 
	 * @return <code>true</code> if highlight is case sensitive
	 */
	public boolean isCaseSensitive();
	
	/**
	 * Gets the dividers.
	 * 
	 * @return The dividers
	 */
	public List<Character> getDividers();
	
	/**
	 * Gets the words which need to be highlighted
	 * 
	 * @return The words which need to be highlighted
	 */
	public List<String> getWords();
	
}
