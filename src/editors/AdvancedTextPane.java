package editors;

import panes.*;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;

import java.awt.*;
import java.awt.event.*;


/**
 * The text pane extended from <code>JTextPane</code>, which contains
 * some useful methods.<br>
 * Also you can link this editor to the line numbers pane by calling method
 * <code>setLineNumbersPane</code>. This method is automatically called
 * by <code>setEditor(AdvancedTextPane)</code> method in the 
 * <code>LineNumbersPane</code> class.
 * 
 * @see LineNumbersPane
 * 
 * @author Phil Tsarik
 * @author Sergey Tuchina
 * @version 0.9.5
 *
 */
public class AdvancedTextPane extends JTextPane {
	
	/**
	 * Constructs a new instence of the <code>AdvancedTextPane</code> class.
	 */
	public AdvancedTextPane() {
		super();
		addKeyListener(new OverwriteTextHandler());
		overwrite = false;
		setTabSize(10);
		setLineWrap(true);
	}
	
	@Override
	public void setSize(Dimension d) {
		if (d.width<=getParent().getSize().width)
			d.width = getParent().getSize().width;
		super.setSize(d);
	}
	
	@Override
	public boolean getScrollableTracksViewportWidth() {
		return lineWrap;
	}
	
	/**
	 * Sets the <code>LineNumbersPane</code> in which this editor will be as viewport.
	 * This method is called from <code>LineNumbersPane</code> in <code>setEditor</code>
	 * method.
	 * 
	 * @param lineNumbersPane The <code>LineNumbersPane</code>'s instance
	 */
	public void setLineNumbersPane(LineNumbersPane lineNumbersPane) {
		this.lineNumbersPane = lineNumbersPane;
		if (lineNumbersPane != null) {
			if (lineNumbersPaneHandler == null) {
				lineNumbersPaneHandler = new LineNumbersPaneHandler();
			}
			getDocument().addDocumentListener(lineNumbersPaneHandler);
		} else {
			if (lineNumbersPaneHandler != null) {
				getDocument().removeDocumentListener(lineNumbersPaneHandler);
			}
		}
	}
	
	/**
	 * Inserts the specified text at the current position.
	 * 
	 * @param str The text to insert
	 */
	public void insert(String str) {
		int pos = this.getCaretPosition();
		insert(str, pos);
	}
	
	/**
	 * Inserts the specified text at the specified position.
	 * 
	 * @param str The text to insert
	 * @param pos The position at which to insert >= 0
	 */
	public void insert(String str, int pos) {
		Document doc = this.getDocument();
		if (doc != null) {
			try {
				doc.insertString(pos, str, null);
			} catch (BadLocationException e) {
			}
		}
	}
	
	/**
	 * Sets tab size.
	 * 
	 * @param tabSize Tab size
	 */
	public void setTabSize(int tabSize) {
		this.tabSize = tabSize;
		FontMetrics fm = this.getFontMetrics(this.getFont());
		int charWidth = fm.charWidth('w');
		int tabWidth = charWidth * tabSize;
 		TabStop[] tabs = new TabStop[50];
 		for (int j = 0; j < tabs.length; j++) {
			int tab = j + 1;
			tabs[j] = new TabStop(tab * tabWidth);
		}
 		TabSet tabSet = new TabSet(tabs);
		SimpleAttributeSet attributes = new SimpleAttributeSet();
		StyleConstants.setTabSet(attributes, tabSet);
		int length = getCharCount();
		this.getStyledDocument().setParagraphAttributes(0, length, attributes, false);
	}
	
	/**
	 * Gets tab size.
	 * 
	 * @return Tab size
	 */
	public int getTabSize() {
		return tabSize;
	}
	
	/**
	 * Tests whether line wrap is enabled.
	 * 
	 * @return <code>true</code> if line wrap is enabled; <code>false</code> otherwise
	 */
	public boolean isLineWrap() {
		return lineWrap;
	}
	
	/**
	 * Sets <code>lineWrap</code> property, which determines whether line wrap is enabled.
	 * 
	 * @param lineWrap <code>true</code> to specify that line wrap is enabled; <code>false</code> to specify that line wrap is disabled
	 */
	public void setLineWrap(boolean lineWrap) {
		this.lineWrap = lineWrap;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				if (lineNumbersPane != null) {
					lineNumbersPane.updateLineNumbers();
				}
			}
		});
	}
	
	/**
	 * Tests whether typing mode is overwrite.
	 * 
	 * @return <code>true</code> if overwrite mode; <code>false</code> if insert mode
	 */
	public boolean isOverwrite() {
		return overwrite;
	}
	
	/**
	 * Sets <code>overwrite</code> property which determines whether typing mode is overwrite.
	 * 
	 * @param overwrite <code>true</code> to specify the overwrite mode; <code>false</code> to specify the insert mode
	 */
	public void setOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
	}
	
	/**
	 * Gets char count.
	 * 
	 * @return Char count >= 0
	 */
	public int getCharCount() {
		return this.getDocument().getLength();
	}
	
	/**
	 * Gets byte count.
	 * 
	 * @return Byte count >= 0
	 */
	public int getByteCount() {
		return this.getText().length();
	}
	
	/**
	 * Gets line count.
	 * 
	 * @return Line count >= 0
	 */
	public int getLineCount() {
		return this.getDocument().getDefaultRootElement().getElementCount();
	}
	
	/**
	 * Gets current line index.
	 * 
	 * @return Current line index >= 0
	 */
	public int getCurrentLineIndex() {
		int offset = this.getCaretPosition();
		int lineOfOffset = this.getDocument().getDefaultRootElement().getElementIndex(offset);
		return lineOfOffset;
	}
	
	/**
	 * Gets current offset relative to current line offset.
	 * 
	 * @return Current column index >= 0
	 */
	public int getCurrentColIndex() {
		int lineStartOffset;
		try {
			lineStartOffset = getLineStartOffset(getCurrentLineIndex());
		}
		catch (NullPointerException e) {
			lineStartOffset = 0;
		}
		return (this.getCaretPosition() - lineStartOffset);
	}
	
	/**
	 * Gets the offset of the start of the line with the specified index.
	 *  
	 * @param line The line index >= 0
	 * @return The offset >= 0. -1 if <code>line</code> < 0 or <code>line</code> >= line count
	 */
	public int getLineStartOffset(int line) {
		if ( (line < 0) || (line >= getLineCount()) ) {
			return -1;
		}
		return this.getDocument().getDefaultRootElement().getElement(line).getStartOffset();
	}
	
	/**
	 * Gets the offset of the end of the line with the specified index.
	 * 
	 * @param line The line index >= 0
	 * @return The offset >= 0. -1 if <code>line</code> < 0 or <code>line</code> >= line count
	 */
	public int getLineEndOffset(int line) {
		if ( (line < 0) || (line >= getLineCount()) ) {
			return -1;
		}
		int endOffset = this.getDocument().getDefaultRootElement().getElement(line).getEndOffset();
		return ((line == getLineCount() - 1) ? (endOffset - 1) : endOffset);
	}
	
	/**
	 * Translates an offset into the components text to a line number.
	 * 
	 * @param offset The offset >= 0
	 * @return The line number >= 0
	 */
	public int getLineOfOffset(int offset) {
		Document doc = this.getDocument();
		if ( (offset < 0) || (offset > doc.getLength()) ) {
			return -1;
		}
		return this.getDocument().getDefaultRootElement().getElementIndex(offset);
	}
	
	/**
	 * Defines the meaning of the height of a row. This defaults to the height of the font.
	 * 
	 * @return The row height >= 1
	 */
	public int getRowHeight() {
		FontMetrics metrics = this.getFontMetrics(this.getFont());
		return metrics.getHeight();
	}
	
	/**
	 * Gets the current line.
	 * 
	 * @return Current line
	 */
	public String getLine() {
		return getLine(getCurrentLineIndex());
	}
	
	/**
	 * Gets a line with the specified index.
	 * 
	 * @param line The line index >= 0
	 * @return Line
	 */
	public String getLine(int line) {
		if ( (line < 0) || (line >= getLineCount()) ) {
			return null;
		}
		String res;
		try {
			res = this.getText(getLineStartOffset(line), getLineEndOffset(line) - getLineStartOffset(line));
		}
		catch (BadLocationException e) {
			res = null;
		}
		return res;
	}
	
	/**
	 * Moves up the current line or selected lines.
	 */
	public void moveUpLine() {
		int startSelectionOffset = this.getSelectionStart();
		int endSelectionOffset = this.getSelectionEnd();
		int firstSelectedLine = getLineOfOffset(startSelectionOffset);
		int lastSelectedLine = getLineOfOffset(endSelectionOffset);
		
		if (firstSelectedLine == lastSelectedLine) {
			moveUpLine(firstSelectedLine);
			return;
		}
		
		if (firstSelectedLine == 0) {
			return;
		}
		
		int lineStartOffset = getLineStartOffset(firstSelectedLine);
		int lineEndOffset = getLineEndOffset(lastSelectedLine);
		
		try {
			Document doc = getDocument();
			String str = doc.getText(lineStartOffset, lineEndOffset-lineStartOffset);
			doc.remove(lineStartOffset, lineEndOffset-lineStartOffset);
			int newStartOffset = getLineStartOffset(firstSelectedLine-1);
			insert(str, newStartOffset);
		}
		catch (BadLocationException e) {
		}
		if (firstSelectedLine != lastSelectedLine) {
			this.select(getLineStartOffset(firstSelectedLine-1), getLineEndOffset(lastSelectedLine-1)-1);
		}
	}
	
	/**
	 * Moves up a line with the specified index.
	 * 
	 * @param line The line index >= 0
	 */
	public void moveUpLine(int line) {
		if ( (line <= 0) || (line >= getLineCount()) ) {
			return;
		}
		int colIndex = getCurrentColIndex();
		int lineStartOffset = getLineStartOffset(line);
		int lineEndOffset = getLineEndOffset(line);
		try {
			Document doc = getDocument();
			String str = doc.getText(lineStartOffset, lineEndOffset-lineStartOffset);
			doc.remove(lineStartOffset, lineEndOffset-lineStartOffset);
			int newStartOffset = getLineStartOffset(line-1);
			insert(str, newStartOffset);
			setCaretPosition(newStartOffset+colIndex);
		}
		catch (BadLocationException e) {
		}
	}
	
	/**
	 * Moves down the current line or selected lines.
	 */
	public void moveDownLine() {
		int startSelectionOffset = this.getSelectionStart();
		int endSelectionOffset = this.getSelectionEnd();
		int firstSelectedLine = getLineOfOffset(startSelectionOffset);
		int lastSelectedLine = getLineOfOffset(endSelectionOffset);
		
		if (firstSelectedLine == lastSelectedLine) {
			moveDownLine(firstSelectedLine);
			return;
		}
		
		if (lastSelectedLine == getLineCount()-1) {
			return;
		}
		
		int lineStartOffset = getLineStartOffset(firstSelectedLine);
		int nextLineStartOffset = getLineStartOffset(lastSelectedLine+1);
		int nextLineEndOffset = getLineEndOffset(lastSelectedLine+1);
		try {
			Document doc = getDocument();
			String str = doc.getText(nextLineStartOffset, nextLineEndOffset-nextLineStartOffset);
			if (lastSelectedLine == getLineCount()-2) {
				str += "\n";
			}
			insert(str, lineStartOffset);
			doc.remove(getLineStartOffset(lastSelectedLine+2), getLineEndOffset(lastSelectedLine+2)-getLineStartOffset(lastSelectedLine+2));
		}
		catch (BadLocationException e) {
		}
		if (firstSelectedLine != lastSelectedLine) {
			this.select(getLineStartOffset(firstSelectedLine+1), getLineEndOffset(lastSelectedLine+1)-1);
		}
	}
	
	/**
	 * Moves down a line with the specified index.
	 * 
	 * @param line The line index >= 0
	 */
	public void moveDownLine(int line) {
		if ( (line < 0) || (line >= getLineCount()-1) ) {
			return;
		}
		int lineStartOffset = getLineStartOffset(line);
		int nextLineStartOffset = getLineStartOffset(line+1);
		int nextLineEndOffset = getLineEndOffset(line+1);
		try {
			Document doc = getDocument();
			String str = doc.getText(nextLineStartOffset, nextLineEndOffset-nextLineStartOffset);
			if (line == getLineCount()-2) {
				str += "\n";
			}
			insert(str, lineStartOffset);
			doc.remove(getLineStartOffset(line+2), getLineEndOffset(line+2)-getLineStartOffset(line+2));
		}
		catch (BadLocationException e) {
		}
	}
	
	/**
	 * Cuts the current line or selected lines.
	 */
	public void cutLine() {
		int startSelectionOffset = this.getSelectionStart();
		int endSelectionOffset = this.getSelectionEnd();
		int firstSelectedLine = getLineOfOffset(startSelectionOffset);
		int lastSelectedLine = getLineOfOffset(endSelectionOffset);
		
		if (firstSelectedLine == lastSelectedLine) {
			cutLine(firstSelectedLine);
			return;
		}
		
		int lineStartOffset = getLineStartOffset(firstSelectedLine);
		int lineEndOffset = getLineEndOffset(lastSelectedLine);
		
		select(lineStartOffset, lineEndOffset);
		cut();
	}
	
	/**
	 * Cuts a line with the specified index.
	 * 
	 * @param line The line index >= 0
	 */
	public void cutLine(int line) {
		if ( (line < 0) || (line >= getLineCount()) ) {
			return;
		}
		int lineStartOffset = getLineStartOffset(line);
		int lineEndOffset = getLineEndOffset(line);
		select(lineStartOffset, lineEndOffset);
		cut();
	}
	
	/**
	 * Copies the current line or selected lines.
	 */
	public void copyLine() {
		int startSelectionOffset = this.getSelectionStart();
		int endSelectionOffset = this.getSelectionEnd();
		int firstSelectedLine = getLineOfOffset(startSelectionOffset);
		int lastSelectedLine = getLineOfOffset(endSelectionOffset);
		
		if (firstSelectedLine == lastSelectedLine) {
			copyLine(firstSelectedLine);
			return;
		}
		
		int lineStartOffset = getLineStartOffset(firstSelectedLine);
		int lineEndOffset = getLineEndOffset(lastSelectedLine);
		
		select(lineStartOffset, lineEndOffset);
		copy();
	}
	
	/**
	 * Copies a line with the specified index.
	 * 
	 * @param line The line index >= 0
	 */
	public void copyLine(int line) {
		if ( (line < 0) || (line >= getLineCount()) ) {
			return;
		}
		int lineStartOffset = getLineStartOffset(line);
		int lineEndOffset = getLineEndOffset(line);
		select(lineStartOffset, lineEndOffset);
		copy();
	}
	
	/**
	 * Deletes the current line or selected lines.
	 */
	public void deleteLine() {
		int startSelectionOffset = this.getSelectionStart();
		int endSelectionOffset = this.getSelectionEnd();
		int firstSelectedLine = getLineOfOffset(startSelectionOffset);
		int lastSelectedLine = getLineOfOffset(endSelectionOffset);
		for (int i = 0; i <= lastSelectedLine-firstSelectedLine; i++) {
			deleteLine(firstSelectedLine);
		}
	}
	
	/**
	 * Deletes a line with the specified index.
	 * 
	 * @param line The line index >= 0
	 */
	public void deleteLine(int line) {
		if ( (line < 0) || (line >= getLineCount()) ) {
			return;
		}
		try {
			this.getDocument().remove(getLineStartOffset(line), getLineEndOffset(line) - getLineStartOffset(line));
		}
		catch (BadLocationException e) {
		}
	}
	
	/**
	 * Duplicates the current line.
	 */
	public void duplicateLine() {
		duplicateLine(getCurrentLineIndex());
	}
	
	/**
	 * Duplicates a line with the specified index.
	 * 
	 * @param line The line index >= 0
	 */
	public void duplicateLine(int line) {
		if ( (line < 0) || (line >= getLineCount()) ) {
			return;
		}
		try {
			this.getDocument().insertString(getLineStartOffset(line), getLine(line), null);
		}
		catch (BadLocationException e) {
		}
	}
	
	/**
	 * Searches the specified string in the text.
	 * 
	 * @param str String to search
	 * @param from Index from which should start to search
	 * @return <code>true</code> if the specified string is found; <code>false</code> otherwise
	 */
	public boolean find(String str, int from) {
		int startIndex = this.getText().indexOf(str, from);
		if (startIndex != -1) {
			this.select(startIndex, startIndex+str.length());
			return true;
		}
		return false;
	}
	
	/**
	 * Searches and replaces the specified string in <code>str</code> with the specified string in <code>rsrt</code>.
	 * 
	 * @param str String to search and replace
	 * @param rsrt String to replace <code>str</code> with
	 * @param from Index from which should start to search
	 * @return <code>true</code> if the specified string is found; <code>false</code> otherwise
	 */
	public boolean replace(String str, String rsrt, int from) {
		int startIndex = this.getText().indexOf(str, from);
		if (startIndex != -1) {
			this.select(startIndex, startIndex + str.length());
			this.replaceSelection(rsrt);
			this.select(startIndex, startIndex+rsrt.length());
			return true;
		}
		return false;
	}
	
	/**
	 * Searches and replaces all entries of the specified string in <code>str</code> with the specified string in <code>rsrt</code>.
	 * 
	 * @param str String to search and replace
	 * @param rsrt String to replace <code>str</code> with
	 * @param from Index from which should start to search
	 * @return <code>true</code> if the specified string is found; <code>false</code> otherwise
	 */
	public boolean replaceAll(String str, String rsrt, int from) {
		int startIndex = this.getText().indexOf(str, from);
		if (startIndex != -1) {
			while (startIndex != -1) {
				this.select(startIndex, startIndex + str.length());
				this.replaceSelection(rsrt);
				this.select(startIndex, startIndex+rsrt.length());
				startIndex = this.getText().indexOf(str, this.getSelectionEnd());
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Converts the selected text range to uppercase.
	 */
	public void convertToUpperCase() {
		convertToUpperCase(getSelectionStart(), getSelectionEnd());
	}
	
	/**
	 * Converts the specified text range to uppercase.
	 * 
	 * @param startOffset The start offset >= 0
	 * @param endOffset The end offset >= <code>startOffset</code>
	 */
	public void convertToUpperCase(int startOffset, int endOffset) {
		if (startOffset >= endOffset) {
			return;
		}
		Document doc = this.getDocument();
		String str;
		try {
			str = doc.getText(startOffset, endOffset-startOffset);
		}
		catch (BadLocationException e) {
			return;
		}
		str = str.toUpperCase();
		try {
			doc.remove(startOffset, endOffset-startOffset);
			insert(str, startOffset);
			select(startOffset, endOffset);
		}
		catch (BadLocationException e) {}
	}
	
	/**
	 * Converts the selected text range to lowercase.
	 */
	public void convertToLowerCase() {
		convertToLowerCase(getSelectionStart(), getSelectionEnd());
	}
	
	/**
	 * Converts the specified text range to lowercase.
	 * 
	 * @param startOffset The start offset >= 0
	 * @param endOffset The end offset >= <code>startOffset</code>
	 */
	public void convertToLowerCase(int startOffset, int endOffset) {
		if (startOffset >= endOffset) {
			return;
		}
		Document doc = this.getDocument();
		String str;
		try {
			str = doc.getText(startOffset, endOffset-startOffset);
		}
		catch (BadLocationException e) {
			return;
		}
		str = str.toLowerCase();
		try {
			doc.remove(startOffset, endOffset-startOffset);
			insert(str, startOffset);
			select(startOffset, endOffset);
		}
		catch (BadLocationException e) {}
	}
	
	
	private int tabSize;
	private boolean lineWrap;
	private boolean overwrite;
	private LineNumbersPane lineNumbersPane;
	protected LineNumbersPaneHandler lineNumbersPaneHandler;
	
	
	private class OverwriteTextHandler extends KeyAdapter {
		
		@Override
		public void keyTyped(KeyEvent evt) {
			if (overwrite) {
				if (evt.getKeyChar() <= 31) {
					return;
				}
				try {
					if (getCaretPosition() != getLineEndOffset(getCurrentLineIndex())-1) {
						getDocument().remove(getCaretPosition(), 1);
					}
				}
				catch (BadLocationException e) {}
			}
		}
		
	}
	
	private class LineNumbersPaneHandler implements DocumentListener {
		
		@Override
		public void changedUpdate(DocumentEvent evt) {}
		
		@Override
		public void insertUpdate(DocumentEvent evt) {
			if (lineNumbersPane != null) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						lineNumbersPane.updateLineNumbers();
					}
				});
			}
		}
		
		@Override
		public void removeUpdate(DocumentEvent evt) {
			if (lineNumbersPane != null) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						lineNumbersPane.updateLineNumbers();
					}
				});
			}
		}
		
	}
	
}
