package panes.enent;

import java.awt.*;


/**
 * <code>ClosableTabbedPane</code>'s event.
 * 
 * @author Phil Tsarik
 * @version 1.0.0
 *
 */
public class ClosableTabbedPaneEvent {
	
	/**
	 * Constructs a new <code>ClosableTabbedPaneEvent</code>.
	 * 
	 * @param comp <code>Component</code>
	 * @param index Index of the component on the <code>ClosableTabbedPane</code>
	 */
	public ClosableTabbedPaneEvent(Component comp, int index) {
		this.index = index;
		this.comp = comp;
	}
	
	/**
	 * Gets an index of the <code>Component</code>.
	 * 
	 * @return An index of the <code>Component</code>
	 */
	public int getIndex() {
		return index;
	}
	
	/**
	 * Gets an <code>Component</code>
	 * 
	 * @return <code>Component</code>
	 */
	public Component getComponent() {
		return comp;
	}
	
	private Component comp;
	private int index;
	
}
