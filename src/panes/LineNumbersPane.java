package panes;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;

import editors.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;


/**
 * A scroll pane with line numbers as <code>JList</code>.<br>
 * Line numbers are being built according to <code>AdvancedTextPane<code> instance,
 * has been set by method <code>setEditor</code>.
 * 
 * @author Phil Tsarik
 * @version 0.5.1
 *
 */
public class LineNumbersPane extends JScrollPane {
	
	/**
	 * Constructs a new <code>CodeEditorPane</code> instance.
	 */
	public LineNumbersPane() {
		super();
		super.getVerticalScrollBar().setUnitIncrement(10);
		addComponentListener(new ComponentEventsHandler());
		
		// create components
		vect = new Vector<String>();
		
		numberList = new JList(vect);
		numberList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		numberList.addListSelectionListener(new ListSelectionEventsHandler());
		numberList.setFocusable(false);
		
		setLineNumbersVisible(false);
		
		// disable interception of ctrl+tab and shift+ctrl+tab by JComponent
		numberList.setFocusTraversalKeysEnabled(false);
	}
	
	public LineNumbersPane(AdvancedTextPane editor) {
		this();
		setEditor(editor);
	}
	
	/**
	 * Gets the editor.
	 * 
	 * @return An instance of the editor
	 */
	public AdvancedTextPane getEditor() {
		return editor;
	}
	
	/**
	 * Sets the editor.
	 * 
	 * @param editor An instance of the editor
	 */
	public void setEditor(AdvancedTextPane editor) {
		if (editor != null) {
			this.editor = editor;
			getViewport().removeAll();
			getViewport().add(this.editor);
			numberList.setFixedCellHeight(editor.getRowHeight());
			this.editor.setFocusTraversalKeysEnabled(false);
			this.editor.setLineNumbersPane(this);
		} else {
			if (editor != null) {
				this.editor.setLineNumbersPane(null);
			}
			getViewport().removeAll();
			this.editor = editor;
		}
	}
	
	/**
	 * Gets the editor's font.
	 * 
	 * @return The editor's font
	 */
	public Font getEditorFont() {
		return (editor != null) ? editor.getFont() : null;
	}
	
	/**
	 * Gets the editor's font color.
	 * 
	 * @return The editor's font color
	 */
	public Color getEditorFontColor() {
		return (editor != null) ? editor.getForeground() : null;
	}
	
	/**
	 * Sets the font for the editor.
	 * 
	 * @param font The desired font for the editor
	 */
	public void setEditorFont(Font font) {
		if (editor != null) {
			editor.setFont(font);
			numberList.setFont(font);
			numberList.setFixedCellHeight(editor.getRowHeight());
		}
	}
	
	/**
	 * Sets the font for the editor.
	 * 
	 * @param font The desired font for the editor
	 * @param color The desired font color for the editor
	 */
	public void setEditorFont(Font font, Color color) {
		if (editor != null) {
			editor.setFont(font);
			editor.setForeground(color);
			numberList.setFont(font);
			numberList.setFixedCellHeight(editor.getRowHeight());
		}
	}
	
	/**
	 * Gets the editor's background color.
	 * 
	 * @return The editor's background color
	 */
	public Color getEditorBackgroundColor() {
		return (editor != null) ? editor.getBackground() : null;
	}
	
	/**
	 * Sets background color for the editor.
	 * 
	 * @param backgroundColor Background color for the editor
	 */
	public void setEditorBackgroundColor(Color backgroundColor) {
		if (editor != null) {
			editor.setBackground(backgroundColor);
		}
	}
	
	/**
	 * Gets line numbers' background color.
	 * 
	 * @return The line numbers' background color
	 */
	public Color getLineNumbersBackgroundColor() {
		return numberList.getBackground();
	}
	
	/**
	 * Sets background color for line numbers.
	 * 
	 * @param backgroundColor Background color for line numbers.
	 */
	public void setLineNumbersBackgroundColor(Color backgroundColor) {
		numberList.setBackground(backgroundColor);
	}
	
	/**
	 * Gets line numbers' font color.
	 * 
	 * @return Line numbers' background color
	 */
	public Color getLineNumbersFontColor() {
		return numberList.getForeground();
	}
	
	/**
	 * Sets font color for line numbers.
	 * 
	 * @param fontColor Font color for line numbers.
	 */
	public void setLineNumbersFontColor(Color fontColor) {
		numberList.setForeground(fontColor);
	}
	
	/**
	 * Tests whether the line numbers are visible.
	 * 
	 * @return <code>true</code> if the line numbers are visible; <code>false</code> otherwise
	 */
	public boolean isLineNumbersVisible() {
		return lineNumbersVisible;
	}
	
	/**
	 * Sets <code>lineNumbersVisible</code> property, which determines whether line numbers are visible.
	 * 
	 * @param lineNumbersVisible <code>true</code> to specify that line numbers are visible; <code>false</code> to specify that it is not visible
	 */
	public void setLineNumbersVisible(boolean lineNumbersVisible) {
		if (editor == null) {
			this.lineNumbersVisible = false;
			setRowHeaderView(null);
			return;
		}
		this.lineNumbersVisible = lineNumbersVisible;
		if (lineNumbersVisible) {
			setRowHeaderView(numberList);
			updateLineNumbers();
		} else {
			setRowHeaderView(null);
		}
	}
	
	/**
	 * Grabs focus to an instance of the <code>LineNumbersPane</code> class.
	 */
	@Override
	public void grabFocus() {
		super.grabFocus();
		if (editor != null) {
			editor.grabFocus();
		}
	}
	
	/**
	 * Updates line numbers of whole text.
	 */
	public void updateLineNumbers() {
		if ( (!lineNumbersVisible) || (editor == null) ) {
			return;
		}
		int lineHeight = editor.getRowHeight();
		vect.clear();
		for (int i = 0; i < editor.getLineCount(); i++) {
			vect.add(String.valueOf(i+1)+":");
			try {
				Rectangle r1 = editor.getUI().modelToView(editor, editor.getLineStartOffset(i));
				Rectangle r2 = editor.getUI().modelToView(editor, editor.getLineEndOffset(i)-1);
				int strHeight = (int)Math.round(r2.getY() - r1.getY() + r2.getHeight());
				int strCnt = strHeight / lineHeight;
				for (int k = 0; k < strCnt-1; k++) {
					vect.add("");
				}
			}
			catch (BadLocationException e) {}
			catch (NullPointerException e) {}
		}
		numberList.setListData(vect);
	}
	
	
	private AdvancedTextPane editor;
	private JList numberList;
	private Vector<String> vect;
	private boolean lineNumbersVisible;
	
	
	/**
	 * Provides to update NumberListPane on resizing.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class ComponentEventsHandler implements ComponentListener {
		@Override
		public void componentResized(ComponentEvent evt) {
			updateLineNumbers();
		}
		@Override
		public void componentHidden(ComponentEvent evt) {}
		@Override
		public void componentMoved(ComponentEvent evt) {}
		@Override
		public void componentShown(ComponentEvent evt) {}
	}
	
	/**
	 * Provides to select lines on clicking on NumberListPane.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class ListSelectionEventsHandler implements ListSelectionListener {
		@Override
		public void valueChanged(ListSelectionEvent evt) {
			if (numberList.isSelectionEmpty()) {
				return;
			}
			if (editor == null) {
				return;
			}
			Object[] values = numberList.getSelectedValues();
			int start = 0;
			int end = 0;
			boolean err = true;
			boolean err2 = true;
			for (int i = 0; i < values.length; i++) {
				String value = (String)values[i];
				if (!value.equals("")) {
					String s = value.substring(0, value.length()-1);
					int line = Integer.parseInt(s);
					start = editor.getLineStartOffset(line-1);
					err = false;
					break;
				}
			}
			if (!err) {
				for (int i = 0; i < values.length; i++) {
					String value = (String)values[i];
					if (!value.equals("")) {
						String s = value.substring(0, value.length()-1);
						int line = Integer.parseInt(s);
						end = editor.getLineEndOffset(line-1);
						err2 = false;
					}
				}
				if (!err2) {
					editor.select(start, end);
				}
			}
			editor.grabFocus();
			numberList.removeSelectionInterval(numberList.getMinSelectionIndex(), numberList.getMaxSelectionIndex());
		}
	}
	
}
