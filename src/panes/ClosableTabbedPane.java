package panes;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

import java.util.Collections;

import javax.swing.event.EventListenerList;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

import panes.enent.*;


/**
 * A tabbed pane with closable tabs. The tab can have own icon and pop up menu (same for all).
 * 
 * @author Phil Tsarik
 * @version 0.5.5
 *
 */
public class ClosableTabbedPane extends JTabbedPane {
	
	/**
	 * Constructs a new instance of the <code>ClosableTabbedPane</code> class.
	 */
	public ClosableTabbedPane() {
		super();
		setFocusable(false);
		
		addMouseListener(new CloseButtonMouseEventsHandler());
		addMouseMotionListener(new CloseButtonMouseEventsHandler());
		addMouseListener(new MouseWheelEventsHandler());
		
		closableTabbedPaneListenerList = new EventListenerList();
		popupMenuHandler = new PopupMenuHandler();
		
		normalCloseIcon = null;
		hoverCloseIcon = null;
		pressedCloseIcon = null;
		closeButtonVisible = true;
		documentIconVisible = true;
		canCloseTabs = true;
		canCloseOnWheel = true;
		
		setClosableTabbedPaneUI();
	}
	
	/**
	 * Sets ClosableTabbedPaneUI.
	 */
	public void setClosableTabbedPaneUI() {
		setUI(new ClosableTabbedPaneUI());
	}
	
	/**
	 * Tests whether tabs of the instance of the <code>ClosableTabbedPane</code> can be closed.
	 * 
	 * @return <code>true</code> if tabs can be closed; <code>false</code> otherwise
	 */
	public boolean isCanCloseTabs() {
		return canCloseTabs;
	}
	
	/**
	 * Sets <code>canCloseTabs</code> property, which determines whether tabs of the instance of the <code>ClosableTabbedPane</code> can be closed.
	 * 
	 * @param canCloseTabs <code>true</code> to specify that this tab can be closed; <code>false</code> to specify that it cannot be closed
	 */
	public void setCanCloseTabs(boolean canCloseTabs) {
		this.canCloseTabs = canCloseTabs;
	}
	
	/**
	 * Tests whether the close button is visible on tabs of the instance of the <code>ClosableTabbedPane</code>.
	 * 
	 * @return <code>true</code> if the close button is visible on tabs; <code>false</code> otherwise
	 */
	public boolean isCloseButtonVisible() {
		return closeButtonVisible;
	}
	
	/**
	 * Sets <code>closeButtonVisible</code> property, which determines whether the close button is visible on tabs of the instance of the <code>ClosableTabbedPane</code>.
	 * 
	 * @param closeButtonVisible <code>true</code> to specify that the close button on tabs should be shown; <code>false</code> to specify that it should not be
	 */
	public void setCloseButtonVisible(boolean closeButtonVisible) {
		this.closeButtonVisible = closeButtonVisible;
	}
	
	/**
	 * Tests whether the document icon is visible on tabs of the instance of the <code>ClosableTabbedPane</code>.
	 * 
	 * @return <code>true</code> if the document icon is visible on tabs; <code>false</code> otherwise
	 */
	public boolean isDocumentIconVisible() {
		return documentIconVisible;
	}
	
	/**
	 * Sets <code>documentIconVisible</code> property, which determines whether the document icon is visible on tabs of the instance of the <code>ClosableTabbedPane</code>.
	 * 
	 * @param documentIconVisible <code>true</code> to specify that the document icon on tabs should be shown; <code>false</code> to specify that it should not be
	 */
	public void setDocumentIconVisible(boolean documentIconVisible) {
		this.documentIconVisible = documentIconVisible;
	}
	
	/**
	 * Tests whether tabs of the instance of the <code>ClosableTabbedPane</code> can be closed on mouse wheel click.
	 * 
	 * @return <code>true</code> if tabs can be closed on mouse wheel click; <code>false</code> otherwise
	 */
	public boolean isCanCloseOnWheel() {
		return canCloseOnWheel;
	}
	
	/**
	 * Sets <code>canCloseOnWheel</code> property, which determines whether tabs of the instance of the <code>ClosableTabbedPane</code> can be closed on mouse wheel click.
	 * 
	 * @param canCloseOnWheel <code>true</code> to specify that this tab can be closed on mouse wheel click; <code>false</code> to specify that it cannot be closed on mouse wheel click
	 */
	public void setCanCloseOnWheel(boolean canCloseOnWheel) {
		this.canCloseOnWheel = canCloseOnWheel;
	}
	
	/**
	 * Allows setting own close icons.
	 * 
	 * @param normal The normal icon
	 * @param hover The icon when the mouse is over
	 * @param pressed The icon when the mouse is pressed
	 */
	public void setCloseIcons(Icon normal, Icon hover, Icon pressed) {
		normalCloseIcon = normal;
		hoverCloseIcon = hover;
		pressedCloseIcon = pressed;
	}
	
	/**
	 * Gets a document icon of a tab with specified index.
	 * 
	 * @return The document icon
	 */
	public Icon getDocumentIconAt(int index) {
		try {
			TabIcon tabicon = (TabIcon)getIconAt(index);
			Icon docicon = tabicon.getDocumentIcon();
			return docicon;
		}
		catch (IndexOutOfBoundsException e) {
			return null;
		}
	}
	
	/**
	 * Sets a document icon on the selected tab.
	 * 
	 * @param documentIcon The document icon
	 */
	public void setDocumentIcon(Icon documentIcon) {
		int index = getSelectedIndex();
		if (index != -1) {
			setIconAt(index, new TabIcon(documentIcon));
		}
	}
	
	/**
	 * Sets a document icon.
	 * 
	 * @param index Tab index
	 * @param documentIcon The document icon
	 */
	public void setDocumentIcon(int index, Icon documentIcon) {
		if (index != -1) {
			setIconAt(index, new TabIcon(documentIcon));
		}
	}
	
	/**
	 * Sets a document icon.
	 * 
	 * @param comp Tab component
	 * @param documentIcon The document icon
	 */
	public void setDocumentIcon(Component comp, Icon documentIcon) {
		int index = indexOfComponent(comp);
		if (index != -1) {
			setIconAt(index, new TabIcon(documentIcon));
		}
	}
	
	/**
	 * Adds a closable tab with specified title and component.
	 * 
	 * @param title The title to be displayed in this tab
	 * @param comp The component to be displayed when this tab is clicked
	 */
	@Override
	public void addTab(String title, Component comp) {
		addTab(title, null, comp, null);
	}
	
	/**
	 * Adds a closable tab with specified title, document icon and component.
	 * 
	 * @param title The title to be displayed in this tab
	 * @param documentIcon The document icon to be displayed in this tab
	 * @param comp The component to be displayed when this tab is clicked
	 */
	@Override
	public void addTab(String title, Icon documentIcon, Component comp) {
		addTab(title, documentIcon, comp, null);
	}
	
	/**
	 * Adds a closable tab with specified title, document icon, component and tooltip.
	 * 
	 * @param title The title to be displayed in this tab
	 * @param documentIcon The document icon to be displayed in this tab
	 * @param comp The component to be displayed when this tab is clicked
	 * @param tip The tooltip to be displayed for this tab
	 */
	@Override
	public void addTab(String title, Icon documentIcon, Component comp, String tip) {
		insertTab(title, new TabIcon(documentIcon), comp, tip, getTabCount());
	}
	
	/**
	 * Inserts a closable tab with specified title, document icon, component and tooltip at specified position.
	 * 
	 * @param title The title to be displayed in this tab
	 * @param documentIcon The document icon to be displayed in this tab
	 * @param comp The component to be displayed when this tab is clicked
	 * @param tip The tooltip to be displayed for this tab
	 * @param index The position to insert this new tab
	 */
	@Override
	public void insertTab(String title, Icon documentIcon, Component comp, String tip, int index) {
		super.insertTab(title, new TabIcon(documentIcon), comp, tip, index);
		fireTabCreated(comp, indexOfComponent(comp));
		setSelectedComponent(comp);
	}
	
	/**
	 * Closes selected tab.
	 * 
	 * @return <code>true</code> if the tab has been closed; <code>false</code> otherwise
	 */
	public boolean closeTab() {
		int index = getSelectedIndex();
		if (index < 0) {
			return false;
		}
		fireTabClosing(getSelectedComponent(), index);
		if (canCloseTabs) {
			remove(index);
			fireTabClosed(getSelectedComponent(), getSelectedIndex());
		}
		return canCloseTabs;
	}
	
	/**
	 * Closes tab with specified index.
	 * 
	 * @param index The index of the tab
	 * @return <code>true</code> if the tab has been closed; <code>false</code> otherwise
	 */
	public boolean closeTab(int index) {
		if (index < 0) {
			return false;
		}
		fireTabClosing(getComponentAt(index), index);
		if (canCloseTabs) {
			remove(index);
			fireTabClosed(getSelectedComponent(), getSelectedIndex());
		}
		return canCloseTabs;
	}
	
	/**
	 * Closes tab with specified component.
	 * 
	 * @param comp The component of the tab
	 * @return <code>true</code> if the tab has been closed; <code>false</code> otherwise
	 */
	public boolean closeTab(Component comp) {
		int index = indexOfComponent(comp);
		if (index < 0) {
			return false;
		}
		fireTabClosing(comp, index);
		if (canCloseTabs) {
			remove(comp);
			fireTabClosed(getSelectedComponent(), getSelectedIndex());
		}
		return canCloseTabs;
	}
	
	/**
	 * Selects the next tab.
	 */
	public void selectNextTab() {
		int sel = getSelectedIndex();
		if (sel != -1) {
			sel = (sel + 1) % getTabCount();
			setSelectedIndex(sel);
		}
	}
	
	/**
	 * Selects the previous tab.
	 */
	public void selectPreviousTab() {
		int sel = getSelectedIndex();
		if (sel != -1) {
			sel--;
			if (sel < 0) {
				sel = getTabCount()-1;
			}
			setSelectedIndex(sel);
		}
	}
	
	/**
	 * Moves the selected tab left.
	 */
	public void moveLeft() {
		int sel = getSelectedIndex();
		if (sel != -1) {
			String title = getTitleAt(sel);
			Icon icon = getIconAt(sel);
			Component comp = getComponentAt(sel);
			String tip = getToolTipTextAt(sel);
			int nsel = sel - 1;
			if (nsel < 0) {
				remove(0);
				super.insertTab(title, icon, comp, tip, getTabCount());
				setSelectedComponent(comp);
			} else {
				super.insertTab(getTitleAt(nsel), getIconAt(nsel), getComponentAt(nsel), getToolTipTextAt(nsel), sel);
				super.insertTab(title, icon, comp, tip, nsel);
				setSelectedComponent(comp);
			}
		}
	}
	
	/**
	 * Moves the selected tab right.
	 */
	public void moveRight() {
		int sel = getSelectedIndex();
		if (sel != -1) {
			String title = getTitleAt(sel);
			Icon icon = getIconAt(sel);
			Component comp = getComponentAt(sel);
			String tip = getToolTipTextAt(sel);
			int nsel = sel + 1;
			if (nsel >= getTabCount()) {
				remove(getTabCount()-1);
				super.insertTab(title, icon, comp, tip, 0);
				setSelectedComponent(comp);
			} else {
				super.insertTab(getTitleAt(nsel), getIconAt(nsel), getComponentAt(nsel), getToolTipTextAt(nsel), sel);
				super.insertTab(title, icon, comp, tip, nsel);
				setSelectedComponent(comp);
			}
		}
	}
	
	/**
	 * Adds the given observer to begin receiving notifications when tabs is closing or creating.
	 * 
	 * @param l The observer to register
	 */
	public void addClosableTabbedPaneListener(ClosableTabbedPaneListener l) {
		closableTabbedPaneListenerList.add(ClosableTabbedPaneListener.class, l);
	}
	
	/**
	 * Unregisters the given observer so it will no longer receive events.
	 * 
	 * @param l The observer to unregister
	 */
	public void removeClosableTabbedPaneListener(ClosableTabbedPaneListener l) {
		closableTabbedPaneListenerList.remove(ClosableTabbedPaneListener.class, l);
	}
	
	/**
	 * Gets all listeners that have registered.
	 * 
	 * @return all listeners that have registered
	 */
	public ClosableTabbedPaneListener[] getClosableTabbedPaneListeners() {
		return closableTabbedPaneListenerList.getListeners(ClosableTabbedPaneListener.class);
	}
	
	/**
	 * Notifies all listeners that have registered interest for notification on
	 * this event type.
	 * 
	 * @param comp The <code>Component</code>
	 * @param index Index of the component on the <code>ClosableTabbedPane</code>
	 */
	protected void fireTabCreated(Component comp, int index) {
		// guaranteed to return a non-null array
		Object[] listeners = closableTabbedPaneListenerList.getListenerList();
		for (Object i : listeners) {
			if (i instanceof ClosableTabbedPaneListener) {
				((ClosableTabbedPaneListener)i).tabCreated(new ClosableTabbedPaneEvent(comp, index));
			}
		}
	}
	
	/**
	 * Notifies all listeners that have registered interest for notification on
	 * this event type.
	 * 
	 * @param comp The <code>Component</code>
	 * @param index Index of the component on the <code>ClosableTabbedPane</code>
	 */
	protected void fireTabClosing(Component comp, int index) {
		// guaranteed to return a non-null array
		Object[] listeners = closableTabbedPaneListenerList.getListenerList();
		for (Object i : listeners) {
			if (i instanceof ClosableTabbedPaneListener) {
				((ClosableTabbedPaneListener)i).tabClosing(new ClosableTabbedPaneEvent(comp, index));
			}
		}
	}
	
	/**
	 * Notifies all listeners that have registered interest for notification on
	 * this event type.
	 * 
	 * @param comp The <code>Component</code>
	 * @param index Index of the component on the <code>ClosableTabbedPane</code>
	 */
	protected void fireTabClosed(Component comp, int index) {
		// guaranteed to return a non-null array
		Object[] listeners = closableTabbedPaneListenerList.getListenerList();
		for (Object i : listeners) {
			if (i instanceof ClosableTabbedPaneListener) {
				((ClosableTabbedPaneListener)i).tabClosed(new ClosableTabbedPaneEvent(comp, index));
			}
		}
	}
	
	/**
	 * Sets pop up menu on tabs.
	 * 
	 * @param p Pop up menu. If <code>p</code> is <code>null</code> the pop up menu listener will be removed
	 */
	public void setPopupMenu(JPopupMenu p) {
		if (p != null) {
			popup = p;
			this.addMouseListener(popupMenuHandler);
		} else {
			this.removeMouseListener(popupMenuHandler);
		}
	}
	
	private Icon normalCloseIcon;
	private Icon hoverCloseIcon;
	private Icon pressedCloseIcon;
	
	private boolean closeButtonVisible;
	private boolean documentIconVisible;
	private boolean canCloseTabs;
	private boolean canCloseOnWheel;
	
	private JPopupMenu popup;
	private EventListenerList closableTabbedPaneListenerList;
	private PopupMenuHandler popupMenuHandler;
	
	/**
	 * Processes mouse events to provide close button features
	 * 
	 * @param evt Mouse event
	 */
	private void processMouseEvents(MouseEvent evt) {
		for (int i = 0; i < getTabCount(); i++) {
			TabIcon tabicon = (TabIcon)getIconAt(i);
			if (tabicon != null) {
				tabicon.mouseOverTab = false;
			}
		}
		repaint();
		int index = indexAtLocation(evt.getX(), evt.getY());
		if (index < 0) return;
		TabIcon icon = (TabIcon)getIconAt(index);
		int selIndex = getSelectedIndex();
		if (icon != null) {
			if (selIndex == index) {
				icon.mouseOverTab = true;
			}
			Rectangle rect = icon.getBounds();
			Point pos = new Point();
			if (evt.getID() == MouseEvent.MOUSE_PRESSED) {
				icon.mousePressed = evt.getModifiers() == MouseEvent.BUTTON1_MASK;
				repaint(rect);
				return;
			}
			if (evt.getID() == MouseEvent.MOUSE_MOVED || evt.getID() == MouseEvent.MOUSE_DRAGGED || evt.getID() == MouseEvent.MOUSE_CLICKED) {
				pos.x = evt.getX();
				pos.y = evt.getY();
				if (rect.contains(pos)) {
					if (evt.getID() == MouseEvent.MOUSE_CLICKED) {
						if (canCloseTabs) {
							if (selIndex > 0) {
								Rectangle rec = getUI().getTabBounds(this, selIndex - 1);
								MouseEvent event = new MouseEvent((Component) evt.getSource(),
										evt.getID() + 1,
										System.currentTimeMillis(),
										evt.getModifiers(),
										rec.x,
										rec.y,
										evt.getClickCount(),
										evt.isPopupTrigger(),
										evt.getButton());
								dispatchEvent(event);
							}
							closeTab(selIndex);
						} else {
							icon.mouseOver = false;
							icon.mousePressed = false;
							repaint(rect);
						}
					} else {
						icon.mouseOver = true;
						icon.mousePressed = evt.getModifiers() == MouseEvent.BUTTON1_MASK;
					}
				} else {
					icon.mouseOver = false;
				}
				repaint();
			}
		}
	}
	
	
	/**
	 * Icon which is placed to the <code>ClosableTabbedPane</code>'s tabs.
	 * Consists close button icons and document icon.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class TabIcon implements Icon {
		
		public TabIcon(Icon documentIcon) {
			width = 16;
			height = 16;
			this.documentIcon = documentIcon;
			mouseOver = false;
			mousePressed = false;
			mouseOverTab = false;
		}
		
		@Override
		public int getIconWidth() {
			int w = width;
			if (!closeButtonVisible) {
				w = 0;
			}
			if ( (documentIcon != null) && (documentIconVisible) ) {
				w += documentIcon.getIconWidth();
			}
			return w;
		}
		
		@Override
		public int getIconHeight() {
			return height;
		}
		
		@Override
		public void paintIcon(Component c, Graphics g, int x, int y) {
			this.x = x;
			this.y = y;
			if ( (documentIcon != null) && (documentIconVisible) ) {
				int w = width;
				if (!closeButtonVisible) {
					w = 0;
				}
				documentIcon.paintIcon(c, g, x+w+2, y);
			}
			if (closeButtonVisible) {
				if ( (normalCloseIcon != null) && (!mouseOver) ) {
					normalCloseIcon.paintIcon(c, g, x, y);
					return;
				}
				if ( (hoverCloseIcon != null) && (mouseOver) && (!mousePressed) ) {
					hoverCloseIcon.paintIcon(c, g, x, y);
					return;
				}
				if ( (pressedCloseIcon != null) && (mouseOver) && (mousePressed) ) {
					pressedCloseIcon.paintIcon(c, g, x, y);
					return;
				}
				x += 2;
				y += 4;
				
				Color col = g.getColor();
				
				if (mouseOverTab) {
					if (mousePressed && mouseOver) {
						x += 1;
						y += 1;
					}
					g.setColor(Color.BLACK);
					g.drawLine(x, y, x+9, y+9);
					g.drawLine(x, y+2, x+7, y+9);
					g.drawLine(x, y+1, x+8, y+9);
					g.drawLine(x+1, y, x+9, y+8);
					g.drawLine(x+2, y, x+9, y+7);
					
					g.drawLine(x+9, y, x, y+9);
					g.drawLine(x+9, y+2, x+2, y+9);
					g.drawLine(x+9, y+1, x+1, y+9);
					g.drawLine(x+8, y, x, y+8);
					g.drawLine(x+7, y, x, y+7);
					
					if (mouseOver) {
						g.setColor(Color.RED);
					} else {
						g.setColor(Color.GRAY);
					}
					g.drawLine(x+1, y+1, x+8, y+8);
					g.drawLine(x+1, y+2, x+7, y+8);
					g.drawLine(x+2, y+1, x+8, y+7);
					g.drawLine(x+8, y+1, x+1, y+8);
					g.drawLine(x+7, y+1, x+1, y+7);
					g.drawLine(x+8, y+2, x+2, y+8);
				} else {
					if (mouseOver) {
						g.setColor(Color.BLACK);
					} else {
						g.setColor(Color.GRAY);
					}
					g.drawLine(x+1, y+1, x+8, y+8);
					g.drawLine(x+1, y+2, x+7, y+8);
					g.drawLine(x+2, y+1, x+8, y+7);
					g.drawLine(x+8, y+1, x+1, y+8);
					g.drawLine(x+7, y+1, x+1, y+7);
					g.drawLine(x+8, y+2, x+2, y+8);
				}
				
				g.setColor(col);
			}
		}
		
		public Rectangle getBounds() {
			return new Rectangle(x, y, width, height);
		}
		
		public Icon getDocumentIcon() {
			return documentIcon;
		}
		
		private int x;
		private int y;
		private int width;
		private int height;
		private Icon documentIcon;
		private boolean mouseOver;
		private boolean mousePressed;
		private boolean mouseOverTab;
		
	}
	
	/**
	 * Provides to receive mouse events on the tabs.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class CloseButtonMouseEventsHandler implements MouseListener, MouseMotionListener{
		
		//@Override
		public void mouseClicked(MouseEvent evt) {
			processMouseEvents(evt);
		}
		
		//@Override
		public void mouseDragged(MouseEvent evt) {
			processMouseEvents(evt);
		}
		
		//@Override
		public void mouseExited(MouseEvent evt) {
			for (int i = 0; i < getTabCount(); i++) {
				TabIcon icon = (TabIcon)getIconAt(i);
				if (icon != null) {
					icon.mouseOver = false;
					icon.mouseOverTab = false;
				}
			}
			repaint();
		}
		
		//@Override
		public void mouseMoved(MouseEvent evt) {
			processMouseEvents(evt);
		}
		
		//@Override
		public void mousePressed(MouseEvent evt) {
			processMouseEvents(evt);
		}
		
		//@Override
		public void mouseEntered(MouseEvent e) {}
		
		//@Override
		public void mouseReleased(MouseEvent e) {}
		
	}
	
	/**
	 * Provides pop up menu showing on tabs.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class PopupMenuHandler extends MouseAdapter {
		
		@Override
		public void mousePressed(MouseEvent evt) {
			// if right mouse button has been pressed
			if (evt.isPopupTrigger()) {
				// show pop up menu
				int i = indexAtLocation(evt.getX(), evt.getY());
				if (i != -1) {
					popup.show(ClosableTabbedPane.this, evt.getX(), evt.getY());
				}
			}
		}
		
		@Override
		public void mouseReleased(MouseEvent evt) {
			// if right mouse button was pressed
			if (evt.isPopupTrigger()) {
				// show pop up menu
				int i = indexAtLocation(evt.getX(), evt.getY());
				if (i != -1) {
					popup.show(ClosableTabbedPane.this, evt.getX(), evt.getY());
				}
			}
		}
		
	}
	
	/**
	 * Provides closing tabs on mouse wheel click.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class MouseWheelEventsHandler extends MouseAdapter {
		
		@Override
		public void mouseReleased(MouseEvent evt) {
			// if middle mouse button has been pressed
			if ( (evt.getButton() == MouseEvent.BUTTON2) && (canCloseOnWheel) ) {
				// close this tab
				int i = indexAtLocation(evt.getX(), evt.getY());
				if (i != -1) {
					closeTab(i);
				}
			}
		}
		
	}
	
	/**
	 * A specific <code>BasicTabbedPaneUI</code>.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class ClosableTabbedPaneUI extends BasicTabbedPaneUI {
		
		@Override
		protected void layoutLabel(int tabPlacement, FontMetrics metrics,
				int tabIndex, String title, Icon icon,
				Rectangle tabRect, Rectangle iconRect,
				Rectangle textRect, boolean isSelected) {
			
			textRect.x = textRect.y = iconRect.x = iconRect.y = 0;
			
			javax.swing.text.View v = getTextViewForTab(tabIndex);
			if (v != null) {
				tabPane.putClientProperty("html", v);
			}
			
			SwingUtilities.layoutCompoundLabel((JComponent) tabPane,
					metrics, title, icon,
					SwingUtilities.CENTER,
					SwingUtilities.CENTER,
					SwingUtilities.CENTER,
					SwingUtilities.RIGHT,
					tabRect,
					iconRect,
					textRect,
					textIconGap + 2);
			
			tabPane.putClientProperty("html", null);
			
			int xNudge = getTabLabelShiftX(tabPlacement, tabIndex, isSelected);
			int yNudge = getTabLabelShiftY(tabPlacement, tabIndex, isSelected);
			iconRect.x += xNudge;
			iconRect.y += yNudge;
			textRect.x += xNudge;
			textRect.y += yNudge;
		}
		
	}
	
}
