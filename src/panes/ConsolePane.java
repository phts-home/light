package panes;

import utilities.*;

import javax.swing.*;
import javax.swing.event.*;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;


/**
 * Console pane component.
 * 
 * @author Phil Tsarik
 * @version 0.7.1
 *
 */
public class ConsolePane extends JScrollPane implements ProgramConsole {
	
	/**
	 * Constructs a new instance of the <code>ConsolePane</code> class.
	 */
	public ConsolePane() {
		popupMenuHandler = new PopupMenuHandler();
		textarea = new JTextArea();
		textarea.setEditable(false);
		canWrite = true;
		getViewport().add(textarea);
	}
	
	@Override
	public void write(String line) {
		if (canWrite) {
			textarea.append(line);
			textarea.setCaretPosition(textarea.getText().length());
		}
	}
	
	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		textarea.setCaretPosition(textarea.getText().length());
	}
	
	/**
	 * Sets background color.
	 * 
	 * @param backgroundColor Background color to set
	 */
	public void setBackgroundColor(Color backgroundColor) {
		textarea.setBackground(backgroundColor);
	}
	
	/**
	 * Sets text area font.
	 * 
	 * @param font Text area font
	 */
	public void setTextAreaFont(Font font) {
		textarea.setFont(font);
	}
	
	/**
	 * Sets text area font.
	 * 
	 * @param font Text area font
	 * @param fontColor Text area font color
	 */
	public void setTextAreaFont(Font font, Color fontColor) {
		textarea.setFont(font);
		textarea.setForeground(fontColor);
	}
	
	/**
	 * Clears text in the console pane
	 */
	public void clear() {
		textarea.setText("");
	}
	
	/**
	 * Copies text in the console pane
	 */
	public void copy() {
		textarea.copy();
	}
	
	/**
	 * Tests whether text can be written to the console pane.
	 * 
	 * @return <code>true</code> if text can be written to the console pane; <code>false</code> otherwise
	 */
	public boolean isCanWrite() {
		return canWrite;
	}
	
	/**
	 * Sets the <code>canWrite</code> property which determines whether text can be written to the console pane.
	 * 
	 * @param canWrite <code>true</code> to allow to write text; <code>false</code> to forbid to write
	 */
	public void setCanWrite(boolean canWrite) {
		this.canWrite = canWrite;
	}
	
	/**
	 * Sets pop up menu.
	 * 
	 * @param p Pop up menu. If <code>p</code> is <code>null</code> the pop up menu listener will be removed
	 */
	public void setPopupMenu(JPopupMenu p) {
		if (p != null) {
			popup = p;
			textarea.addMouseListener(popupMenuHandler);
		} else {
			textarea.removeMouseListener(popupMenuHandler);
		}
	}
	
	
	private JTextArea textarea;
	private JPopupMenu popup;
	private PopupMenuHandler popupMenuHandler;
	private boolean canWrite;
	
	
	/**
	 * Provides pop up menu showing.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class PopupMenuHandler extends MouseAdapter {
		
		@Override
		public void mousePressed(MouseEvent evt) {
			// if right mouse button was pressed
			if (evt.isPopupTrigger()) {
				// show pop up menu
				popup.show(evt.getComponent(), evt.getX(), evt.getY());
			}
		}
		
		@Override
		public void mouseReleased(MouseEvent evt) {
			// if right mouse button was pressed
			if (evt.isPopupTrigger()) {
				// show pop up menu
				popup.show(evt.getComponent(), evt.getX(), evt.getY());
			}
		}
		
	}
	
}
