package preferences;

import documents.SimpleXMLDocument;


/**
 * Interface for object witch can load or save some preferences.
 * 
 * @author Phil Tsarik
 *
 */
public interface PreferencesCustomizable {
	
	/**
	 * Loads preferences from document.
	 * 
	 * @param doc XML document associated with this preferences
	 */
	public void loadPreferences(SimpleXMLDocument doc);
	
	/**
	 * Saves preferences to document.
	 * 
	 * @param doc XML document associated with this preferences
	 */
	public void savePreferences(SimpleXMLDocument doc);
	
}
