package components;

import javax.swing.tree.*;


/**
 * The <code>FileTree</code>'s node class.
 * 
 * @author Phil Tsarik
 * @version 1.0.0
 *
 */
public class FileTreeNode extends DefaultMutableTreeNode implements Comparable<FileTreeNode> {
	
	/**
	 * Constructs a new instance of the <code>FileTreeNode</code> class.
	 */
	public FileTreeNode() {
		super();
		this.fileTreeItem = null;
	}
	
	/**
	 * Constructs a new instance of the <code>FileTreeNode</code> class.
	 * 
	 * @param fileTreeItem The <code>FileTreeItem</code> instance which keeps information about node's content.
	 */
	public FileTreeNode(FileTreeItem fileTreeItem) {
		super(fileTreeItem);
		this.fileTreeItem = fileTreeItem;
	}
	
	/**
	 * Constructs a new instance of the <code>FileTreeNode</code> class.
	 * 
	 * @param fileTreeItem The FileTreeItem instance which keeps information about node's content.
	 * @param allowsChildren <code>true</code> if each node is asked to see if it can have children, <code>false</code> if any node can have children
	 */
	public FileTreeNode(FileTreeItem fileTreeItem, boolean allowsChildren) {
		super(fileTreeItem, allowsChildren);
		this.fileTreeItem = fileTreeItem;
	}
	
	/**
	 * Gets the <code>FileTreeItem</code> instance.
	 * 
	 * @return The <code>FileTreeItem</code> instance
	 */
	public FileTreeItem getFileTreeItem() {
		return fileTreeItem;
	}
	
	@Override
	public int compareTo(FileTreeNode o) {
		if (fileTreeItem == null) {
			return -1;
		}
		if (o.getFileTreeItem() == null) {
			return 1;
		}
		if (o.getFileTreeItem().isDir() != fileTreeItem.isDir()) {
			if (fileTreeItem.isDir()) {
				return -1;
			} else {
				return 1;
			}
		}
		String name2 = o.getFileTreeItem().toString();
		return fileTreeItem.toString().compareTo(name2);
	}
	
	private FileTreeItem fileTreeItem;
	
}
