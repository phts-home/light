package components;

import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.io.*;
import java.util.*;


/**
 * A component which displays a file tree with directories and files as nodes.
 * It uses <code>FileTreeNode</code> and <code>FileTreeItem</code> classes.<br><br>
 * This component builds a file tree relative to some parent file (directory), showing
 * a root node as the parent directory or a directory which contains the parent file.<br><br>
 * Properties <code>depth</code> and <code>sorted</code> determine a nesting depth
 * in tree and whether should be the tree sorted respectively.<br><br>
 * You can also set your own icons of file, folder and expanded folder nodes.
 * 
 * @author Phil Tsarik
 * @version 1.0.0
 *
 */
public class FileTree extends JTree {
	
	/**
	 * Constructs a new instance of the <code>FileTree</code> class.
	 */
	public FileTree() {
		this(null, null, -1, false, null, null, null);
	}
	
	/**
	 * Constructs a new instance of the <code>FileTree</code> class.
	 * 
	 * @param parentFile Parent file or directory
	 * @param rootName Root name
	 * @param depth Depth property
	 * @param sorted Sorted property
	 * @param fileIcon File node icon
	 * @param folderIcon Folder node icon
	 * @param expandedIcon Expanded folder node icon
	 */
	public FileTree(File parentFile, String rootName, int depth, boolean sorted, Icon fileIcon, Icon folderIcon, Icon expandedIcon) {
		super();
		filesAndFoldersRenderer = new FilesAndFoldersRenderer(fileIcon, folderIcon, expandedIcon);
		this.setCellRenderer(filesAndFoldersRenderer);
		tm = new DefaultTreeModel(null, false);
		this.setModel(tm);
		this.depth = depth;
		this.sorted = sorted;
		setParentFile(parentFile);
	}
	
	/**
	 * Gets the parent file or directory.
	 * 
	 * @return The parent file or directory
	 */
	public File getParentFile() {
		return parentFile;
	}
	
	/**
	 * Sets the parent file or directory.
	 * 
	 * @param parentFile The parent file or directory
	 */
	public void setParentFile(File parentFile) {
		this.parentFile = parentFile;
		setParentDir(parentFile);
		refresh();
	}
	
	/**
	 * Gets the <code>depth</code> property.
	 * 
	 * @return The <code>depth</code> property
	 */
	public int getDepth() {
		return depth;
	}
	
	/**
	 * Sets the <code>depth</code> property.
	 * 
	 * @param depth The <code>depth</code> property
	 */
	public void setDepth(int depth) {
		this.depth = depth;
		refresh();
	}
	
	/**
	 * Gets the <code>sorted</code> property.
	 * 
	 * @return <code>true</code> if the <code>sorted</code> property is <code>true</code>
	 */
	public boolean isSorted() {
		return sorted;
	}
	
	/**
	 * Sets the <code>sorted</code> property.
	 * 
	 * @param sorted The <code>sorted</code> property
	 */
	public void setSorted(boolean sorted) {
		this.sorted = sorted;
		refresh();
	}
	
	/**
	 * Sets both the <code>depth</code> and the <code>sorted</code> properties.
	 * 
	 * @param depth The <code>depth</code> property
	 * @param sorted The <code>sorted</code> property
	 */
	public void setDepthAndSorted(int depth, boolean sorted) {
		this.depth = depth;
		this.sorted = sorted;
		refresh();
	}
	
	/**
	 * Gets the custom file icon.
	 * 
	 * @return The custom file icon
	 */
	public Icon getFileIcon() {
		return filesAndFoldersRenderer.getFileIcon();
	}
	
	/**
	 * Sets the custom file icon. If <code>fileIcon</code> is <code>null</code> then will be shown default icon.
	 * 
	 * @param fileIcon The custom file icon
	 */
	public void setFileIcon(Icon fileIcon) {
		filesAndFoldersRenderer.setFileIcon(fileIcon);
	}
	
	/**
	 * Gets the custom folder icon.
	 * 
	 * @return The custom folder icon
	 */
	public Icon getFolderIcon() {
		return filesAndFoldersRenderer.getFolderIcon();
	}
	
	/**
	 * Sets the custom folder icon. If <code>folderIcon</code> is <code>null</code> then will be shown default icon.
	 * 
	 * @param folderIcon The custom folder icon
	 */
	public void setFolderIcon(Icon folderIcon) {
		filesAndFoldersRenderer.setFolderIcon(folderIcon);
	}
	
	/**
	 * Gets the custom expanded folder icon.
	 * 
	 * @return The custom expanded folder icon
	 */
	public Icon getExpandedIcon() {
		return filesAndFoldersRenderer.getExpandedIcon();
	}
	
	/**
	 * Sets the custom expanded folder icon. If <code>expandedIcon</code> is <code>null</code> then will be shown default icon.
	 * 
	 * @param expandedIcon The custom expanded folder icon
	 */
	public void setExpandedIcon(Icon expandedIcon) {
		filesAndFoldersRenderer.setExpandedIcon(expandedIcon);
	}
	
	@Override
	public void setFont(Font font) {
		super.setFont(font);
		if (filesAndFoldersRenderer != null) {
			filesAndFoldersRenderer.setFont(font);
		}
	}
	
	@Override
	public void setBackground(Color backgroundColor) {
		super.setBackground(backgroundColor);
		if (filesAndFoldersRenderer != null) {
			filesAndFoldersRenderer.setBackgroundNonSelectionColor(backgroundColor);
		}
	}
	
	@Override
	public void setForeground(Color foregroundColor) {
		super.setForeground(foregroundColor);
		if (filesAndFoldersRenderer != null) {
			filesAndFoldersRenderer.setTextNonSelectionColor(foregroundColor);
		}
	}
	
	/**
	 * Sets the background selection color.
	 * 
	 * @param backgroundSelectionColor The background selection color
	 */
	public void setBackgroundSelectionColor(Color backgroundSelectionColor) {
		if (filesAndFoldersRenderer != null) {
			filesAndFoldersRenderer.setBackgroundSelectionColor(backgroundSelectionColor);
		}
	}
	
	/**
	 * Sets the border selection color.
	 * 
	 * @param borderSelectionColor The border selection color
	 */
	public void setBorderSelectionColor(Color borderSelectionColor) {
		if (filesAndFoldersRenderer != null) {
			filesAndFoldersRenderer.setBorderSelectionColor(borderSelectionColor);
		}
	}
	
	/**
	 * Sets the text selection color.
	 * 
	 * @param textSelectionColor The text selection color
	 */
	public void setTextSelectionColor(Color textSelectionColor) {
		if (filesAndFoldersRenderer != null) {
			filesAndFoldersRenderer.setTextSelectionColor(textSelectionColor);
		}
	}
	
	/**
	 * Refreshes the tree. If <code>parentFile</code> or <code>rootName<code> is <code>null</code>
	 * the the tree will be cleared.
	 */
	public void refresh() {
		if ( (parentFile == null) || (parentDir == null) || (!parentFile.exists()) || (!parentDir.exists()) ) {
			clear();
			return;
		}
		FileTreeNode rootNode = new FileTreeNode(new FileTreeItem(parentDir, true));
		tm.setRoot(rootNode);
		createNodes(rootNode, parentDir.getAbsoluteFile(), 0);
		this.expandRow(0);
	}
	
	/**
	 * Clears the tree.
	 */
	public void clear() {
		tm.setRoot(null);
	}
	
	/**
	 * Gets the selected FileTreeNode.
	 * 
	 * @return The selected FileTreeNode
	 */
	public FileTreeNode getSelectedFileTreeNode() {
		FileTreeNode node = (FileTreeNode)this.getLastSelectedPathComponent();
		return node;
	}
	
	
	private File parentFile;
	private File parentDir;
	private DefaultTreeModel tm;
	private FilesAndFoldersRenderer filesAndFoldersRenderer;
	private int depth;
	private boolean sorted;
	
	
	private void setParentDir(File parentFile) {
		if ( (parentFile == null) || (parentFile.isDirectory()) ) {
			parentDir = parentFile;
		} else {
			if (parentFile.getParent() == null) {
				return;
			}
			parentDir = new File(parentFile.getParent());
		}
	}
	
	private void createNodes(DefaultMutableTreeNode top, File parent, int level) {
		if ( (depth > 0) && (level > depth) ) {
			return;
		}
		String s[] = parent.list();
		ArrayList<FileTreeNode> list = new ArrayList<FileTreeNode>();
		for (int i = 0; i < s.length; i++) {
			File f = new File(parent.getAbsolutePath() + "/" + s[i]);
			FileTreeNode dirNode = new FileTreeNode(new FileTreeItem(f, false));
			if ( (f.isDirectory()) && ((depth == -1) || (level < depth)) ) {
				createNodes(dirNode, f, level+1);
			}
			if ( (!f.isDirectory()) || ((f.isDirectory() && (level < depth))) ) {
				list.add(dirNode);
			}
		}
		if (sorted) {
			Collections.sort(list);
		}
		for (int i = 0; i < list.size(); i++) {
			top.add(list.get(i));
		}
		list = null;
	}
	
	
	private class FilesAndFoldersRenderer extends DefaultTreeCellRenderer {
		
		public FilesAndFoldersRenderer(Icon fileIcon, Icon folderIcon, Icon expandedIcon) {
			this.fileIcon = fileIcon;
			this.folderIcon = folderIcon;
			this.expandedIcon = expandedIcon;
		}
		
		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
			if (value instanceof FileTreeNode) {
				FileTreeNode node = (FileTreeNode)value;
				if (node == null) {
					return null;
				}
				FileTreeItem item = node.getFileTreeItem();
				if (item == null) {
					return null;
				}
				if (item.isDir()) {
					if (!expanded) {
						if (folderIcon != null) {
							setIcon(folderIcon);
						}
					} else {
						if (expandedIcon != null) {
							setIcon(expandedIcon);
						}
					}
				} else {
					if (fileIcon != null) {
						setIcon(fileIcon);
					}
				}
			}
			return this;
		}
		
		public Icon getFileIcon() {
			return fileIcon;
		}
		
		public void setFileIcon(Icon fileIcon) {
			this.fileIcon = fileIcon;
		}
		
		public Icon getFolderIcon() {
			return folderIcon;
		}
		
		public void setFolderIcon(Icon folderIcon) {
			this.folderIcon = folderIcon;
		}
		
		public Icon getExpandedIcon() {
			return folderIcon;
		}
		
		public void setExpandedIcon(Icon expandedIcon) {
			this.expandedIcon = expandedIcon;
		}
		
		private Icon fileIcon;
		private Icon folderIcon;
		private Icon expandedIcon;
		
	}
	
}
