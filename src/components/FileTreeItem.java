package components;

import java.io.File;


/**
 * Contains information about <code>FileTree</code>'s node.
 * 
 * @author Phil Tsarik
 * @version 1.0.0
 *
 */
public class FileTreeItem {
	
	/**
	 * Constructs a new instance of the <code>FileTreeItem</code> class.
	 * 
	 * @param f The file which corresponds to this item
	 * @param root <code>True</code> if this item is root tree element
	 */
	public FileTreeItem(File f, boolean root) {
		this.f = f;
		this.root = root;
	}
	
	/**
	 * Gets file.
	 * 
	 * @return The file
	 */
	public File getFile() {
		return f;
	}
	
	/**
	 * Tests whether this item is directory.
	 * 
	 * @return <code>true</code> if this item is directory; <code>false</code> if it is not directory
	 */
	public boolean isDir() {
		return (f.isDirectory() || root);
	}
	
	@Override
	public String toString() {
		if (root) {
			return f.getAbsolutePath();
		}
		return f.getName();
	}
	
	private File f;
	private boolean root;
	
}
