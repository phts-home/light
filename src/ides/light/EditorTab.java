package ides.light;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.*;
import javax.swing.event.*;

import ides.light.parameters.*;
import ides.light.profiles.LightDocument;

import preferences.PreferencesApplicable;

import panes.*;
import editors.*;
import editors.event.*;

import java.io.*;


/**
 * The tab with a <code>CodeEditor</code>'s and <code>LineNumbersPane</code> instances and some functions.
 * 
 * @author Phil Tsarik
 *
 */
public class EditorTab extends JPanel implements PreferencesApplicable {
	
	/**
	 * Constructs a new instance of the <code>EditorTab</code> class.
	 * 
	 * @param owner The program's main frame
	 */
	public EditorTab(MainFrame owner) {
		super(new BorderLayout());
		// init some variables
		this.owner = owner;
		this.searchingText = "";
		this.saved = false;
		this.openedFile = new File("");
		
		// create components
		createComponents();
		owner.getPreferences().addApplicable(this);
		
		// disable interception of ctrl+tab and shift+ctrl+tab by JComponent
		setFocusTraversalKeysEnabled(false);
		lineNumbersPane.setFocusTraversalKeysEnabled(false);
	}
	
	@Override
	public void applyPreferences() {
		lineNumbersPane.setEditorFont(Parameters.EDITOR_FONT_APPEARANCE, Parameters.EDITOR_FONT_COLOR);
		lineNumbersPane.setEditorBackgroundColor(Parameters.EDITOR_BACKGR_COLOR);
		editor.setCaretColor(Parameters.EDITOR_CURSOR_COLOR);
		editor.setSelectionColor(Parameters.EDITOR_SELECTION_COLOR);
		editor.setSelectedTextColor(Parameters.EDITOR_SELECTEDTEXT_COLOR);
		lineNumbersPane.setLineNumbersBackgroundColor(Parameters.EDITOR_LINENUMBERS_BACKGR_COLOR);
		lineNumbersPane.setLineNumbersFontColor(Parameters.EDITOR_LINENUMBERS_FOREGR_COLOR);
		editor.setLineWrap(Parameters.EDITOR_LINEWRAP_ENABLE);
		editor.setTabSize(Parameters.EDITOR_TAB_SIZE);
		lineNumbersPane.setLineNumbersVisible(Parameters.EDITOR_LINENUMBERS_VISIBLE);
		editor.setInsertSpaces(Parameters.EDITOR_TAB_INSERTSPACES_ENABLE);
		editor.setAutoDetectType(Parameters.GENERAL_AUTODETECTTYPE_ENABLE);
	}
	
	/**
	 * Gets a currently opened file.
	 * 
	 * @return Path of a currently opened file
	 */
	public File getOpenedFile() {
		return openedFile;
	}
	
	/**
	 * Gets placed on the tab an instance of the <code>CodeEditor</code>.
	 * 
	 * @return A placed on frame the instance of the <code>CodeEditor</code>
	 */
	public CodeEditor getCodeEditor() {
		return editor;
	}
	
	/**
	 * Gets the LineNumbersPane instance.
	 * 
	 * @return the LineNumbersPane instance
	 */
	public LineNumbersPane getLineNumbersPane() {
		return lineNumbersPane;
	}
	
	/**
	 * Tests whether the text of the instance of the <code>CodeEditor</code> is saved on disc.
	 * 
	 * @return <code>true</code> if the text is saved on disc; <code>false</code> otherwise (if document is not saved and untitled yet)
	 */
	public boolean isSaved() {
		return saved;
	}
	
	/**
	 * Shows confirmation dialog to save current file.
	 * 
	 * @return <code>true</code> if the document has been saved on disc; <code>false</code> otherwise (if saving has been canceled)
	 */
	public boolean confirmToSave() {
		int res = 0;
		// bring this frame on top
		owner.getPane().setSelectedComponent(this);
		// if text was changed - confirm to save
		if (editor.isChanged()) {
			res = JOptionPane.showConfirmDialog(this, "Save changes to \""+getOpenedFile().getAbsolutePath()+"\"?", Parameters.PROGRAM_NAME, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (res == JOptionPane.YES_OPTION ) {
				if (!save()) {
					return false;
				}
			}
		}
		// if text was not changed or answer was YES or NO - load new document
		if ( (!editor.isChanged()) || (res == JOptionPane.YES_OPTION) || (res == JOptionPane.NO_OPTION) ) {
			return true;
		}
		return false;
	}
	
	/**
	 * Loads a new document of specified type.
	 * 
	 * @param type Type of the document
	 * @param title Title of the tab
	 * @param popup Pop up menu to set to editor
	 */
	public void loadUntitled(String type, String title, JPopupMenu popup) {
		owner.getConsolePane().write("Creating a new document...\n");
		editor.newDocument(type);
		owner.getPane().setDocumentIcon(this, editor.getCodeEditorDocument().getIcon());
		editor.setPopupMenu(popup);
		setOpenedFile(true, new File(title));
		updateTextStatus();
		updatePositionStatus();
		updateOverwriteStatus();
		doSetReadOnly(false);
		owner.refreshAllComponents();
		lineNumbersPane.grabFocus();
		owner.getConsolePane().write("Done\n\n");
	}
	
	/**
	 * Loads a file to the editor.
	 * 
	 * @param fsrc Source file
	 * @param popup Pop up menu to set to editor
	 */
	public boolean loadFile(File fsrc, JPopupMenu popup) {
		owner.getConsolePane().write("Loading file \""+fsrc.getAbsolutePath()+"\"...\n");
		if (editor.readFromFile(fsrc)) {
			editor.setPopupMenu(popup);
			setOpenedFile(false, fsrc);
			owner.getRecentFileList().remove(fsrc.getAbsolutePath());
			doSetReadOnly(false);
			owner.refreshAllComponents();
			owner.getPane().setDocumentIcon(this, editor.getCodeEditorDocument().getIcon());
		} else {
			owner.getConsolePane().write("ERROR: Unable to open file \""+fsrc.getAbsolutePath()+"\"\n\n");
			JOptionPane.showMessageDialog(owner, "Unable to open file \""+fsrc.getAbsolutePath()+"\"", "Open file", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		owner.getConsolePane().write("Done\n\n");
		updateTextStatus();
		updatePositionStatus();
		updateOverwriteStatus();
		lineNumbersPane.grabFocus();
		return true;
	}
	
	/**
	 * Executes document saving.
	 * 
	 * @return <code>true</code> if a document in the tab has been saved; <code>false</code> otherwise
	 */
	public boolean save() {
		if ( ( (saved) && (!editor.isChanged()) ) || (editor.isReadOnly()) ) {
			return true;
		}
		boolean s = false;
		if (saved) {
			editor.setAutoDetectType(false);
			if (editor.writeToFile(openedFile)) {
				setOpenedFile(false, openedFile);
				owner.refreshAllComponents();
				owner.getPane().setDocumentIcon(this, editor.getCodeEditorDocument().getIcon());
				s = true;
			}
			editor.setAutoDetectType(Parameters.GENERAL_AUTODETECTTYPE_ENABLE);
		} else {
			s = saveAs();
		}
		return s;
	}
	
	/**
	 * Executes document saving as.
	 * 
	 * @return <code>true</code> if a document in the tab has been saved; <code>false</code> otherwise
	 */
	public boolean saveAs() {
		boolean s = false;
		owner.getFileChooser().setMultiSelectionEnabled(false);
		if (!openedFile.getName().contains(".")) {
			owner.getFileChooser().setSelectedFile(new File(openedFile.getAbsolutePath() + editor.getCodeEditorDocument().getFirstExtension()));
		} else {
			owner.getFileChooser().setSelectedFile(openedFile);
		}
		if (owner.getFileChooser().showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			if (editor.writeToFile(owner.getFileChooser().getSelectedFile())) {
				setOpenedFile(false, owner.getFileChooser().getSelectedFile());
				owner.refreshAllComponents();
				owner.getPane().setDocumentIcon(this, editor.getCodeEditorDocument().getIcon());
				s = true;
			}
		}
		return s;
	}
	
	/**
	 * Closes this frame.
	 * 
	 * @return <code>true</code> if the tab has been closed; <code>false</code> otherwise
	 */
	public boolean close() {
		boolean s = confirmToSave();
		if (s) {
			if (saved) {
				owner.getRecentFileList().add(openedFile.getAbsolutePath());
				owner.getDocumentList().add(openedFile.getAbsolutePath());
			}
		}
		return s;
	}
	
	/**
	 * Executes searching in the document.
	 */
	public void doFind() {
		// create components of the dialog
		Object[] comp = {
			"Search string:",
			new JTextField(editor.getSelectedText())
		};
		// show the dialog
		int res = JOptionPane.showOptionDialog(
				this,
				comp,
				"Find text",
				JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				new String[] {"Find", "Close"},
				null
			);
		// analize the result
		switch (res) {
		case 0: // find
			searchingText = ((JTextField)comp[1]).getText();
			if (!searchingText.equals("")) {
				if (!editor.find(searchingText, 0)) {
					JOptionPane.showMessageDialog(this,"The specified text was not found","Find text",JOptionPane.WARNING_MESSAGE);
					searchingText = "";
				}
			}
			break;
		}
	}
	
	/**
	 * Searches the next entry of the searching text.
	 */
	public void doFindNext() {
		if (searchingText.equals("")) {
			doFind();
		} else {
			if (!editor.find(searchingText, editor.getSelectionEnd())) {
				JOptionPane.showMessageDialog(this,"The specified text was not found","Find text",JOptionPane.WARNING_MESSAGE);
				searchingText = "";
			}
		}
	}
	
	/**
	 * Executes text replacing in the document.
	 */
	public void doReplace() {
		// create components of the dialog
		Object[] comp = {
			"Search string:",
			new JTextField(editor.getSelectedText()),
			"Replace with:",
			new JTextField()
		};
		int res = -2;
		// while Close button or Esc will be not pressed
		while ( (res != 3) && (res != -1) ) {
			// show the dialog
			res = JOptionPane.showOptionDialog(
				this,
				comp,
				"Replace text",
				JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				new String[] {"Find", "Replace", "Replace All", "Close"},
				null
			);
			// analize the result
			switch (res) {
			case 0: // find
				if (!((JTextField)comp[1]).getText().equals("")) {
					if (!editor.find(((JTextField)comp[1]).getText(), editor.getSelectionEnd())) {
						JOptionPane.showMessageDialog(this,"The specified text was not found","Replace text",JOptionPane.WARNING_MESSAGE);
						editor.setSelectionStart(0);
						editor.setSelectionEnd(0);
					}
				}
				break;
			case 1: // replace
				if (!((JTextField)comp[1]).getText().equals("")) {
					if (!editor.replace(((JTextField)comp[1]).getText(), ((JTextField)comp[3]).getText(), editor.getSelectionStart())) {
						JOptionPane.showMessageDialog(this,"The specified text was not found","Replace text",JOptionPane.WARNING_MESSAGE);
						editor.setSelectionStart(0);
						editor.setSelectionEnd(0);
					}
				}
				break;
			case 2: // replace all
				if (!((JTextField)comp[1]).getText().equals("")) {
					if (!editor.replaceAll(((JTextField)comp[1]).getText(), ((JTextField)comp[3]).getText(), 0)) {
						JOptionPane.showMessageDialog(this,"The specified text was not found","Replace text",JOptionPane.WARNING_MESSAGE);
						editor.setSelectionStart(0);
						editor.setSelectionEnd(0);
					}
				}
				break;
			}
		}
	}
	
	/**
	 * Calls <code>undo()</code> method.
	 * 
	 * @see editors.CodeEditor#undo()
	 */
	public void doUndo() {
		editor.undo();
	}
	
	/**
	 * Calls <code>redo()</code> method.
	 * 
	 * @see editors.CodeEditor#redo()
	 */
	public void doRedo() {
		editor.redo();
	}
	
	/**
	 * Calls <code>isReadOnly()</code> method.
	 * 
	 * @return <code>isReadOnly()</code> method result
	 * @see editors.CodeEditor#isReadOnly()
	 */
	public boolean doGetReadOnly() {
		return editor.isReadOnly();
	}
	
	/**
	 * Calls <code>setReadOnly(boolean)</code> method.
	 * 
	 * @param readOnly <code>readOnly</code> property
	 * @see editors.CodeEditor#setReadOnly(boolean)
	 */
	public void doSetReadOnly(boolean readOnly) {
		editor.setReadOnly(readOnly);
	}
	
	/**
	 * Updates cursor position status on status bar
	 */
	public void updatePositionStatus() {
		owner.getStatusBar().getPanel(0).setText("pos: "+Integer.toString(editor.getCurrentLineIndex()+1) + ":" + Integer.toString(editor.getCurrentColIndex()+1));
		owner.getStatusBar().getPanel(1).setText("sel: "+Integer.toString(editor.getSelectionEnd()-editor.getSelectionStart()));
	}
	
	/**
	 * Updates text information on status bar
	 */
	public void updateTextStatus() {
		owner.getStatusBar().getPanel(2).setText("lines: "+Integer.toString(editor.getLineCount()));
	}
	
	/**
	 * Updates overwrite status on status bar
	 */
	public void updateOverwriteStatus() {
		String s = "";
		if (editor.isOverwrite()) {
			s = "OVR";
		} else {
			s = "INS";
		}
		owner.getStatusBar().getPanel(4).setText(s);
	}
	
	
	private MainFrame owner;
	private LineNumbersPane lineNumbersPane;
	private CodeEditor editor;
	private File openedFile;
	private String searchingText;
	private boolean saved;
	
	
	/**
	 * Sets title of the tab.
	 * 
	 * @param title Title of the tab
	 */
	private void setTitle(String title) {
		owner.getPane().setTitleAt(owner.getPane().indexOfComponent(this), title);
	}
	
	/**
	 * Sets a currently opened file.
	 * 
	 * @param untitled If <code>true</code> the document will be untitled
	 * @param fsrc Source file
	 */
	private void setOpenedFile(boolean untitled, File fsrc) {
		openedFile = fsrc;
		setTitle(fsrc.getName());
		if (untitled) {
			owner.getPane().setToolTipTextAt(owner.getPane().indexOfComponent(this), fsrc.getName());
		} else {
			owner.getPane().setToolTipTextAt(owner.getPane().indexOfComponent(this), fsrc.getAbsolutePath());
		}
		saved = !untitled;
	}
	
	/**
	 * Creates all components of the frame.
	 */
	private void createComponents() {
		editor = new CodeEditor();
		editor.addCodeEditorChangedListener(new CodeEditorDocumentEventsHandler());
		editor.addKeyListener(new OverwriteStatusHandler());
		editor.addCaretListener(new CaretEventsHandler());
		LightDocument lightDocument = new LightDocument(this, owner.getConfig(), "Plain", owner.getConsolePane());
		editor.setCodeEditorDocument(lightDocument);
		editor.setCodeEditorHighlighter(lightDocument);
		
		lineNumbersPane = new LineNumbersPane(editor);
		
		add(lineNumbersPane, BorderLayout.CENTER);
	}
	
	/**
	 * Provides receiving event on change CodeEditor's document.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class CodeEditorDocumentEventsHandler implements CodeEditorChangeListener {
		
		@Override
		public void editorChanged() {
			setTitle("* "+getOpenedFile().getName());
			updateTextStatus();
			owner.refreshEditorDependedComponents(EditorTab.this);
		}
		
		@Override
		public void undoableEditHappened() {
			owner.refreshEditorDependedComponents(EditorTab.this);
		}
		
	}
	
	/**
	 * Provides receiving event on caret position changing.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class CaretEventsHandler implements CaretListener {
		
		@Override
		public void caretUpdate(CaretEvent evt) {
			updatePositionStatus();
		}
		
	}
	
	/**
	 * Provides to watch the Insert key pressing.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class OverwriteStatusHandler extends KeyAdapter {
		
		public void keyPressed(KeyEvent evt) {
			if (evt.getKeyCode() == KeyEvent.VK_INSERT) {
				editor.setOverwrite(!editor.isOverwrite());
				updateOverwriteStatus();
			}
		}
		
	}
	
}
