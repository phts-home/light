package ides.light.parameters;

import java.awt.Color;
import java.awt.Font;


/**
 * This class contains program properties which initializes on creating components.
 * 
 * @author Phil Tsarik
 *
 */
public class Parameters {
	
	public static final String PROGRAM_NAME = "Light";
	public static final String[] PROGRAM_AUTHORS = {"Phil Tsarik", "Sergey Tuchina"};
	public static final String PROGRAM_VERSION = "0.8.5";
	public static final String PROGRAM_BUILD = "710";
	
	static public final String PROGRAM_PREFERENCES_PATH = "preferences/preferences.xml";
	static public final String PROFILES_CONFIG_PATH = "profiles/profiles.xml";
	
	static public boolean GENERAL_SPLASHSCREEN_VISIBLE;
	static public boolean GENERAL_RECENTFILES_ENABLE;
	static public int GENERAL_RECENTFILES_NUMBER;
	static public boolean GENERAL_REMEMBERSESSION_ENABLE;
	static public boolean GENERAL_AUTODETECTTYPE_ENABLE;
	
	static public int MAINFRAME_WIDTH;
	static public int MAINFRAME_HEIGHT;
	static public int MAINFRAME_X;
	static public int MAINFRAME_Y;
	static public int MAINFRAME_STATE;
	
	static public boolean EDITOR_LINENUMBERS_VISIBLE;
	static public boolean EDITOR_LINEWRAP_ENABLE;
	static public int EDITOR_TAB_SIZE;
	static public boolean EDITOR_TAB_INSERTSPACES_ENABLE;
	
	static public Color EDITOR_BACKGR_COLOR;
	static public Color EDITOR_CURSOR_COLOR;
	static public Color EDITOR_SELECTION_COLOR;
	static public Color EDITOR_SELECTEDTEXT_COLOR;
	
	static public Color EDITOR_LINENUMBERS_BACKGR_COLOR;
	static public Color EDITOR_LINENUMBERS_FOREGR_COLOR;
	
	static public Font EDITOR_FONT_APPEARANCE;
	static public Color EDITOR_FONT_COLOR;
	
	static public Color CONSOLE_BACKGR_COLOR;
	static public Font CONSOLE_FONT_APPEARANCE;
	static public Color CONSOLE_FONT_COLOR;
	
	static public Color NAVIGATOR_BACKGR_COLOR;
	static public Font NAVIGATOR_FONT_APPEARANCE;
	static public Color NAVIGATOR_FONT_COLOR;
	static public Color NAVIGATOR_SELECTION_COLOR;
	static public Color NAVIGATOR_SELECTEDTEXT_COLOR;
	
	static public int NAVIGATOR_DEPTH_VALUE;
	static public boolean NAVIGATOR_SORTED_ENABLE;
	
	static public int INTERFACE_LOOKANDFEEL;
	
	static public boolean INTERFACE_TABBAR_CLOSEBUTTONS_VISIBLE;
	static public boolean INTERFACE_TABBAR_DOCUMENTICONS_VISIBLE;
	static public int INTERFACE_TABBAR_POSITION;
	
	static public boolean INTERFACE_TOOLBAR_VISIBLE;
	static public boolean INTERFACE_STATUSBAR_VISIBLE;
	static public boolean INTERFACE_CONSOLEPANE_VISIBLE;
	static public boolean INTERFACE_NAVIGATOR_VISIBLE;
	
}
