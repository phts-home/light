package ides.light;

import ides.light.parameters.Parameters;
import java.awt.*;

import javax.swing.*;


/**
 * The <code>SplashScreen</code> class. Shows the splash screen.
 * 
 * @author Phil Tsarik
 *
 */
public class SplashScreen extends Window {
	
	/**
	 * Constructs a new instance of the <code>SplashScreen<code> class.
	 * 
	 * @param owner The program's main frame
	 */
	public SplashScreen(Frame owner) {
		super(owner);
		int width = 300;
		int height = 200;
		setSize(width, height);
		Dimension scrSize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(scrSize.width/2 - width/2, scrSize.height/2 - height/2);
		setBackground(Color.DARK_GRAY);
		
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		JPanel panel = new JPanel();
		panel.setLayout(layout);
		panel.setBackground(Color.DARK_GRAY);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0;
		
		constraints.weighty = 100;
		constraints.anchor = GridBagConstraints.EAST;
		JLabel label3 = new JLabel(" v"+Parameters.PROGRAM_VERSION+"."+Parameters.PROGRAM_BUILD);
		label3.setForeground(new Color(255, 239, 101));
		addComponentToGrid(label3, constraints, 0, 0, 1, 1, panel, layout);
		
		constraints.weighty = 100;
		constraints.anchor = GridBagConstraints.CENTER;
		JLabel label1 = new JLabel(new ImageIcon("resources/images/splash.gif"));
		addComponentToGrid(label1, constraints, 0, 1, 1, 1, panel, layout);
		
		constraints.weighty = 0;
		constraints.anchor = GridBagConstraints.CENTER;
		JLabel label2 = new JLabel("Loading...");
		label2.setForeground(new Color(255, 239, 101));
		addComponentToGrid(label2, constraints, 0, 2, 1, 1, panel, layout);
		
		add(panel);
	}
	
	private void addComponentToGrid(Component c, GridBagConstraints constraints, int x, int y, int w, int h, Container target, GridBagLayout layout) {
		constraints.gridx = x;
		constraints.gridy = y;
		constraints.gridwidth = w;
		constraints.gridheight = h;
		layout.setConstraints(c, constraints);
		target.add(c);
	}
	
}
