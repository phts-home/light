package ides.light;

import ides.light.actions.*;
import ides.light.parameters.Parameters;
import ides.light.profiles.*;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetContext;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

import panes.*;
import panes.enent.ClosableTabbedPaneEvent;
import panes.enent.ClosableTabbedPaneListener;
import preferences.PreferencesApplicable;
import preferences.PreferencesCustomizable;
import preferences.SimpleProgramPreferences;


import java.io.*;
import java.util.*;

import org.w3c.dom.Element;

import components.FileTree;
import components.FileTreeItem;
import components.FileTreeNode;
import components.SimpleStatusBar;

import documents.SimpleXMLDocument;


/**
 * The program's main frame.
 * 
 * @author Phil Tsarik
 * @author Sergey Tuchina
 *
 */
public class MainFrame extends JFrame implements PreferencesCustomizable, PreferencesApplicable {
	
	/**
	 * Default window width.
	 */
	public final static int DEFAULT_WIDTH = 900;
	
	/**
	 * Default window height.
	 */
	public final static int DEFAULT_HEIGHT = 700;
	
	/**
	 * Constructs a new instance of the <code>MainFrame</code> class.
	 * 
	 * @param preferences The <code>SimpleProgramPreferences</code> instance
	 * @param filesToOpen Files to open array
	 */
	public MainFrame(SimpleProgramPreferences preferences, String[] filesToOpen) {
		super();
		splashScreen = new SplashScreen(this);
		
		// add frame listener
		addWindowListener(new WindowEventsHandler());
		
		// init some variables
		this.preferences = preferences;
		config = new ProfilesConfig(this, Parameters.PROFILES_CONFIG_PATH, "profiles");
		this.filesToOpen = filesToOpen;
		untitledCount = 0;
		
		// set window icon
		Toolkit kit = Toolkit.getDefaultToolkit();
		setIconImage(kit.getImage("resources/icons/light-16-16.gif"));
		
		// set window title
		setTitle(Parameters.PROGRAM_NAME);
		
		// create components
		createComponents();
		
		// add this form to preferences as Customizable
		preferences.addCustomizable(this);
		
		// add this form to preferences as Applicable
		preferences.addApplicable(this);
		
		// show the frame
		setVisible(true);
		
		// refresh components
		refreshAllComponents();
		
		// disable interception of ctrl+tab and shift+ctrl+tab by JFrame
		setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, Collections.EMPTY_SET);
		setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, Collections.EMPTY_SET);
		setFocusTraversalKeys(KeyboardFocusManager.UP_CYCLE_TRAVERSAL_KEYS, Collections.EMPTY_SET);
		setFocusTraversalKeys(KeyboardFocusManager.DOWN_CYCLE_TRAVERSAL_KEYS, Collections.EMPTY_SET);
		
		// hide splashScreen screen and remove it from memory
		splashScreen.setVisible(false);
		splashScreen = null;
	}
	
	@Override
	public void loadPreferences(SimpleXMLDocument doc) {
		Element elem = doc.getElement(null, "general");
		Parameters.GENERAL_SPLASHSCREEN_VISIBLE = doc.readBooleanValue(elem, "splashscreen_visible", true);
		splashScreen.setVisible(Parameters.GENERAL_SPLASHSCREEN_VISIBLE);
		
		consolePane.write("Reading preferences...\n");
		elem = doc.getElement(null, "main_frame");
		Dimension scrSize = Toolkit.getDefaultToolkit().getScreenSize();
		Parameters.MAINFRAME_STATE = doc.readIntegerValue(elem, "state", Frame.NORMAL);
		Parameters.MAINFRAME_WIDTH = doc.readIntegerValue(elem, "width", DEFAULT_WIDTH);
		Parameters.MAINFRAME_HEIGHT = doc.readIntegerValue(elem, "height", DEFAULT_HEIGHT);
		Parameters.MAINFRAME_X = doc.readIntegerValue(elem, "x", scrSize.width/2 - DEFAULT_WIDTH/2);
		Parameters.MAINFRAME_Y = doc.readIntegerValue(elem, "y", scrSize.height/2 - DEFAULT_HEIGHT/2);
		
		elem = doc.getElement(null, "general");
		Parameters.GENERAL_AUTODETECTTYPE_ENABLE = doc.readBooleanValue(elem, "autodetecttype_enable", true);
		
		elem = doc.getElement(null, "interface");
		Parameters.INTERFACE_LOOKANDFEEL = doc.readIntegerValue(elem, "lookandfeel", 0);
		Parameters.INTERFACE_STATUSBAR_VISIBLE = doc.readBooleanValue(elem, "statusbar_visible", true);
		Parameters.INTERFACE_TOOLBAR_VISIBLE = doc.readBooleanValue(elem, "toolbar_visible", true);
		Parameters.INTERFACE_CONSOLEPANE_VISIBLE = doc.readBooleanValue(elem, "consolepane_visible", true);
		Parameters.INTERFACE_NAVIGATOR_VISIBLE = doc.readBooleanValue(elem, "navigator_visible", true);
		consoleSplitPaneDividerLocation = doc.readIntegerValue(elem, "console_divider_location", 450);
		navigatorSplitPaneDividerLocation = doc.readIntegerValue(elem, "navigator_divider_location", 700);
		consoleSplitPane.setDividerLocation(consoleSplitPaneDividerLocation);
		navigatorSplitPane.setDividerLocation(navigatorSplitPaneDividerLocation);
		
		elem = doc.getElement(null, "editor");
		Parameters.EDITOR_LINENUMBERS_VISIBLE = doc.readBooleanValue(elem, "linenumbers_visible", true);
		Parameters.EDITOR_LINEWRAP_ENABLE = doc.readBooleanValue(elem, "linewrap_enable", false);
		Parameters.EDITOR_TAB_SIZE = doc.readIntegerValue(elem, "tab_size", 4);
		Parameters.EDITOR_TAB_INSERTSPACES_ENABLE = doc.readBooleanValue(elem, "insertspaces_enable", false);
		Parameters.EDITOR_FONT_APPEARANCE = doc.readFontValue(elem, "font_appearance", new Font("SansSerif", Font.PLAIN, 14));
		Parameters.EDITOR_FONT_COLOR = doc.readColorValue(elem, "font_color", Color.BLACK);
		
		elem = doc.getElement(null, "console");
		Parameters.CONSOLE_FONT_APPEARANCE = doc.readFontValue(elem, "font_appearance", new Font("SansSerif", Font.PLAIN, 14));
		Parameters.CONSOLE_FONT_COLOR = doc.readColorValue(elem, "font_color", Color.BLACK);
		
		elem = doc.getElement(null, "navigator");
		Parameters.NAVIGATOR_DEPTH_VALUE = doc.readIntegerValue(elem, "depth", 2);
		Parameters.NAVIGATOR_SORTED_ENABLE = doc.readBooleanValue(elem, "sorted", false);
		Parameters.NAVIGATOR_FONT_APPEARANCE = doc.readFontValue(elem, "font_appearance", new Font("SansSerif", Font.PLAIN, 12));
		Parameters.NAVIGATOR_FONT_COLOR = doc.readColorValue(elem, "font_color", Color.BLACK);
		
		elem = doc.getElement(null, "colors");
		Parameters.EDITOR_BACKGR_COLOR = doc.readColorValue(elem, "editor_backgr_color", Color.WHITE);
		Parameters.EDITOR_CURSOR_COLOR = doc.readColorValue(elem, "editor_cursor_color", Color.BLACK);
		Parameters.EDITOR_SELECTION_COLOR = doc.readColorValue(elem, "editor_selection_color", Color.GRAY);
		Parameters.EDITOR_SELECTEDTEXT_COLOR = doc.readColorValue(elem, "editor_selectedtext_color", Color.BLACK);
		Parameters.EDITOR_LINENUMBERS_BACKGR_COLOR = doc.readColorValue(elem, "linenumbers_backgr_color", Color.LIGHT_GRAY);
		Parameters.EDITOR_LINENUMBERS_FOREGR_COLOR = doc.readColorValue(elem, "linenumbers_foregr_color", Color.BLACK);
		Parameters.CONSOLE_BACKGR_COLOR = doc.readColorValue(elem, "console_backgr_color", Color.WHITE);
		Parameters.NAVIGATOR_BACKGR_COLOR = doc.readColorValue(elem, "navigator_backgr_color", Color.WHITE);
		Parameters.NAVIGATOR_SELECTION_COLOR = doc.readColorValue(elem, "navigator_selection_color", Color.GRAY);
		Parameters.NAVIGATOR_SELECTEDTEXT_COLOR = doc.readColorValue(elem, "navigator_selectedtext_color", Color.BLACK);
		
		elem = doc.getElement(null, "tabbar");
		Parameters.INTERFACE_TABBAR_CLOSEBUTTONS_VISIBLE = doc.readBooleanValue(elem, "closebuttons_visible", true);
		Parameters.INTERFACE_TABBAR_DOCUMENTICONS_VISIBLE = doc.readBooleanValue(elem, "documenticons_visible", true);
		Parameters.INTERFACE_TABBAR_POSITION = doc.readIntegerValue(elem, "position", 0);
		
		// set window size
		setSize(Parameters.MAINFRAME_WIDTH, Parameters.MAINFRAME_HEIGHT);
		// set window position
		setLocation(Parameters.MAINFRAME_X, Parameters.MAINFRAME_Y);
		setExtendedState(Parameters.MAINFRAME_STATE);
		
		// open all opened documents from last session
		documentList.execute();
		// open documents typed in command line
		for (int i = 0; i < filesToOpen.length; i++) {
			openDocument(new File(filesToOpen[i]).getAbsoluteFile());
		}
		consolePane.write("Done\n\n");
	}
	
	@Override
	public void savePreferences(SimpleXMLDocument doc) {
		consolePane.write("Saving preferences...\n");
		Element elem = doc.getElement(null, "main_frame");
		doc.addProperty(elem, "state", this.getExtendedState());
		this.setExtendedState(Frame.NORMAL);
		doc.addProperty(elem, "width", this.getWidth());
		doc.addProperty(elem, "height", this.getHeight());
		doc.addProperty(elem, "x", this.getX());
		doc.addProperty(elem, "y", this.getY());
		
		elem = doc.getElement(null, "general");
		doc.addProperty(elem, "splashscreen_visible", Parameters.GENERAL_SPLASHSCREEN_VISIBLE);
		doc.addProperty(elem, "autodetecttype_enable", Parameters.GENERAL_AUTODETECTTYPE_ENABLE);
		
		elem = doc.getElement(null, "interface");
		doc.addProperty(elem, "lookandfeel", Parameters.INTERFACE_LOOKANDFEEL);
		doc.addProperty(elem, "statusbar_visible", Parameters.INTERFACE_STATUSBAR_VISIBLE);
		doc.addProperty(elem, "toolbar_visible", Parameters.INTERFACE_TOOLBAR_VISIBLE);
		doc.addProperty(elem, "consolepane_visible", Parameters.INTERFACE_CONSOLEPANE_VISIBLE);
		doc.addProperty(elem, "navigator_visible", Parameters.INTERFACE_NAVIGATOR_VISIBLE);
		
		doc.addProperty(elem, "console_divider_location", (!consolePane.isVisible()) ? consoleSplitPaneDividerLocation : consoleSplitPane.getDividerLocation());
		doc.addProperty(elem, "navigator_divider_location", (!navigatorPane.isVisible()) ? navigatorSplitPaneDividerLocation : navigatorSplitPane.getDividerLocation());
		
		elem = doc.getElement(null, "editor");
		doc.addProperty(elem, "linenumbers_visible", Parameters.EDITOR_LINENUMBERS_VISIBLE);
		doc.addProperty(elem, "linewrap_enable", Parameters.EDITOR_LINEWRAP_ENABLE);
		doc.addProperty(elem, "tab_size", Parameters.EDITOR_TAB_SIZE);
		doc.addProperty(elem, "insertspaces_enable", Parameters.EDITOR_TAB_INSERTSPACES_ENABLE);
		doc.addProperty(elem, "font_appearance", Parameters.EDITOR_FONT_APPEARANCE);
		doc.addProperty(elem, "font_color", Parameters.EDITOR_FONT_COLOR);
		
		elem = doc.getElement(null, "console");
		doc.addProperty(elem, "font_appearance", Parameters.CONSOLE_FONT_APPEARANCE);
		doc.addProperty(elem, "font_color", Parameters.CONSOLE_FONT_COLOR);
		
		elem = doc.getElement(null, "navigator");
		doc.addProperty(elem, "depth", Parameters.NAVIGATOR_DEPTH_VALUE);
		doc.addProperty(elem, "sorted", Parameters.NAVIGATOR_SORTED_ENABLE);
		doc.addProperty(elem, "font_appearance", Parameters.NAVIGATOR_FONT_APPEARANCE);
		doc.addProperty(elem, "font_color", Parameters.NAVIGATOR_FONT_COLOR);
		
		elem = doc.getElement(null, "colors");
		doc.addProperty(elem, "editor_backgr_color", Parameters.EDITOR_BACKGR_COLOR);
		doc.addProperty(elem, "editor_cursor_color", Parameters.EDITOR_CURSOR_COLOR);
		doc.addProperty(elem, "editor_selection_color", Parameters.EDITOR_SELECTION_COLOR);
		doc.addProperty(elem, "editor_selectedtext_color", Parameters.EDITOR_SELECTEDTEXT_COLOR);
		doc.addProperty(elem, "linenumbers_backgr_color", Parameters.EDITOR_LINENUMBERS_BACKGR_COLOR);
		doc.addProperty(elem, "linenumbers_foregr_color", Parameters.EDITOR_LINENUMBERS_FOREGR_COLOR);
		doc.addProperty(elem, "console_backgr_color", Parameters.CONSOLE_BACKGR_COLOR);
		doc.addProperty(elem, "navigator_backgr_color", Parameters.NAVIGATOR_BACKGR_COLOR);
		doc.addProperty(elem, "navigator_selection_color", Parameters.NAVIGATOR_SELECTION_COLOR);
		doc.addProperty(elem, "navigator_selectedtext_color", Parameters.NAVIGATOR_SELECTEDTEXT_COLOR);
		
		elem = doc.getElement(null, "tabbar");
		doc.addProperty(elem, "closebuttons_visible", Parameters.INTERFACE_TABBAR_CLOSEBUTTONS_VISIBLE);
		doc.addProperty(elem, "documenticons_visible", Parameters.INTERFACE_TABBAR_DOCUMENTICONS_VISIBLE);
		doc.addProperty(elem, "position", Parameters.INTERFACE_TABBAR_POSITION);
		consolePane.write("Done\n\n");
	}
	
	@Override
	public void applyPreferences() {
		consolePane.write("Applying preferences...\n");
		try {
			switch (Parameters.INTERFACE_LOOKANDFEEL) {
			case 0:
				UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
				aSetMetal.setSelected(true);
				break;
			case 1:
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
				aSetMotif.setSelected(true);
				break;
			case 2:
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
				aSetWin.setSelected(true);
				break;
			}
			updateComponentUI();
		}
		catch (Exception e) {
		}
		pane.setCloseButtonVisible(Parameters.INTERFACE_TABBAR_CLOSEBUTTONS_VISIBLE);
		pane.setDocumentIconVisible(Parameters.INTERFACE_TABBAR_DOCUMENTICONS_VISIBLE);
		
		consolePane.setBackgroundColor(Parameters.CONSOLE_BACKGR_COLOR);
		consolePane.setTextAreaFont(Parameters.CONSOLE_FONT_APPEARANCE, Parameters.CONSOLE_FONT_COLOR);
		
		fileTree.setBackground(Parameters.NAVIGATOR_BACKGR_COLOR);
		fileTree.setForeground(Parameters.NAVIGATOR_FONT_COLOR);
		fileTree.setBackgroundSelectionColor(Parameters.NAVIGATOR_SELECTION_COLOR);
		fileTree.setBorderSelectionColor(Parameters.NAVIGATOR_SELECTION_COLOR);
		fileTree.setTextSelectionColor(Parameters.NAVIGATOR_SELECTEDTEXT_COLOR);
		fileTree.setFont(Parameters.NAVIGATOR_FONT_APPEARANCE);
		
		fileTree.setDepthAndSorted(Parameters.NAVIGATOR_DEPTH_VALUE, Parameters.NAVIGATOR_SORTED_ENABLE);
		
		switch (Parameters.INTERFACE_TABBAR_POSITION) {
		case 0: pane.setTabPlacement(ClosableTabbedPane.TOP); break;
		case 1: pane.setTabPlacement(ClosableTabbedPane.BOTTOM); break;
		case 2: pane.setTabPlacement(ClosableTabbedPane.LEFT); break;
		case 3: pane.setTabPlacement(ClosableTabbedPane.RIGHT); break;
		}
		setToolBarVisible(Parameters.INTERFACE_TOOLBAR_VISIBLE);
		setStatusBarVisible(Parameters.INTERFACE_STATUSBAR_VISIBLE);
		setConsolePaneVisible(Parameters.INTERFACE_CONSOLEPANE_VISIBLE);
		setNavigatorVisible(Parameters.INTERFACE_NAVIGATOR_VISIBLE);
		
		aShowLineNumbers.setSelected(Parameters.EDITOR_LINENUMBERS_VISIBLE);
		aSetWordWrap.setSelected(Parameters.EDITOR_LINEWRAP_ENABLE);
		consolePane.write("Done\n\n");
	}
	
	/**
	 * Gets file chooser.
	 * 
	 * @return File chooser
	 */
	public JFileChooser getFileChooser() {
		return fileChooser;
	}
	
	/**
	 * Gets "Preferences" dialog.
	 * 
	 * @return "Preferences" dialog
	 */
	public PreferencesDialog getPreferencesDialog() {
		return preferencesDialog;
	}
	
	/**
	 * Gets preferences.
	 * 
	 * @return Preferences
	 */
	public SimpleProgramPreferences getPreferences() {
		return preferences;
	}
	
	/**
	 * Gets profiles config.
	 * 
	 * @return Profiles config
	 */
	public ProfilesConfig getConfig() {
		return config;
	}
	
	/**
	 * Gets the <code>EditorTab</code>'s instance at specified index.
	 * 
	 * @param index The index of the tab <code>EditorTab</code> component
	 * @return The <code>EditorTab</code>'s instance
	 */
	public EditorTab getEditorTabAt(int index) {
		return (EditorTab)pane.getComponentAt(index);
	}
	
	/**
	 * Gets a selected <code>EditorTab</code>'s instance.
	 * 
	 * @return The <code>EditorTab</code>'s instance
	 */
	public EditorTab getSelectedEditorTab() {
		return (EditorTab)pane.getSelectedComponent();
	}
	
	/**
	 * Gets the <code>ClosableTabbedPane</code>'s instance placed on the frame.
	 * 
	 * @return The <code>ClosableTabbedPane</code>'s instance placed on the frame
	 */
	public ClosableTabbedPane getPane() {
		return pane;
	}
	
	/**
	 * Gets the <code>ConsolePane</code>'s instance placed on the frame.
	 * 
	 * @return The <code>ConsolePane</code>'s instance placed on the frame
	 */
	public ConsolePane getConsolePane() {
		return consolePane;
	}
	
	/**
	 * Gets the <code>TabList</code>'s instance.
	 * 
	 * @return The <code>TabList</code>'s instance
	 */
	public TabList getTabList() {
		return tabList;
	}
	
	/**
	 * Gets the <code>RecentFileList</code>'s instance.
	 * 
	 * @return The <code>RecentFileList</code>'s instance
	 */
	public RecentFileList getRecentFileList() {
		return fileList;
	}
	
	/**
	 * Gets the <code>DocumentList</code>'s instance.
	 * 
	 * @return The <code>DocumentList</code>'s instance
	 */
	public DocumentList getDocumentList() {
		return documentList;
	}
	
	/**
	 * Gets frame status bar.
	 * 
	 * @return The <code>SimpleStatusBar</code>'s instance
	 */
	public SimpleStatusBar getStatusBar() {
		return statusBar;
	}
	
	/**
	 * Sets <code>toolBarVisible</code> property, which determines whether tool bar is visible.
	 * 
	 * @param toolBarVisible <code>true</code> to specify that the tool bar is visible; <code>false</code> to specify that it is not visible
	 */
	public void setToolBarVisible(boolean toolBarVisible) {
		Parameters.INTERFACE_TOOLBAR_VISIBLE = toolBarVisible;
		toolBar.setVisible(toolBarVisible);
		aShowToolBar.setSelected(toolBarVisible);
	}
	
	/**
	 * Sets <code>statusBarVisible</code> property, which determines whether status bar is visible.
	 * 
	 * @param statusBarVisible <code>true</code> to specify that the status bar is visible; <code>false</code> to specify that it is not visible
	 */
	public void setStatusBarVisible(boolean statusBarVisible) {
		Parameters.INTERFACE_STATUSBAR_VISIBLE = statusBarVisible;
		statusBar.setVisible(statusBarVisible);
		aShowStatusBar.setSelected(statusBarVisible);
	}
	
	/**
	 * Sets <code>consolePaneVisible</code> property, which determines whether console pane is visible.
	 * 
	 * @param consolePaneVisible <code>true</code> to specify that the console pane is visible; <code>false</code> to specify that it is not visible
	 */
	public void setConsolePaneVisible(boolean consolePaneVisible) {
		boolean old = Parameters.INTERFACE_CONSOLEPANE_VISIBLE;
		Parameters.INTERFACE_CONSOLEPANE_VISIBLE = consolePaneVisible;
		if (!consolePaneVisible) {
			if (consoleSplitPane.getDividerLocation() != -1) {
				consoleSplitPaneDividerLocation = consoleSplitPane.getDividerLocation();
			}
		}
		consolePane.setVisible(consolePaneVisible);
		consolePane.setCanWrite(consolePaneVisible);
		aShowConsolePane.setSelected(consolePaneVisible);
		if (old != consolePaneVisible) {
			consoleSplitPane.setDividerLocation(consoleSplitPaneDividerLocation);
		}
	}
	
	/**
	 * Sets <code>navigatorVisible</code> property, which determines whether Navigator is visible.
	 * 
	 * @param navigatorVisible <code>true</code> to specify that the Navigator is visible; <code>false</code> to specify that it is not visible
	 */
	public void setNavigatorVisible(boolean navigatorVisible) {
		boolean old = Parameters.INTERFACE_NAVIGATOR_VISIBLE;
		Parameters.INTERFACE_NAVIGATOR_VISIBLE = navigatorVisible;
		if (!navigatorVisible) {
			if (navigatorSplitPane.getDividerLocation() != -1) {
				navigatorSplitPaneDividerLocation = navigatorSplitPane.getDividerLocation();
			}
		} else {
			refreshAllComponents();
		}
		navigatorPane.setVisible(navigatorVisible);
		aShowNavigator.setSelected(navigatorVisible);
		if (old != navigatorVisible) {
			navigatorSplitPane.setDividerLocation(navigatorSplitPaneDividerLocation);
		}
	}
	
	/**
	 * Sets the current editor read only.
	 * 
	 * @param readOnly <code>true</code> to specify that the current editor is read only; <code>false</code> to specify that it is not read only
	 */
	public void setReadOnly(boolean readOnly) {
		getSelectedEditorTab().doSetReadOnly(readOnly);
		refreshAllComponents();
	}
	
	/**
	 * Refreshes all components as the selected frame requires.
	 */
	public void refreshAllComponents() {
		EditorTab tab = getSelectedEditorTab();
		boolean en = tab != null;
		aNewPlainInSelected.setEnabled(en);
		aOpenInSelected.setEnabled(en);
		aSave.setEnabled(en);
		aSaveAs.setEnabled(en);
		aSaveAll.setEnabled(en);
		aClose.setEnabled(en);
		aCloseAll.setEnabled(en);
		aCloseOthers.setEnabled(en);
		aPrint.setEnabled(en);
		aUndo.setEnabled(en);
		aRedo.setEnabled(en);
		aCut.setEnabled(en);
		aCopy.setEnabled(en);
		aPaste.setEnabled(en);
		aCutLine.setEnabled(en);
		aCopyLine.setEnabled(en);
		aMoveUpLine.setEnabled(en);
		aMoveDownLine.setEnabled(en);
		aDuplicateLine.setEnabled(en);
		aDeleteLine.setEnabled(en);
		aIndentBlock.setEnabled(en);
		aOutdentBlock.setEnabled(en);
		aInsertTimeDate.setEnabled(en);
		aInsertFilename.setEnabled(en);
		aInsertPathFilename.setEnabled(en);
		aToUppercase.setEnabled(en);
		aToLowercase.setEnabled(en);
		aConvertToCRLF.setEnabled(en);
		aConvertToLF.setEnabled(en);
		aConvertToCR.setEnabled(en);
		aFind.setEnabled(en);
		aFindNext.setEnabled(en);
		aReplace.setEnabled(en);
		aSelectAll.setEnabled(en);
		aRun.setEnabled(en);
		aCompile.setEnabled(en);
		aCompileandrun.setEnabled(en);
		aEditHighlightConfiguration.setEnabled(en);
		aSelectNextTab.setEnabled(en);
		aSelectPreviousTab.setEnabled(en);
		aMoveLeftTab.setEnabled(en);
		aMoveRightTab.setEnabled(en);
		newInSelTabMenu.setEnabled(en);
		editMenu.setVisible(en);
		viewMenu.setVisible(en);
		documentMenu.setVisible(en);
		tabMenu.setVisible(en);
		if (!en) {
			fileTree.clear();
			return;
		}
		boolean readOnly = tab.doGetReadOnly();
		aSetReadOnly.setSelected(readOnly);
		aSetReadOnly2.setSelected(readOnly);
		aCut.setEnabled(!readOnly);
		aPaste.setEnabled(!readOnly);
		aCutLine.setEnabled(!readOnly);
		aMoveUpLine.setEnabled(!readOnly);
		aMoveDownLine.setEnabled(!readOnly);
		aDuplicateLine.setEnabled(!readOnly);
		aDeleteLine.setEnabled(!readOnly);
		aIndentBlock.setEnabled(!readOnly);
		aOutdentBlock.setEnabled(!readOnly);
		aInsertFilename.setEnabled(!readOnly);
		aInsertPathFilename.setEnabled(!readOnly);
		aConvertToCRLF.setEnabled(!readOnly);
		aConvertToLF.setEnabled(!readOnly);
		aConvertToCR.setEnabled(!readOnly);
		aReplace.setEnabled(!readOnly);
		refreshEditorDependedComponents(tab);
		
		if (navigatorPane.isVisible()) {
			final File parentFile = tab.getOpenedFile();
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					fileTree.setParentFile(parentFile);
				}
			});
		}
	}
	
	public void refreshEditorDependedComponents(EditorTab tab) {
		boolean readOnly = tab.doGetReadOnly();
		if (readOnly) {
			aUndo.setEnabled(false);
			aRedo.setEnabled(false);
		} else {
			aUndo.setEnabled(tab.getCodeEditor().canUndo());
			aRedo.setEnabled(tab.getCodeEditor().canRedo());
		}
		aSave.setEnabled( tab.getCodeEditor().isChanged() || !tab.isSaved() );
		boolean en = false;
		for (int i = 0; i < pane.getTabCount(); i++) {
			if ( (getEditorTabAt(i).getCodeEditor().isChanged()) || (!getEditorTabAt(i).isSaved()) ) {
				en = true;
				break;
			}
		}
		aSaveAll.setEnabled(en);
		aRun.setEnabled(tab.getCodeEditor().getCodeEditorDocument().isCanRun());
		aCompile.setEnabled(tab.getCodeEditor().getCodeEditorDocument().isCanCompile());
		aCompileandrun.setEnabled(tab.getCodeEditor().getCodeEditorDocument().isCanCompileAndRun());
	}
	
	/**
	 * Updates components' UI
	 */
	public void updateComponentUI() {
		SwingUtilities.updateComponentTreeUI(this);
		SwingUtilities.updateComponentTreeUI(preferencesDialog);
		SwingUtilities.updateComponentTreeUI(fileChooser);
		pane.setClosableTabbedPaneUI();
		SwingUtilities.updateComponentTreeUI(editorPopup);
		SwingUtilities.updateComponentTreeUI(tabPopup);
		SwingUtilities.updateComponentTreeUI(consolePopup);
		SwingUtilities.updateComponentTreeUI(navigatorPopup);
	}
	
	/**
	 * Creates a new document into a new tab.
	 * 
	 * @param type Document type
	 */
	public EditorTab createDocument(String type) {
		return createEditorTab(type);
	}
	
	/**
	 * Opens a document into a new tab.
	 */
	public void openDocument() {
		fileChooser.setMultiSelectionEnabled(true);
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			File[] files = fileChooser.getSelectedFiles();
			for (int i = 0; i < files.length; i++) {
				openDocument(files[i]);
			}
		}
	}
	
	/**
	 * Opens a document with the specified file.
	 * 
	 * @param fsrc Source file
	 */
	public void openDocument(File fsrc) {
		int c = pane.getComponentCount();
		boolean exists = false;
		for (int n = 0; n < c; n++) {
			exists = fsrc.getAbsolutePath().equals(getEditorTabAt(n).getOpenedFile().getAbsolutePath());
			if (exists) {
				pane.setSelectedIndex(n);
				break;
			}
		}
		if (!exists) {
			createEditorTab(fsrc);
		}
	}
	
	/**
	 * Creates a new document into the selected tab.
	 * 
	 * @param type Document type
	 */
	public EditorTab createDocumentInSelectedTab(String type) {
		EditorTab tab = (EditorTab)pane.getSelectedComponent();
		if (tab.confirmToSave()) {
			tab.loadUntitled(type, "Untitled-"+(++untitledCount), editorPopup);
		}
		return tab;
	}
	
	/**
	 * Opens a document into the selected tab.
	 */
	public void openDocumentInSelectedTab() {
		EditorTab tab = (EditorTab)pane.getSelectedComponent();
		if (tab.confirmToSave()) {
			fileChooser.setMultiSelectionEnabled(false);
			if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				int c = pane.getComponentCount();
				boolean exists = false;
				for (int n = 0; n < c; n++) {
					exists = file == getEditorTabAt(n).getOpenedFile();
					if (exists) {
						pane.setSelectedIndex(n);
						break;
					}
				}
				if (!exists) {
					tab.loadFile(file, editorPopup);
				}
			}
		}
	}
	
	/**
	 * Closes all tabs.
	 * 
	 * @return <code>true</code> if all tabs have been closed; <code>false</code> otherwise
	 */
	public boolean closeAll() {
		boolean closed = true;
		int c = pane.getComponentCount();
		for (int i = c-1; i >= 0; i--) {
			closed = pane.closeTab(i); 
			if (!closed) {
				return false;
			}
		}
		return closed;
	}
	
	/**
	 * Closes all tabs but selected.
	 * 
	 * @return <code>true</code> if all but selected tabs have been closed; <code>false</code> otherwise
	 */
	public boolean closeOthers() {
		boolean closed = true;
		int c = pane.getComponentCount();
		int sel = pane.getSelectedIndex();
		for (int i = c-1; i >= 0; i--) {
			if (i != sel) {
				closed = pane.closeTab(i); 
				if (!closed) {
					return false;
				}
			}
		}
		return closed;
	}
	
	/**
	 * Saves all documents of all tabs.
	 */
	public void saveAll() {
		boolean saved = true;
		int c = pane.getComponentCount();
		for (int i = c-1; i >= 0; i--) {
			saved = getEditorTabAt(i).save(); 
			if (!saved) {
				return;
			}
		}
	}
	
	/**
	 * Quits the program.
	 */
	public void exit() {
		// clear document list
		documentList.clear();
		documentList.setSelectedDoc(pane.getSelectedIndex());
		// if all tabs have been closed
		if (closeAll()) {
			preferences.save();
			this.setDefaultCloseOperation(EXIT_ON_CLOSE);
			this.dispose();
		} else {
			this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		}
	}
	
	private SplashScreen splashScreen;
	private PreferencesDialog preferencesDialog;
	private JFileChooser fileChooser;
	private JPopupMenu editorPopup;
	private JPopupMenu tabPopup;
	private JPopupMenu consolePopup;
	private JPopupMenu navigatorPopup;
	private TabList tabList;
	private RecentFileList fileList;
	private DocumentList documentList;
	private ClosableTabbedPane pane;
	private JToolBar toolBar;
	private SimpleStatusBar statusBar;
	private SimpleProgramPreferences preferences;
	private ProfilesConfig config;
	private int untitledCount;
	private String[] filesToOpen;
	private ConsolePane consolePane;
	private JSplitPane consoleSplitPane;
	private JSplitPane navigatorSplitPane;
	private FileTree fileTree;
	private JScrollPane navigatorPane;
	private int consoleSplitPaneDividerLocation;
	private int navigatorSplitPaneDividerLocation;
	
	private MainFrameMenuAction aNewPlainInSelected;
	private MainFrameMenuAction aOpenInSelected;
	private MainFrameMenuAction aClose;
	private MainFrameMenuAction aCloseAll;
	private MainFrameMenuAction aCloseOthers;
	private MainFrameMenuAction aSave;
	private MainFrameMenuAction aSaveAs;
	private MainFrameMenuAction aSaveAll;
	private MainFrameMenuAction aPrint;
	private MainFrameMenuAction aUndo;
	private MainFrameMenuAction aRedo;
	private MainFrameMenuAction aCut;
	private MainFrameMenuAction aCopy;
	private MainFrameMenuAction aPaste;
	private MainFrameMenuAction aMoveUpLine;
	private MainFrameMenuAction aMoveDownLine;
	private MainFrameMenuAction aCutLine;
	private MainFrameMenuAction aCopyLine;
	private MainFrameMenuAction aDuplicateLine;
	private MainFrameMenuAction aDeleteLine;
	private MainFrameMenuAction aIndentBlock;
	private MainFrameMenuAction aOutdentBlock;
	private MainFrameMenuAction aInsertTimeDate;
	private MainFrameMenuAction aInsertFilename;
	private MainFrameMenuAction aInsertPathFilename;
	private MainFrameMenuAction aToUppercase;
	private MainFrameMenuAction aToLowercase;
	private MainFrameMenuAction aConvertToCRLF;
	private MainFrameMenuAction aConvertToLF;
	private MainFrameMenuAction aConvertToCR;
	
	private MainFrameMenuAction aFind;
	private MainFrameMenuAction aFindNext;
	private MainFrameMenuAction aReplace;
	private MainFrameMenuAction aSelectAll;
	private JCheckBoxMenuItem aSetReadOnly;
	private JCheckBoxMenuItem aSetReadOnly2;
	
	private JCheckBoxMenuItem aShowLineNumbers;
	private JCheckBoxMenuItem aSetWordWrap;
	private JCheckBoxMenuItem aShowToolBar;
	private JCheckBoxMenuItem aShowStatusBar;
	private JCheckBoxMenuItem aShowConsolePane;
	private JCheckBoxMenuItem aShowNavigator;
	private JCheckBoxMenuItem aSetMetal;
	private JCheckBoxMenuItem aSetMotif;
	private JCheckBoxMenuItem aSetWin;
	
	private MainFrameMenuAction aRun;
	private MainFrameMenuAction aCompile;
	private MainFrameMenuAction aCompileandrun;
	private MainFrameMenuAction aSetPlainType;
	private MainFrameMenuAction aEditHighlightConfiguration;
	
	private MainFrameMenuAction aSelectNextTab;
	private MainFrameMenuAction aSelectPreviousTab;
	private MainFrameMenuAction aMoveLeftTab;
	private MainFrameMenuAction aMoveRightTab;
	private JMenu newInSelTabMenu;
	private JMenu editMenu;
	private JMenu documentMenu;
	private JMenu viewMenu;
	private JMenu typeMenu;
	private JMenu tabMenu;
	
	/**
	 * Creates a new tab with a document with specified type.
	 * 
	 * @param type Document type
	 */
	private EditorTab createEditorTab(String type) {
		EditorTab tab = new EditorTab(this);
		pane.addTab("", tab);
		tab.loadUntitled(type, "Untitled-"+(++untitledCount), editorPopup);
		return tab;
	}
	
	/**
	 * Creates a new tab with a document with specified file.
	 * 
	 * @param type File
	 */
	private EditorTab createEditorTab(File fsrc) {
		if (!fsrc.exists()) {
			JOptionPane.showMessageDialog(this, "Unable to open file \""+fsrc.getAbsolutePath()+"\"", "Open file", JOptionPane.ERROR_MESSAGE);
			return null;
		}
		EditorTab tab = new EditorTab(this);
		pane.addTab("", tab);
		if (!tab.loadFile(fsrc, editorPopup)) {
			tab.loadUntitled("Plain", "Untitled-"+(++untitledCount), editorPopup);
		}
		return tab;
	}
	
	/**
	 * Creates all components of the frame.
	 */
	private void createComponents() {
		// create actions
		MainFrameMenuAction aNewPl = new ActionNewSomeDocument(this, "Plain Document", new ImageIcon("resources/icons/plain.gif"));
		aNewPl.putValue(AbstractAction.SHORT_DESCRIPTION, "New Plain Document");
		aNewPl.putValue(AbstractAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		aNewPl.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_P);
		aNewPlainInSelected = new ActionNewSomeDocumentInSelected(this, "Plain Document", new ImageIcon("resources/icons/plain.gif"));
		aNewPlainInSelected.putValue(AbstractAction.SHORT_DESCRIPTION, "New Plain Document");
		aNewPlainInSelected.putValue(AbstractAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		aNewPlainInSelected.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_P);
		MainFrameMenuAction aOpen = new ActionOpen(this);
		aOpenInSelected = new ActionOpenInSelected(this);
		aClose = new ActionClose(this);
		aCloseAll = new ActionCloseAll(this);
		aCloseOthers = new ActionCloseOthers(this);
		aSave = new ActionSave(this);
		aSaveAs = new ActionSaveAs(this);
		aSaveAll = new ActionSaveAll(this);
		aPrint = new ActionPrint(this);
		MainFrameMenuAction aExit = new ActionExit(this);
		aUndo = new ActionUndo(this);
		aRedo = new ActionRedo(this);
		aCut = new ActionCut(this);
		aCopy = new ActionCopy(this);
		aPaste = new ActionPaste(this);
		aMoveUpLine = new ActionMoveUpLine(this);
		aMoveDownLine = new ActionMoveDownLine(this);
		aCutLine = new ActionCutLine(this);
		aCopyLine = new ActionCopyLine(this);
		aDuplicateLine = new ActionDuplicateLine(this);
		aDeleteLine = new ActionDeleteLine(this);
		aIndentBlock = new ActionIndentBlock(this);
		aOutdentBlock = new ActionOutdentBlock(this);
		aInsertTimeDate = new ActionInsertTimeDate(this);
		aInsertFilename = new ActionInsertFilename(this);
		aInsertPathFilename = new ActionInsertPathFilename(this);
		aToUppercase = new ActionToUppercase(this);
		aToLowercase = new ActionToLowercase(this);
		aConvertToCRLF = new ActionConvertToCRLF(this);
		aConvertToLF = new ActionConvertToLF(this);
		aConvertToCR = new ActionConvertToCR(this);
		aFind = new ActionFind(this);
		aFindNext = new ActionFindNext(this);
		aReplace = new ActionReplace(this);
		aSelectAll = new ActionSelectAll(this);
		aSetReadOnly = new JCheckBoxMenuItem(new ActionSetReadOnly(this));
		aSetReadOnly2 = new JCheckBoxMenuItem(new ActionSetReadOnly(this));
		
		aShowLineNumbers = new JCheckBoxMenuItem(new ActionShowLineNumbers(this));
		aSetWordWrap = new JCheckBoxMenuItem(new ActionSetWordWrap(this));
		MainFrameMenuAction aDefaultFont = new ActionDefaultFont(this);
		
		aRun = new ActionRun(this);
		aCompile = new ActionCompile(this);
		aCompileandrun = new ActionCompileandrun(this);
		aSetPlainType = new ActionSetDocType(this, "Plain Document", new ImageIcon("resources/icons/plain.gif"));
		aEditHighlightConfiguration = new ActionEditHighlightConfiguration(this);
		
		aShowToolBar = new JCheckBoxMenuItem(new ActionShowToolBar(this));
		aShowStatusBar = new JCheckBoxMenuItem(new ActionShowStatusBar(this));
		aShowConsolePane = new JCheckBoxMenuItem(new ActionShowConsolePane(this));
		aShowNavigator = new JCheckBoxMenuItem(new ActionShowNavigator(this));
		aSetMetal = new JCheckBoxMenuItem(new ActionSetMetal(this));
		aSetMotif = new JCheckBoxMenuItem(new ActionSetMotif(this));
		aSetWin = new JCheckBoxMenuItem(new ActionSetWin(this));
		MainFrameMenuAction aEditProfilesConfiguration = new ActionEditProfilesConfiguration(this);
		MainFrameMenuAction aPreferences = new ActionPreferences(this);
		
		aSelectNextTab = new ActionSelectNextTab(this);
		aSelectPreviousTab = new ActionSelectPreviousTab(this);
		aMoveLeftTab = new ActionMoveLeftTab(this);
		aMoveRightTab = new ActionMoveRightTab(this);
		MainFrameMenuAction aReadme = new ActionReadme(this);
		MainFrameMenuAction aProfilesInfo = new ActionProfilesInfo(this);
		MainFrameMenuAction aAbout = new ActionAbout(this);
		
		// menu File
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic('F');
		// submenu New
		JMenu newMenu = new JMenu("New");
		newMenu.setMnemonic('N');
		newMenu.add(aNewPl);
		// submenu New In Selected Tab
		newInSelTabMenu = new JMenu("New In Selected Tab");
		newInSelTabMenu.setMnemonic('e');
		newInSelTabMenu.add(aNewPlainInSelected);
		
		// submenu Open Recent
		JMenu recentMenu = new JMenu("Open Recent");
		recentMenu.setMnemonic('R');
		recentMenu.add(new ActionRecentMenuItemNoFiles());
		recentMenu.addMenuListener(new MenuListener() {
			public void menuSelected(MenuEvent e) {
				// generate list when menu is selected
				fileList.paint();
			}
			public void menuDeselected(MenuEvent e) {}
			public void menuCanceled(MenuEvent e) {}
		});
		// add items to menu File
		fileMenu.add(newMenu);
		fileMenu.add(newInSelTabMenu);
		fileMenu.addSeparator();
		fileMenu.add(aOpen);
		fileMenu.add(aOpenInSelected);
		fileMenu.add(recentMenu);
		fileMenu.addSeparator();
		fileMenu.add(aClose);
		fileMenu.add(aCloseAll);
		fileMenu.addSeparator();
		fileMenu.add(aSave);
		fileMenu.add(aSaveAs);
		fileMenu.add(aSaveAll);
		fileMenu.addSeparator();
		fileMenu.add(aPrint);
		fileMenu.addSeparator();
		fileMenu.add(aExit);
		
		// menu Edit
		editMenu = new JMenu("Edit");
		editMenu.setMnemonic('E');
		// submenu Line
		JMenu lineMenu = new JMenu("Line");
		lineMenu.setMnemonic('L');
		lineMenu.add(aMoveUpLine);
		lineMenu.add(aMoveDownLine);
		lineMenu.addSeparator();
		lineMenu.add(aCutLine);
		lineMenu.add(aCopyLine);
		lineMenu.addSeparator();
		lineMenu.add(aDuplicateLine);
		lineMenu.add(aDeleteLine);
		// submenu Block
		JMenu blockMenu = new JMenu("Block");
		blockMenu.setMnemonic('B');
		blockMenu.add(aIndentBlock);
		blockMenu.add(aOutdentBlock);
		// submenu Insert
		JMenu insertMenu = new JMenu("Insert");
		insertMenu.setMnemonic('I');
		insertMenu.add(aInsertTimeDate);
		insertMenu.addSeparator();
		insertMenu.add(aInsertFilename);
		insertMenu.add(aInsertPathFilename);
		// submenu Convert
		JMenu convertMenu = new JMenu("Convert");
		convertMenu.setMnemonic('C');
		convertMenu.add(aToUppercase);
		convertMenu.add(aToLowercase);
		convertMenu.addSeparator();
		convertMenu.add(aConvertToCRLF);
		convertMenu.add(aConvertToLF);
		convertMenu.add(aConvertToCR);
		// add items to menu Edit
		editMenu.add(aUndo);
		editMenu.add(aRedo);
		editMenu.addSeparator();
		editMenu.add(aCut);
		editMenu.add(aCopy);
		editMenu.add(aPaste);
		editMenu.addSeparator();
		editMenu.add(lineMenu);
		editMenu.add(blockMenu);
		editMenu.add(insertMenu);
		editMenu.add(convertMenu);
		editMenu.addSeparator();
		editMenu.add(aFind);
		editMenu.add(aFindNext);
		editMenu.add(aReplace);
		editMenu.addSeparator();
		editMenu.add(aSelectAll);
		editMenu.addSeparator();
		editMenu.add(aSetReadOnly);
		
		// menu View
		viewMenu = new JMenu("View");
		viewMenu.setMnemonic('V');
		// add items to menu View
		viewMenu.add(aShowLineNumbers);
		viewMenu.add(aSetWordWrap);
		viewMenu.addSeparator();
		viewMenu.add(aDefaultFont);
		
		// menu Document
		documentMenu = new JMenu("Document");
		documentMenu.setMnemonic('D');
		// submenu Set Profile
		typeMenu = new JMenu("Set Profile");
		typeMenu.setMnemonic('P');
		typeMenu.add(aSetPlainType);
		// add items to menu View
		documentMenu.add(aRun);
		documentMenu.add(aCompile);
		documentMenu.add(aCompileandrun);
		documentMenu.addSeparator();
		documentMenu.add(typeMenu);
		documentMenu.addSeparator();
		documentMenu.add(aEditHighlightConfiguration);
		
		// menu Settings
		JMenu settingsMenu = new JMenu("Settings");
		settingsMenu.setMnemonic('S');
		// submenu Look And Feel
		JMenu lookandfeelMenu = new JMenu("Look And Feel");
		lookandfeelMenu.setMnemonic('L');
		ButtonGroup group = new ButtonGroup();
		group.add(aSetMetal);
		group.add(aSetMotif);
		group.add(aSetWin);
		lookandfeelMenu.add(aSetMetal);
		lookandfeelMenu.add(aSetMotif);
		lookandfeelMenu.add(aSetWin);
		// add items to menu Settings
		settingsMenu.add(aShowToolBar);
		settingsMenu.add(aShowStatusBar);
		settingsMenu.add(aShowConsolePane);
		settingsMenu.add(aShowNavigator);
		settingsMenu.addSeparator();
		settingsMenu.add(lookandfeelMenu);
		settingsMenu.addSeparator();
		settingsMenu.add(aEditProfilesConfiguration);
		settingsMenu.add(aPreferences);
		
		// menu Tab
		tabMenu = new JMenu("Tab");
		tabMenu.setMnemonic('T');
		// menu Tabs List
		JMenu tablistMenu = new JMenu("Tab List");
		tablistMenu.add(new ActionTabsListMenuItemNoTabs());
		tablistMenu.setMnemonic('L');
		tablistMenu.addMenuListener(new MenuListener() {
			public void menuSelected(MenuEvent e) {
				// generate list when menu is selected
				tabList.paint();
			}
			public void menuDeselected(MenuEvent e) {}
			public void menuCanceled(MenuEvent e) {}
		});
		// add items to menu Tab
		tabMenu.add(tablistMenu);
		tabMenu.addSeparator();
		tabMenu.add(aSelectNextTab);
		tabMenu.add(aSelectPreviousTab);
		tabMenu.addSeparator();
		tabMenu.add(aMoveLeftTab);
		tabMenu.add(aMoveRightTab);
		
		// menu Help
		JMenu helpMenu = new JMenu("Help");
		helpMenu.setMnemonic('H');
		// add items to menu Help
		/*helpMenu.add(aContents);
		helpMenu.addSeparator();*/
		helpMenu.add(aReadme);
		helpMenu.add(aProfilesInfo);
		helpMenu.addSeparator();
		helpMenu.add(aAbout);
		
		// place menu bar
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.add(fileMenu);
		menuBar.add(editMenu);
		menuBar.add(viewMenu);
		menuBar.add(documentMenu);
		menuBar.add(settingsMenu);
		menuBar.add(tabMenu);
		menuBar.add(helpMenu);
		menuBar.setFocusTraversalKeysEnabled(false);
		
		// create tool bar
		toolBar = new JToolBar();
		toolBar.add(aNewPl);
		toolBar.addSeparator();
		toolBar.add(aOpen);
		toolBar.add(aSave);
		toolBar.add(aSaveAll);
		toolBar.addSeparator();
		toolBar.add(aCut);
		toolBar.add(aCopy);
		toolBar.add(aPaste);
		toolBar.addSeparator();
		toolBar.add(aUndo);
		toolBar.add(aRedo);
		toolBar.addSeparator();
		toolBar.add(aRun);
		toolBar.add(aCompile);
		toolBar.add(aCompileandrun);
		toolBar.addSeparator();
		toolBar.add(aFind);
		toolBar.add(aReplace);
		toolBar.addSeparator();
		toolBar.add(aPreferences);
		toolBar.setFocusable(false);
		toolBar.setFocusTraversalKeysEnabled(false);
		
		// create pop up menu
		editorPopup = new JPopupMenu();
		editorPopup.add(aUndo);
		editorPopup.add(aRedo);
		editorPopup.addSeparator();
		editorPopup.add(aCut);
		editorPopup.add(aCopy);
		editorPopup.add(aPaste);
		editorPopup.addSeparator();
		editorPopup.add(aFind);
		editorPopup.add(aFindNext);
		editorPopup.add(aReplace);
		editorPopup.addSeparator();
		editorPopup.add(aSelectAll);
		
		// create pop up menu on tabs
		tabPopup = new JPopupMenu();
		// submenu New In Selected Tab
		JMenu newInSelTabMenu2 = new JMenu("New In Selected Tab");
		newInSelTabMenu2.add(aNewPlainInSelected);
		// add items to tab pop up menu
		tabPopup.add(aClose);
		tabPopup.add(aCloseOthers);
		tabPopup.add(aCloseAll);
		tabPopup.addSeparator();
		tabPopup.add(aSave);
		tabPopup.add(aSaveAs);
		tabPopup.addSeparator();
		tabPopup.add(aOpenInSelected);
		tabPopup.add(newInSelTabMenu2);
		tabPopup.addSeparator();
		tabPopup.add(aSetReadOnly2);
		
		// create console pop up menu
		consolePopup = new JPopupMenu();
		consolePopup.add(new AbstractAction("Copy") {
			@Override
			public void actionPerformed(ActionEvent evt) {
				try {
					consolePane.copy();
					getSelectedEditorTab().getLineNumbersPane().grabFocus();
				}
				catch (NullPointerException e) {}
			}
		});
		consolePopup.addSeparator();
		consolePopup.add(new AbstractAction("Clear") {
			@Override
			public void actionPerformed(ActionEvent evt) {
				try {
					consolePane.clear();
					getSelectedEditorTab().getLineNumbersPane().grabFocus();
				}
				catch (NullPointerException e) {}
			}
		});
		
		// create navigator pop up menu
		navigatorPopup = new JPopupMenu();
		navigatorPopup.add(new AbstractAction("Open File") {
			@Override
			public void actionPerformed(ActionEvent evt) {
				FileTreeNode node = fileTree.getSelectedFileTreeNode();
				if (node == null) {
					return;
				}
				FileTreeItem item = node.getFileTreeItem();
				if (item == null) {
					return;
				}
				if (!item.isDir()) {
					openDocument(fileTree.getSelectedFileTreeNode().getFileTreeItem().getFile());
				}
			}
		});
		navigatorPopup.addSeparator();
		navigatorPopup.add(new AbstractAction("Refresh") {
			@Override
			public void actionPerformed(ActionEvent evt) {
				fileTree.refresh();
			}
		});
		
		// add items of documents from config
		for (int i = 0; i < config.getNewActions().size(); i++) {
			newMenu.add(config.getNewActions().get(i));
		}
		for (int i = 0; i < config.getNewInSelActions().size(); i++) {
			newInSelTabMenu.add(config.getNewInSelActions().get(i));
			newInSelTabMenu2.add(config.getNewInSelActions().get(i));
		}
		for (int i = 0; i < config.getSettypeActions().size(); i++) {
			typeMenu.add(config.getSettypeActions().get(i));
		}
		
		// create open/save dialog
		fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File("."));
		
		// create console pane
		consolePane = new ConsolePane();
		consolePane.setPopupMenu(consolePopup);
		consolePane.setFocusTraversalKeysEnabled(false);
		
		// create tabbed pane
		pane = new ClosableTabbedPane();
		pane.setPopupMenu(tabPopup);
		pane.addChangeListener(new TabSelectionHandler());
		pane.addClosableTabbedPaneListener(new TabEventsHandler());
		new DropTarget(pane, new FileDropHandler());
		pane.setFocusTraversalKeysEnabled(false);
		
		// create status bar
		statusBar = new SimpleStatusBar(20);
		statusBar.addPanel("pos:", 0, 100, SimpleStatusBar.ALIGNMENT_LEFT, SimpleStatusBar.BORDER_LOWERED);
		statusBar.addPanel("sel:", 0, 100, SimpleStatusBar.ALIGNMENT_LEFT, SimpleStatusBar.BORDER_LOWERED);
		statusBar.addPanel("lines:", 0, 100, SimpleStatusBar.ALIGNMENT_LEFT, SimpleStatusBar.BORDER_LOWERED);
		statusBar.addPanel("", 100, 100, SimpleStatusBar.ALIGNMENT_LEFT, SimpleStatusBar.BORDER_LOWERED);
		statusBar.addPanel("", 0, 50, SimpleStatusBar.ALIGNMENT_CENTER, SimpleStatusBar.BORDER_LOWERED);
		statusBar.setFocusTraversalKeysEnabled(false);
		
		// create Navigator
		fileTree = new FileTree(null, null, 0, false, new ImageIcon("resources/icons/file.gif"), 
				new ImageIcon("resources/icons/folder.gif"), new ImageIcon("resources/icons/expanded.gif"));
		fileTree.addMouseListener(new FileTreeItemClickHandler());
		fileTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		fileTree.setFocusTraversalKeysEnabled(false);
		navigatorPane = new JScrollPane(fileTree);
		navigatorPane.setFocusTraversalKeysEnabled(false);
		
		// create navigatorSplitPane
		navigatorSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, pane, navigatorPane);
		navigatorSplitPane.setContinuousLayout(true); 
		navigatorSplitPane.setOneTouchExpandable(false);
		navigatorSplitPane.setResizeWeight(1.0);
		navigatorSplitPane.setFocusTraversalKeysEnabled(false);
		
		// create consoleSplitPane
		consoleSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, navigatorSplitPane, consolePane); 
		consoleSplitPane.setContinuousLayout(true); 
		consoleSplitPane.setOneTouchExpandable(false);
		consoleSplitPane.setBackground(Color.GRAY);
		consoleSplitPane.setResizeWeight(1.0);
		consoleSplitPane.setFocusTraversalKeysEnabled(false);
	
		// place components on frame
		Container contentPane = getContentPane();
		contentPane.add(toolBar, BorderLayout.NORTH);
		contentPane.add(consoleSplitPane, BorderLayout.CENTER);
		contentPane.add(statusBar, BorderLayout.SOUTH);
		
		// create dialogs
		preferencesDialog = new PreferencesDialog(this);
		
		// create lists
		tabList = new TabList(this, tablistMenu);
		fileList = new RecentFileList(this, recentMenu);
		documentList = new DocumentList(this);
	}
	
	
	/**
	 * Provides to drag and drop files from OS to open them.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	class FileDropHandler extends DropTargetAdapter {
		
		public void drop(DropTargetDropEvent evt) {
			try {
				DropTargetContext context = evt.getDropTargetContext();
				evt.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
				Transferable t  = evt.getTransferable();
				Object data = t.getTransferData(DataFlavor.javaFileListFlavor);
				if (data instanceof java.util.List) {
					java.util.List list = (java.util.List)data;
					for (int k=0; k<list.size(); k++) {
						Object dataLine = list.get(k);
						if (dataLine instanceof File) {
							if (!((File)dataLine).isDirectory()) {
								openDocument((File)dataLine);
							}
						}
					}
				}
				context.dropComplete(true);
			}
			catch (Exception e) {}
		}
		
	}
	
	/**
	 * Provides receiving window events.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class WindowEventsHandler extends WindowAdapter {
		
		@Override
		public void windowClosing(WindowEvent evt) {
			exit();
		}
		
	}
	
	/**
	 * Provides receiving events on closing and creating tabs.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class TabEventsHandler implements ClosableTabbedPaneListener {
		
		@Override
		public void tabClosing(ClosableTabbedPaneEvent evt) {
			EditorTab tab = (EditorTab)evt.getComponent();
			boolean closed = tab.close();
			if (closed) {
				pane.setCanCloseTabs(true);
			} else {
				pane.setCanCloseTabs(false);
				tab.getLineNumbersPane().grabFocus();
			}
		}
		
		@Override
		public void tabCreated(ClosableTabbedPaneEvent evt) {
			EditorTab tab = (EditorTab)evt.getComponent();
			pane.setSelectedComponent(tab);
		}
		
		@Override
		public void tabClosed(ClosableTabbedPaneEvent evt) {
			refreshAllComponents();
		}
		
	}
	
	/**
	 * Provides receiving events on selecting tabs
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class TabSelectionHandler implements ChangeListener {
		
		@Override
		public void stateChanged(ChangeEvent evt) {
			tabList.paint();
			try {
				EditorTab tab = getSelectedEditorTab();
				tab.updatePositionStatus();
				tab.updateTextStatus();
				tab.updateOverwriteStatus();
				refreshAllComponents();
				getSelectedEditorTab().getLineNumbersPane().grabFocus();
			}
			catch (NullPointerException e) {}
		}
		
	}
	
	/**
	 * Provides receiving mouse events on FileTree pane
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class FileTreeItemClickHandler extends MouseAdapter {
		
		@Override
		public void mouseClicked(MouseEvent evt) {
			if ( (!evt.isPopupTrigger()) && (evt.getClickCount() == 2) ) {
				FileTreeNode node = fileTree.getSelectedFileTreeNode();
				if (node == null) {
					return;
				}
				FileTreeItem item = node.getFileTreeItem();
				if (item == null) {
					return;
				}
				if (!item.isDir()) {
					openDocument(fileTree.getSelectedFileTreeNode().getFileTreeItem().getFile());
				}
				return;
			}
		}
		@Override
		public void mousePressed(MouseEvent evt) {
			fileTree.setSelectionRow(fileTree.getRowForLocation(evt.getX(), evt.getY()));
			// if right mouse button was pressed
			if (evt.isPopupTrigger()) {
				// show pop up menu
				navigatorPopup.show(evt.getComponent(), evt.getX(), evt.getY());
			}
		}
		
		@Override
		public void mouseReleased(MouseEvent evt) {
			// if right mouse button was pressed
			if (evt.isPopupTrigger()) {
				// show pop up menu
				navigatorPopup.show(evt.getComponent(), evt.getX(), evt.getY());
			}
		}
		
	}
	
}
