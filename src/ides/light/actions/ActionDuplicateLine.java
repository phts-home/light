package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionDuplicateLine extends MainFrameMenuAction {
	
	public ActionDuplicateLine(MainFrame owner) {
		super(owner, "Duplicate");
		putValue(SHORT_DESCRIPTION, "Duplicate");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_U);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().duplicateLine();
		}
		catch (NullPointerException e) {
		}
	}
	
}
