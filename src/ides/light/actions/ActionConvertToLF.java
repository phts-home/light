package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionConvertToLF extends MainFrameMenuAction {
	
	public ActionConvertToLF(MainFrame owner) {
		super(owner, "Unix Format (LF)");
		putValue(SHORT_DESCRIPTION, "Unix Format (LF)");
		putValue(MNEMONIC_KEY, KeyEvent.VK_N);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			String text = owner.getSelectedEditorTab().getCodeEditor().getText();
			text = text.replace("\r\n", "\n");
			text = text.replace("\r", "\n");
			owner.getSelectedEditorTab().getCodeEditor().setText(text);
		}
		catch (NullPointerException e) {}
	}
	
}
