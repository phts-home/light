package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionUndo extends MainFrameMenuAction {
	
	public ActionUndo(MainFrame owner) {
		super(owner, "Undo", new ImageIcon("resources/icons/undo.gif"));
		putValue(SHORT_DESCRIPTION, "Undo");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_U);
		this.owner = owner;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().doUndo();
		}
		catch (NullPointerException e) {
		}
	}
	
}
