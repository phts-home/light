package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionTabsListMenuItem extends MainFrameMenuAction {
	
	public ActionTabsListMenuItem(MainFrame owner, String caption, int id) {
		super(owner, id+1 + " " + caption);
		putValue(SHORT_DESCRIPTION, caption);
		this.owner = owner;
		this.id = id;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getPane().setSelectedIndex(id);
		}
		catch (NullPointerException e) {
		}
	}
	
	private int id;
	
}
