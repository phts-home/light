package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionExit extends MainFrameMenuAction {
	
	public ActionExit(MainFrame owner) {
		super(owner, "Exit");
		putValue(SHORT_DESCRIPTION, "Exit");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_X);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.exit();
		}
		catch (NullPointerException e) {
		}
	}
	
}
