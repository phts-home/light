package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionMoveDownLine extends MainFrameMenuAction {
	
	public ActionMoveDownLine(MainFrame owner) {
		super(owner, "Move Down");
		putValue(SHORT_DESCRIPTION, "Move Down");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_D);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().moveDownLine();
		}
		catch (NullPointerException e) {
		}
	}
	
}
