package ides.light.actions;

import ides.light.MainFrame;
import javax.swing.*;
import java.awt.event.*;


public class ActionToLowercase extends MainFrameMenuAction {
	
	public ActionToLowercase(MainFrame owner) {
		super(owner, "To Lowercase");
		putValue(SHORT_DESCRIPTION, "To Lowercase");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_L);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().convertToLowerCase();
		}
		catch (NullPointerException e) {}
	}
	
}
