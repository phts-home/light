package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionSelectPreviousTab extends MainFrameMenuAction {
	
	public ActionSelectPreviousTab(MainFrame owner) {
		super(owner, "Previous Tab");
		putValue(SHORT_DESCRIPTION, "Previous Tab");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.ALT_DOWN_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_P);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getPane().selectPreviousTab();
		}
		catch (NullPointerException e) {
		}
	}
	
}
