package ides.light.actions;

import ides.light.MainFrame;
import ides.light.parameters.Parameters;

import javax.swing.*;
import java.awt.event.*;


public class ActionSetMotif extends MainFrameMenuAction {
	
	public ActionSetMotif(MainFrame owner) {
		super(owner, "Motif");
		putValue(SHORT_DESCRIPTION, "Motif");
		putValue(MNEMONIC_KEY, KeyEvent.VK_O);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
			owner.updateComponentUI();
			Parameters.INTERFACE_LOOKANDFEEL = 1;
		}
		catch (Exception e) {}
	}
	
}
