package ides.light.actions;

import ides.light.MainFrame;
import ides.light.profiles.*;

import java.io.*;
import java.awt.event.*;


public class ActionEditHighlightConfiguration extends MainFrameMenuAction {
	
	public ActionEditHighlightConfiguration(MainFrame owner) {
		super(owner, "Customize Highlight");
		putValue(SHORT_DESCRIPTION, "Customize Highlight");
		putValue(MNEMONIC_KEY, KeyEvent.VK_H);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			String path = ((LightDocument)owner.getSelectedEditorTab().getCodeEditor().getCodeEditorDocument()).getProfile().getHighlightPath();
			if ( (path != null) && (!path.equals("")) ) {
				owner.openDocument(new File(path).getAbsoluteFile());
			}
		}
		catch (NullPointerException e) {
		}
	}
	
}
