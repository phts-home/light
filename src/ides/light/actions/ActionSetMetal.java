package ides.light.actions;

import ides.light.MainFrame;
import ides.light.parameters.Parameters;

import javax.swing.*;
import java.awt.event.*;


public class ActionSetMetal extends MainFrameMenuAction {
	
	public ActionSetMetal(MainFrame owner) {
		super(owner, "Metal");
		putValue(SHORT_DESCRIPTION, "Metal");
		putValue(MNEMONIC_KEY, KeyEvent.VK_M);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
			owner.updateComponentUI();
			Parameters.INTERFACE_LOOKANDFEEL = 0;
		}
		catch (Exception e) {}
	}
	
}
