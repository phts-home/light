package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionSaveAs extends MainFrameMenuAction {
	
	public ActionSaveAs(MainFrame owner) {
		super(owner, "Save As...");
		putValue(SHORT_DESCRIPTION, "Save As...");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_A);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().saveAs();
		}
		catch (NullPointerException e) {
		}
	}
	
}
