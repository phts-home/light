package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionMoveLeftTab extends MainFrameMenuAction {
	
	public ActionMoveLeftTab(MainFrame owner) {
		super(owner, "Move Left");
		putValue(SHORT_DESCRIPTION, "Move Left");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.CTRL_DOWN_MASK | InputEvent.ALT_DOWN_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_L);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getPane().moveLeft();
		}
		catch (NullPointerException e) {
		}
	}
	
}
