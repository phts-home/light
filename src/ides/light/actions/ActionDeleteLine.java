package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionDeleteLine extends MainFrameMenuAction {
	
	public ActionDeleteLine(MainFrame owner) {
		super(owner, "Delete");
		putValue(SHORT_DESCRIPTION, "Delete");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_D);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().deleteLine();
		}
		catch (NullPointerException e) {
		}
	}
	
}
