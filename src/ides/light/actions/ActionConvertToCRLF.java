package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionConvertToCRLF extends MainFrameMenuAction {
	
	public ActionConvertToCRLF(MainFrame owner) {
		super(owner, "Windows Format (CR + LF)");
		putValue(SHORT_DESCRIPTION, "Windows Format (CR + LF)");
		putValue(MNEMONIC_KEY, KeyEvent.VK_W);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			String text = owner.getSelectedEditorTab().getCodeEditor().getText();
			text = text.replace("\n", "\r\n");
			text = text.replace("\r", "\r\n");
			text = text.replace("\r\r\n", "\r\n");
			text = text.replace("\r\n\n", "\r\n");
			owner.getSelectedEditorTab().getCodeEditor().setText(text);
		}
		catch (NullPointerException e) {}
	}
	
}
