package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionSave extends MainFrameMenuAction {
	
	public ActionSave(MainFrame owner) {
		super(owner, "Save", new ImageIcon("resources/icons/save.gif"));
		putValue(SHORT_DESCRIPTION, "Save");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_S);
		this.owner = owner;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().save();
		}
		catch (NullPointerException e) {
		}
	}
	
	private MainFrame owner;
	
}
