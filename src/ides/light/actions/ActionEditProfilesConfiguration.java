package ides.light.actions;

import ides.light.MainFrame;

import java.io.*;
import java.awt.event.*;


public class ActionEditProfilesConfiguration extends MainFrameMenuAction {
	
	public ActionEditProfilesConfiguration(MainFrame owner) {
		super(owner, "Customize Profiles");
		putValue(SHORT_DESCRIPTION, "Customize Profiles");
		putValue(MNEMONIC_KEY, KeyEvent.VK_U);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.openDocument(new File("profiles/profiles.xml").getAbsoluteFile());
		}
		catch (NullPointerException e) {
		}
	}
	
}
