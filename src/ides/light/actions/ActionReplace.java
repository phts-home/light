package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionReplace extends MainFrameMenuAction {
	
	public ActionReplace(MainFrame owner) {
		super(owner, "Replace...", new ImageIcon("resources/icons/replace.gif"));
		putValue(SHORT_DESCRIPTION, "Replace...");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_R);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().doReplace();
		}
		catch (NullPointerException e) {
		}
	}
	
}
