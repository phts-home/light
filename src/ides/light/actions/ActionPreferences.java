package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;



public class ActionPreferences extends MainFrameMenuAction {
	
	public ActionPreferences(MainFrame owner) {
		super(owner, "Preferences...", new ImageIcon("resources/icons/preferences.gif"));
		putValue(SHORT_DESCRIPTION, "Preferences...");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_P);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getPreferencesDialog().show();
		}
		catch (NullPointerException e) {
		}
	}
	
}
