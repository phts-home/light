package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionCopy extends MainFrameMenuAction {
	
	public ActionCopy(MainFrame owner) {
		super(owner, "Copy", new ImageIcon("resources/icons/copy.gif"));
		putValue(SHORT_DESCRIPTION, "Copy");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_C);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().copy();
		}
		catch (NullPointerException e) {
		}
	}
	
}
