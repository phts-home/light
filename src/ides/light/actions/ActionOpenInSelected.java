package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionOpenInSelected extends MainFrameMenuAction {
	
	public ActionOpenInSelected(MainFrame owner) {
		super(owner, "Open In Selected Tab...");
		putValue(SHORT_DESCRIPTION, "Open In Selected Tab...");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_T);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.openDocumentInSelectedTab();
		}
		catch (NullPointerException e) {
		}
	}
	
}
