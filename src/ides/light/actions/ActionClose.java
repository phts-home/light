package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionClose extends MainFrameMenuAction {
	
	public ActionClose(MainFrame owner) {
		super(owner, "Close");
		putValue(SHORT_DESCRIPTION, "Close");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_C);
		this.owner = owner;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getPane().closeTab();
		}
		catch (NullPointerException e) {
		}
	}
	
	private MainFrame owner;
}
