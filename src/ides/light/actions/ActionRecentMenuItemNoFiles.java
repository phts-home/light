package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionRecentMenuItemNoFiles extends MainFrameMenuAction {
	
	public ActionRecentMenuItemNoFiles() {
		super("No Recent Files");
		putValue(SHORT_DESCRIPTION, "No Recent Files");
		setEnabled(false);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
	}
	
}
