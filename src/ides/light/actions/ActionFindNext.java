package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionFindNext extends MainFrameMenuAction {
	
	public ActionFindNext(MainFrame owner) {
		super(owner, "Find Next");
		putValue(SHORT_DESCRIPTION, "Find Next");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		putValue(MNEMONIC_KEY, KeyEvent.VK_N);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().doFindNext();
		}
		catch (NullPointerException e) {
		}
	}
	
}
