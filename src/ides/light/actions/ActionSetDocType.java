package ides.light.actions;

import ides.light.EditorTab;
import ides.light.MainFrame;

import javax.swing.*;

import java.awt.event.*;


public class ActionSetDocType extends MainFrameMenuAction {
	
	public ActionSetDocType(MainFrame owner, String caption, Icon icon) {
		super(owner, caption, icon);
		putValue(SHORT_DESCRIPTION, caption);
		this.owner = owner;
		this.caption = caption;
		this.icon = icon;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			EditorTab tab = owner.getSelectedEditorTab();
			tab.getCodeEditor().getCodeEditorDocument().setType(owner.getConfig().getTypeByMenuCaption(caption));
			tab.getCodeEditor().updateCodeEditorHighlighter();
			owner.refreshAllComponents();
			owner.getPane().setDocumentIcon(tab, icon);
		}
		catch (NullPointerException e) {
		}
	}
	
	private MainFrame owner;
	private String caption;
	private Icon icon;
	
}
