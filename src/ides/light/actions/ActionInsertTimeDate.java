package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;

import java.awt.event.*;
import java.util.Date;
import java.text.DateFormat;


public class ActionInsertTimeDate extends MainFrameMenuAction {
	
	public ActionInsertTimeDate(MainFrame owner) {
		super(owner, "Time/Date");
		putValue(SHORT_DESCRIPTION, "Time/Date");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		putValue(MNEMONIC_KEY, KeyEvent.VK_T);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			String str = DateFormat.getInstance().format(new Date());
			owner.getSelectedEditorTab().getCodeEditor().insert(str);
		}
		catch (NullPointerException e) {
		}
	}
	
}
