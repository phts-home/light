package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;

import java.awt.event.*;


public class ActionInsertPathFilename extends MainFrameMenuAction {
	
	public ActionInsertPathFilename(MainFrame owner) {
		super(owner, "Path And Filename");
		putValue(SHORT_DESCRIPTION, "Path And Filename");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F7, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_P);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().insert(owner.getSelectedEditorTab().getOpenedFile().getAbsolutePath());
		}
		catch (NullPointerException e) {
		}
	}
	
}
