package ides.light.actions;

import ides.light.MainFrame;
import ides.light.parameters.Parameters;

import javax.swing.*;
import java.awt.event.*;


public class ActionSetWin extends MainFrameMenuAction {
	
	public ActionSetWin(MainFrame owner) {
		super(owner, "Windows");
		putValue(SHORT_DESCRIPTION, "Windows");
		putValue(MNEMONIC_KEY, KeyEvent.VK_W);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			owner.updateComponentUI();
			Parameters.INTERFACE_LOOKANDFEEL = 2;
		}
		catch (Exception e) {}
	}
	
}
