package ides.light.actions;

import ides.light.MainFrame;
import javax.swing.*;
import java.awt.event.*;


public class ActionOutdentBlock extends MainFrameMenuAction {
	
	public ActionOutdentBlock(MainFrame owner) {
		super(owner, "Outdent");
		putValue(SHORT_DESCRIPTION, "Outdent");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_O);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().outdent();
		}
		catch (NullPointerException e) {}
	}
	
}
