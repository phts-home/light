package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionCopyLine extends MainFrameMenuAction {
	
	public ActionCopyLine(MainFrame owner) {
		super(owner, "Copy");
		putValue(SHORT_DESCRIPTION, "Copy");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_C);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().copyLine();
		}
		catch (NullPointerException e) {
		}
	}
	
}
