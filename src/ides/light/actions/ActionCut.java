package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionCut extends MainFrameMenuAction {
	
	public ActionCut(MainFrame owner) {
		super(owner, "Cut", new ImageIcon("resources/icons/cut.gif"));
		putValue(SHORT_DESCRIPTION, "Cut");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_T);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().cut();
		}
		catch (NullPointerException e) {
		}
	}
	
}
