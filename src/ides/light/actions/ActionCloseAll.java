package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionCloseAll extends MainFrameMenuAction {
	
	public ActionCloseAll(MainFrame owner) {
		super(owner, "Close All");
		putValue(SHORT_DESCRIPTION, "Close All");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_L);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.closeAll();
		}
		catch (NullPointerException e) {
		}
	}
	
}
