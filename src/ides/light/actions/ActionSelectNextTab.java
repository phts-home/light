package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionSelectNextTab extends MainFrameMenuAction {
	
	public ActionSelectNextTab(MainFrame owner) {
		super(owner, "Next Tab");
		putValue(SHORT_DESCRIPTION, "Next Tab");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.ALT_DOWN_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_N);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getPane().selectNextTab();
		}
		catch (NullPointerException e) {
		}
	}
	
}
