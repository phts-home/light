package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionTabsListMenuItemNoTabs extends MainFrameMenuAction {
	
	public ActionTabsListMenuItemNoTabs() {
		super("No Tabs");
		putValue(SHORT_DESCRIPTION, "No Windows");
		setEnabled(false);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	}
	
}
