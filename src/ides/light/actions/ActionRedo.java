package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionRedo extends MainFrameMenuAction {
	
	public ActionRedo(MainFrame owner) {
		super(owner, "Redo", new ImageIcon("resources/icons/redo.gif"));
		putValue(SHORT_DESCRIPTION, "Redo");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_R);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().doRedo();
		}
		catch (NullPointerException e) {
		}
	}
	
}
