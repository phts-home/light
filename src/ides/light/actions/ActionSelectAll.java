package ides.light.actions;
import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionSelectAll extends MainFrameMenuAction {
	
	public ActionSelectAll(MainFrame owner) {
		super(owner, "Select All");
		putValue(SHORT_DESCRIPTION, "Select All");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_A);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().selectAll();
		}
		catch (NullPointerException e) {
		}
	}
	
}
