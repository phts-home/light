package ides.light.actions;

import ides.light.MainFrame;
import ides.light.parameters.Parameters;

import javax.swing.*;

import java.awt.Toolkit;
import java.awt.event.*;


public class ActionAbout extends MainFrameMenuAction {
	
	public ActionAbout(MainFrame owner) {
		super(owner, "About...");
		putValue(SHORT_DESCRIPTION, "About...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_A);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		JOptionPane.showMessageDialog(owner,
				"<html><b>"
					+"<h3>"+Parameters.PROGRAM_NAME+"</h3>"
					+"<br>Version "+Parameters.PROGRAM_VERSION+"."
					+Parameters.PROGRAM_BUILD+"</b></html>\n\n"
					+"<html>&#169; 2008 by "+Parameters.PROGRAM_AUTHORS[0]+" and "+Parameters.PROGRAM_AUTHORS[1]
					+"</html>",
				"About", JOptionPane.INFORMATION_MESSAGE, 
				new ImageIcon("resources/icons/light-32-32.gif"));
	}
}
