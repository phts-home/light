package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionRun extends MainFrameMenuAction {
	
	public ActionRun(MainFrame owner) {
		super(owner, "Run", new ImageIcon("resources/icons/run.gif"));
		putValue(SHORT_DESCRIPTION, "Run");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0));
		putValue(MNEMONIC_KEY, KeyEvent.VK_R);
		this.owner = owner;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().getCodeEditorDocument().run();
		}
		catch (NullPointerException e) {
		}
	}
	
	private MainFrame owner;
}
