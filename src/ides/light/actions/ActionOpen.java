package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionOpen extends MainFrameMenuAction {
	
	public ActionOpen(MainFrame owner) {
		super(owner, "Open...", new ImageIcon("resources/icons/open.gif"));
		putValue(SHORT_DESCRIPTION, "Open...");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_O);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.openDocument();
		}
		catch (NullPointerException e) {
		}
	}
	
}
