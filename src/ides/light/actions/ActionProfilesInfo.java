package ides.light.actions;

import ides.light.MainFrame;

import java.io.*;
import java.awt.event.*;


public class ActionProfilesInfo extends MainFrameMenuAction {
	
	public ActionProfilesInfo(MainFrame owner) {
		super(owner, "Profiles Info");
		putValue(SHORT_DESCRIPTION, "Profiles Info");
		putValue(MNEMONIC_KEY, KeyEvent.VK_R);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.openDocument(new File("profiles/info.txt").getAbsoluteFile());
			if (owner.getSelectedEditorTab().getOpenedFile().getAbsolutePath().equals(new File("profiles/info.txt").getAbsolutePath())) {
				owner.setReadOnly(!owner.getSelectedEditorTab().doGetReadOnly());
			}
		}
		catch (NullPointerException e) {
		}
	}
	
}
