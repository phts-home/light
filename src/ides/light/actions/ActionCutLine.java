package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;

import java.awt.event.*;


public class ActionCutLine extends MainFrameMenuAction {
	
	public ActionCutLine(MainFrame owner) {
		super(owner, "Cut");
		putValue(SHORT_DESCRIPTION, "Cut");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_T);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().cutLine();
		}
		catch (NullPointerException e) {
		}
	}
	
}
