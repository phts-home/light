package ides.light.actions;

import ides.light.MainFrame;
import javax.swing.*;
import java.awt.event.*;


public class ActionToUppercase extends MainFrameMenuAction {
	
	public ActionToUppercase(MainFrame owner) {
		super(owner, "To Uppercase");
		putValue(SHORT_DESCRIPTION, "To Uppercase");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_U);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().convertToUpperCase();
		}
		catch (NullPointerException e) {}
	}
	
}
