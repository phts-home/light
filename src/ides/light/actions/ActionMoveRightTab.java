package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionMoveRightTab extends MainFrameMenuAction {
	
	public ActionMoveRightTab(MainFrame owner) {
		super(owner, "Move Right");
		putValue(SHORT_DESCRIPTION, "Move Right");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.CTRL_DOWN_MASK | InputEvent.ALT_DOWN_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_R);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getPane().moveRight();
		}
		catch (NullPointerException e) {
		}
	}
	
}
