package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionRecentMenuItem extends MainFrameMenuAction {
	
	public ActionRecentMenuItem(MainFrame owner, String caption, int id) {
		super(owner, id+1 + " " + caption);
		putValue(SHORT_DESCRIPTION, caption);
		this.id = id;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getRecentFileList().execute(id);
		}
		catch (NullPointerException e) {
		}
	}
	
	private int id;
	
}
