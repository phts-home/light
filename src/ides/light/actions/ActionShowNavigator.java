package ides.light.actions;
import ides.light.MainFrame;
import ides.light.parameters.Parameters;

import javax.swing.*;
import java.awt.event.*;


public class ActionShowNavigator extends MainFrameMenuAction {
	
	public ActionShowNavigator(MainFrame owner) {
		super(owner, "Show Navigator");
		putValue(SHORT_DESCRIPTION, "Show Navigator");
		putValue(MNEMONIC_KEY, KeyEvent.VK_N);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		owner.setNavigatorVisible(!Parameters.INTERFACE_NAVIGATOR_VISIBLE);
	}
	
}
