package ides.light.actions;

import java.awt.event.*;

import ides.light.MainFrame;

import javax.swing.*;


public abstract class MainFrameMenuAction extends AbstractAction {
	
	public MainFrameMenuAction(String name) {
		super(name);
		this.owner = null;
	}
	
	public MainFrameMenuAction(MainFrame owner, String name) {
		super(name);
		this.owner = owner;
	}
	
	public MainFrameMenuAction(MainFrame owner, String name, Icon icon) {
		super(name, icon);
		this.owner = owner;
	}
	
	public void actionPerformed(ActionEvent evt) {
		try {
			owner.getSelectedEditorTab().getLineNumbersPane().grabFocus();
		}
		catch (NullPointerException e) {
		}
	}
	
	protected MainFrame owner;
	
}
