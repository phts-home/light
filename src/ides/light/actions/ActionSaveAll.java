package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;

import java.awt.event.*;


public class ActionSaveAll extends MainFrameMenuAction {
	
	public ActionSaveAll(MainFrame owner) {
		super(owner, "Save All", new ImageIcon("resources/icons/saveall.gif"));
		putValue(SHORT_DESCRIPTION, "Save All");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_E);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.saveAll();
		}
		catch (NullPointerException e) {
		}
	}
	
}
