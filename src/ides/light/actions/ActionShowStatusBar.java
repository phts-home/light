package ides.light.actions;
import ides.light.MainFrame;
import ides.light.parameters.Parameters;

import javax.swing.*;
import java.awt.event.*;


public class ActionShowStatusBar extends MainFrameMenuAction {
	
	public ActionShowStatusBar(MainFrame owner) {
		super(owner, "Show Status Bar");
		putValue(SHORT_DESCRIPTION, "Show Status Bar");
		putValue(MNEMONIC_KEY, KeyEvent.VK_B);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		owner.setStatusBarVisible(!Parameters.INTERFACE_STATUSBAR_VISIBLE);
	}
	
}
