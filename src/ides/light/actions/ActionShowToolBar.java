package ides.light.actions;
import ides.light.MainFrame;
import ides.light.parameters.Parameters;

import javax.swing.*;
import java.awt.event.*;


public class ActionShowToolBar extends MainFrameMenuAction {
	
	public ActionShowToolBar(MainFrame owner) {
		super(owner, "Show Toolbar");
		putValue(SHORT_DESCRIPTION, "Show Toolbar");
		putValue(MNEMONIC_KEY, KeyEvent.VK_T);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		owner.setToolBarVisible(!Parameters.INTERFACE_TOOLBAR_VISIBLE);
	}
	
}
