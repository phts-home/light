package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionMoveUpLine extends MainFrameMenuAction {
	
	public ActionMoveUpLine(MainFrame owner) {
		super(owner, "Move Up");
		putValue(SHORT_DESCRIPTION, "Move Up");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_UP, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_U);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().moveUpLine();
		}
		catch (NullPointerException e) {
		}
	}
	
}
