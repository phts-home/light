package ides.light.actions;

import ides.light.MainFrame;
import ides.light.parameters.Parameters;

import javax.swing.*;

import dialogs.*;
import dialogs.event.*;

import java.awt.event.*;



public class ActionDefaultFont extends MainFrameMenuAction {
	
	public ActionDefaultFont(MainFrame owner) {
		super(owner, "Default Font...");
		putValue(SHORT_DESCRIPTION, "Default Font...");
		putValue(MNEMONIC_KEY, KeyEvent.VK_F);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			FontChooser fontChooser = new FontChooser(owner, "Default font", owner.getIconImage());
			fontChooser.addFontChooserListener(new FontChooserListener() {
				@Override
				public void buttonPressed(FontChooserEvent evt) {
					if ( (evt.getButton() == AbstractDialogEvent.OK_BUTTON) || (evt.getButton() == AbstractDialogEvent.APPLY_BUTTON) ) {
						Parameters.EDITOR_FONT_APPEARANCE = evt.getDialog().getSelectedFont();
						Parameters.EDITOR_FONT_COLOR = evt.getDialog().getSelectedFontColor();
						owner.getPreferences().apply();
					}
				}
			});
			fontChooser.showFontDialog(Parameters.EDITOR_FONT_APPEARANCE, Parameters.EDITOR_FONT_COLOR);
		}
		catch (NullPointerException e) {
		}
	}
	
}
