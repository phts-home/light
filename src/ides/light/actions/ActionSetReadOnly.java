package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionSetReadOnly extends MainFrameMenuAction {
	
	public ActionSetReadOnly(MainFrame owner) {
		super(owner, "Read Only");
		putValue(SHORT_DESCRIPTION, "Read Only");
		putValue(MNEMONIC_KEY, KeyEvent.VK_E);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.setReadOnly(!owner.getSelectedEditorTab().doGetReadOnly());
		}
		catch (NullPointerException e) {
		}
	}
	
}
