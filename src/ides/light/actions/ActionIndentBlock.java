package ides.light.actions;

import ides.light.MainFrame;
import javax.swing.*;
import java.awt.event.*;


public class ActionIndentBlock extends MainFrameMenuAction {
	
	public ActionIndentBlock(MainFrame owner) {
		super(owner, "Indent");
		putValue(SHORT_DESCRIPTION, "Indent");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_I);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().indent();
		}
		catch (NullPointerException e) {}
	}
	
}
