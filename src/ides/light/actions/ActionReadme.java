package ides.light.actions;

import ides.light.MainFrame;

import java.io.*;
import java.awt.event.*;


public class ActionReadme extends MainFrameMenuAction {
	
	public ActionReadme(MainFrame owner) {
		super(owner, "Readme");
		putValue(SHORT_DESCRIPTION, "Readme");
		putValue(MNEMONIC_KEY, KeyEvent.VK_R);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.openDocument(new File("readme.txt").getAbsoluteFile());
			if (owner.getSelectedEditorTab().getOpenedFile().getAbsolutePath().equals(new File("readme.txt").getAbsolutePath())) {
				owner.setReadOnly(!owner.getSelectedEditorTab().doGetReadOnly());
			}
		}
		catch (NullPointerException e) {
		}
	}
	
}
