package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionPrint extends MainFrameMenuAction {
	
	public ActionPrint(MainFrame owner) {
		super(owner, "Print...", new ImageIcon("resources/icons/print.gif"));
		putValue(SHORT_DESCRIPTION, "Print...");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_P);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().print();
		}
		catch (java.awt.print.PrinterException e) {}
		catch (NullPointerException e) {}
	}
	
}
