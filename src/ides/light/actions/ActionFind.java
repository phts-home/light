package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionFind extends MainFrameMenuAction {
	
	public ActionFind(MainFrame owner) {
		super(owner, "Find...", new ImageIcon("resources/icons/find.gif"));
		putValue(SHORT_DESCRIPTION, "Find...");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_F);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().doFind();
		}
		catch (NullPointerException e) {
		}
	}
	
}
