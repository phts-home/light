package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionCloseOthers extends MainFrameMenuAction {
	
	public ActionCloseOthers(MainFrame owner) {
		super(owner, "Close Others");
		putValue(SHORT_DESCRIPTION, "Close Others");
		putValue(MNEMONIC_KEY, KeyEvent.VK_H);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.closeOthers();
		}
		catch (NullPointerException e) {
		}
	}
	
}
