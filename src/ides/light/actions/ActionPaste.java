package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionPaste extends MainFrameMenuAction {
	
	public ActionPaste(MainFrame owner) {
		super(owner, "Paste", new ImageIcon("resources/icons/paste.gif"));
		putValue(SHORT_DESCRIPTION, "Paste");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_P);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().paste();
		}
		catch (NullPointerException e) {
		}
	}
	
}
