package ides.light.actions;
import ides.light.MainFrame;
import ides.light.parameters.Parameters;

import javax.swing.*;
import java.awt.event.*;


public class ActionShowConsolePane extends MainFrameMenuAction {
	
	public ActionShowConsolePane(MainFrame owner) {
		super(owner, "Show Console Pane");
		putValue(SHORT_DESCRIPTION, "Show Console Pane");
		putValue(MNEMONIC_KEY, KeyEvent.VK_C);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		owner.setConsolePaneVisible(!Parameters.INTERFACE_CONSOLEPANE_VISIBLE);
	}
	
}
