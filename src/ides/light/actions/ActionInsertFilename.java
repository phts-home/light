package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;

import java.awt.event.*;


public class ActionInsertFilename extends MainFrameMenuAction {
	
	public ActionInsertFilename(MainFrame owner) {
		super(owner, "Filename");
		putValue(SHORT_DESCRIPTION, "Filename");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0));
		putValue(MNEMONIC_KEY, KeyEvent.VK_F);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().insert(owner.getSelectedEditorTab().getOpenedFile().getName());
		}
		catch (NullPointerException e) {
		}
	}
	
}
