package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;

import java.awt.event.*;


public class ActionCompileandrun extends MainFrameMenuAction {
	
	public ActionCompileandrun(MainFrame owner) {
		super(owner, "Compile And Run", new ImageIcon("resources/icons/compileandrun.gif"));
		putValue(SHORT_DESCRIPTION, "Compile And Run");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F11, InputEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_A);
		this.owner = owner;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().getCodeEditorDocument().compileAndRun();
		}
		catch (NullPointerException e) {
		}
	}
	
	private MainFrame owner;
}
