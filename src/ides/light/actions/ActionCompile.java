package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;

import java.awt.event.*;


public class ActionCompile extends MainFrameMenuAction {
	
	public ActionCompile(MainFrame owner) {
		super(owner, "Compile", new ImageIcon("resources/icons/compile.gif"));
		putValue(SHORT_DESCRIPTION, "Compile");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0));
		putValue(MNEMONIC_KEY, KeyEvent.VK_C);
		this.owner = owner;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			owner.getSelectedEditorTab().getCodeEditor().getCodeEditorDocument().compile();
		}
		catch (NullPointerException e) {
		}
	}
	
	private MainFrame owner;
}
