package ides.light.actions;

import ides.light.MainFrame;
import ides.light.parameters.Parameters;

import javax.swing.*;
import java.awt.event.*;


public class ActionShowLineNumbers extends MainFrameMenuAction {
	
	public ActionShowLineNumbers(MainFrame owner) {
		super(owner, "Line Numbers");
		putValue(SHORT_DESCRIPTION, "Line Numbers");
		putValue(MNEMONIC_KEY, KeyEvent.VK_L);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			Parameters.EDITOR_LINENUMBERS_VISIBLE = !Parameters.EDITOR_LINENUMBERS_VISIBLE;
			owner.getPreferences().apply();
		}
		catch (NullPointerException e) {
		}
	}
	
}
