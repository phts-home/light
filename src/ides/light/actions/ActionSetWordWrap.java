package ides.light.actions;

import ides.light.MainFrame;
import ides.light.parameters.Parameters;

import javax.swing.*;
import java.awt.event.*;


public class ActionSetWordWrap extends MainFrameMenuAction {
	
	public ActionSetWordWrap(MainFrame owner) {
		super(owner, "Word Wrap");
		putValue(SHORT_DESCRIPTION, "Word Wrap");
		putValue(MNEMONIC_KEY, KeyEvent.VK_W);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			Parameters.EDITOR_LINEWRAP_ENABLE = !Parameters.EDITOR_LINEWRAP_ENABLE;
			owner.getPreferences().apply();
		}
		catch (NullPointerException e) {
		}
	}
	
}
