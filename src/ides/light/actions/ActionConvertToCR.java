package ides.light.actions;

import ides.light.MainFrame;

import javax.swing.*;
import java.awt.event.*;


public class ActionConvertToCR extends MainFrameMenuAction {
	
	public ActionConvertToCR(MainFrame owner) {
		super(owner, "Mac Format (CR)");
		putValue(SHORT_DESCRIPTION, "Mac Format (CR)");
		putValue(MNEMONIC_KEY, KeyEvent.VK_M);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		super.actionPerformed(evt);
		try {
			String text = owner.getSelectedEditorTab().getCodeEditor().getText();
			text = text.replace("\r\n", "\r");
			text = text.replace("\n", "\r");
			owner.getSelectedEditorTab().getCodeEditor().setText(text);
		}
		catch (NullPointerException e) {}
	}
	
}
