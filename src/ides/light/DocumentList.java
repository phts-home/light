package ides.light;

import org.w3c.dom.Element;

import documents.SimpleXMLDocument;

import preferences.PreferencesApplicable;
import preferences.PreferencesCustomizable;


import ides.light.parameters.Parameters;

import java.io.*;
import java.util.*;


/**
 * Opened document list to provide to remember the current session.
 * 
 * @author Phil Tsarik
 *
 */
public class DocumentList implements PreferencesCustomizable, PreferencesApplicable {
	
	/**
	 * Constructs a new instance of the <code>DocumentList</code> class.
	 * 
	 * @param owner The program's main frame
	 */
	public DocumentList(MainFrame owner) {
		this.owner = owner;
		this.enable = true;
		documents = new ArrayList<String>();
		
		owner.getPreferences().addCustomizable(this);
		owner.getPreferences().addApplicable(this);
	}
	
	@Override
	public void loadPreferences(SimpleXMLDocument doc) {
		Element elem = doc.getElement(null, "last_session_documents");
		Parameters.GENERAL_REMEMBERSESSION_ENABLE = doc.readBooleanValue(elem, "enable", true);
		selIndex = doc.readIntegerValue(elem, "selected", 0);
		documents = doc.readStringArray(null, "last_session_documents", "document", "", documents);
	}
	
	@Override
	public void savePreferences(SimpleXMLDocument doc) {
		Element elem = doc.getElement(null, "last_session_documents");
		doc.addProperty(elem, "enable", Parameters.GENERAL_REMEMBERSESSION_ENABLE);
		doc.addProperty(elem, "selected", selIndex);
		if (Parameters.GENERAL_REMEMBERSESSION_ENABLE) {
			doc.addArrayProperty(null, "last_session_documents", "document", documents);
		}
	}
	
	@Override
	public void applyPreferences() {
		enable = Parameters.GENERAL_REMEMBERSESSION_ENABLE;
		if (!enable) {
			documents.clear();
		}
	}
	
	/**
	 * Adds opened document's filename.
	 * 
	 * @param filename Opened document's filename
	 */
	public void add(String filename) {
		if (enable) {
			documents.add(0, filename);
		}
	}
	
	/**
	 * Removes the specified document's filename.
	 * 
	 * @param filename Document's filename
	 */
	public void remove(String filename) {
		documents.remove(filename);
	}
	
	/**
	 * Clears list.
	 */
	public void clear() {
		documents.clear();
	}
	
	/**
	 * Opens all documents (if list is enable).
	 */
	public void execute() {
		if (enable) {
			for (int i = 0; i < documents.size(); i++) {
				owner.openDocument(new File(documents.get(i)).getAbsoluteFile());
			}
			if (selIndex < owner.getPane().getTabCount()) {
				owner.getPane().setSelectedIndex(selIndex);
			}
		}
	}
	
	/**
	 * Sets index of selected document after opening.
	 * 
	 * @param selIndex index of selected document
	 */
	public void setSelectedDoc(int selIndex) {
		this.selIndex = selIndex;
	}
	
	private MainFrame owner;
	private boolean enable;
	private int selIndex;
	private ArrayList <String> documents;
	
}
