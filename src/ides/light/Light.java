package ides.light;

import ides.light.parameters.*;
import preferences.SimpleProgramPreferences;


/**
 * Main entry program class.<br><br>
 * 
 * Light is a simple programmer's text editor with many features.
 * There are some of them:
 * <ul>
 *   <li>Syntax highlighting for languages.
 *   <li>Possibility to create/change your own document profiles.
 *   <li>Edit multiple documents simultaneously.
 * </ul>
 * 
 * @author Phil Tsarik
 * @author Sergey Tuchina
 *
 */
public class Light {
	
	/**
	 * Program entry point.
	 * 
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		// create preferences
		SimpleProgramPreferences preferences = new SimpleProgramPreferences(Parameters.PROGRAM_PREFERENCES_PATH, "preferences");
		
		// create main frame
		new MainFrame(preferences, args);
	}
	
}
