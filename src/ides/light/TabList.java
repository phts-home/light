package ides.light;

import ides.light.actions.ActionTabsListMenuItem;
import ides.light.actions.ActionTabsListMenuItemNoTabs;

import javax.swing.*;


/**
 * Tab list, which paints them captions to the menu. This list does not
 * contain the captions and it only writes them to menu as required.
 * 
 * @author Phil Tsarik
 *
 */
public class TabList {
	
	/**
	 * Constructs a new instance of the <code>TabList</code> class.
	 * 
	 * @param owner The program's main frame
	 * @param menu The menu that should contain tab captions items.
	 */
	public TabList(MainFrame owner, JMenu menu) {
		this.owner = owner;
		this.menu = menu;
	}
	
	/**
	 * Writes captions to the menu.
	 */
	public void paint() {
		menu.removeAll();
		ButtonGroup group = new ButtonGroup();
		int c = owner.getPane().getTabCount();
		if (c == 0) {
			menu.add(new ActionTabsListMenuItemNoTabs());
			return;
		}
		EditorTab tab;
		int sel = owner.getPane().getSelectedIndex();
		for (int i = 0; i < c; i++) {
			tab = (EditorTab)owner.getPane().getComponentAt(i);
			JCheckBoxMenuItem item = new JCheckBoxMenuItem(new ActionTabsListMenuItem(owner, tab.getOpenedFile().getName(), i));
			if (i != sel) {
				item.setSelected(false);
			} else {
				item.setSelected(true);
			}
			group.add(item);
			menu.add(item);
		}
	}
	
	private MainFrame owner;
	private JMenu menu;
}
