package ides.light;

import ides.light.actions.ActionRecentMenuItem;
import ides.light.actions.ActionRecentMenuItemNoFiles;
import ides.light.parameters.Parameters;

import javax.swing.*;

import org.w3c.dom.Element;

import documents.SimpleXMLDocument;

import preferences.PreferencesApplicable;
import preferences.PreferencesCustomizable;


import java.io.*;
import java.util.*;


/**
 * Recent file list.
 * 
 * @author Phil Tsarik
 *
 */
public class RecentFileList implements PreferencesCustomizable, PreferencesApplicable {
	
	/**
	 * Constructs a new instance of the <code>RecentFileList</code> class.
	 * 
	 * @param owner The program's main frame
	 * @param menu The menu which must contain this list
	 */
	public RecentFileList(MainFrame owner, JMenu menu) {
		this.owner = owner;
		this.menu = menu;
		this.enable = true;
		this.maxnumber = 10;
		this.items = new ArrayList<String>();
		owner.getPreferences().addCustomizable(this);
		owner.getPreferences().addApplicable(this);
	}
	
	@Override
	public void loadPreferences(SimpleXMLDocument doc) {
		Element elem = doc.getElement(null, "recent_file_list");
		Parameters.GENERAL_RECENTFILES_ENABLE = doc.readBooleanValue(elem, "enable", true);
		Parameters.GENERAL_RECENTFILES_NUMBER = doc.readIntegerValue(elem, "maxnumber", 10);
		
		items = doc.readStringArray(null, "recent_file_list", "file", "", items);
	}
	
	@Override
	public void savePreferences(SimpleXMLDocument doc) {
		Element elem = doc.getElement(null, "recent_file_list");
		doc.addProperty(elem, "enable", enable);
		doc.addProperty(elem, "maxnumber", maxnumber);
		if (enable) {
			doc.addArrayProperty(null, "recent_file_list", "file", items);
		}
	}
	
	@Override
	public void applyPreferences() {
		enable = Parameters.GENERAL_RECENTFILES_ENABLE;
		maxnumber = Parameters.GENERAL_RECENTFILES_NUMBER;
		while ( (items.size() > 1) && (items.size() > maxnumber) ) {
			items.remove(items.size()-1);
		}
	}
	
	/**
	 * Paints list in the menu.
	 */
	public void paint() {
		menu.removeAll();
		int c = items.size();
		if ( (!enable) || (c == 0) ) {
			menu.add(new ActionRecentMenuItemNoFiles());
			return;
		}
		for (int i = 0; i < c; i++) {
			JMenuItem item = new JMenuItem(new ActionRecentMenuItem(owner, items.get(i), i));
			menu.add(item);
		}
	}
	
	/**
	 * Adds item  to list.
	 * 
	 * @param caption The caption of menu item
	 */
	public void add(String caption) {
		while ( (items.size() > 1) && (items.size() >= Parameters.GENERAL_RECENTFILES_NUMBER) ) {
			items.remove(items.size()-1);
		}
		while (items.contains(caption)) {
			items.remove(caption);
		}
		items.add(0, caption);
	}
	
	/**
	 * Remove item from list.
	 * 
	 * @param caption The caption of menu item
	 */
	public void remove(String caption) {
		items.remove(caption);
	}
	
	/**
	 * Executes an item.
	 * 
	 * @param index Item index
	 */
	public void execute(int index) {
		File f = new File(items.get(index));
		if (f.exists()) {
			owner.openDocument(f);
		} else {
			int res = JOptionPane.showConfirmDialog(owner, "Unable to open file \""+f.getAbsolutePath()+"\". Remove from list?", Parameters.PROGRAM_NAME, JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
			if (res == JOptionPane.YES_OPTION) {
				remove(f.getAbsolutePath());
			}
		}
	}
	
	private MainFrame owner;
	private boolean enable;
	private int maxnumber;
	private JMenu menu;
	private ArrayList <String> items;
}
