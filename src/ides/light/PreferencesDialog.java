package ides.light;

import ides.light.parameters.Parameters;

import dialogs.*;
import dialogs.event.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import components.ColorSelector;


/**
 * The program's "Preferences" dialog.
 * 
 * @author Phil Tsarik
 *
 */
public class PreferencesDialog extends AbstractDialog {
	
	/**
	 * Constructs a new instance of the <code>PreferencesDialog</code> class.
	 * 
	 * @param owner The program's main frame
	 */
	public PreferencesDialog(MainFrame owner) {
		super(owner, "Preferences", owner.getIconImage(), 500, 400);
		this.owner = owner;
	}
	
	private MainFrame owner;
	private JTabbedPane pane;
	
	// tab General components
	private JCheckBox cbGeneralSplashVisible;
	private JCheckBox cbGeneralRecentfilesEnable;
	private JSpinner spGeneralRecentfilesNumber;
	private JCheckBox cbGeneralRemembersessionEnable;
	private JCheckBox cbGeneralAutodetectEnable;
	
	// tab Interface components
	private JRadioButton rbInterfaceTabbarPosition1;
	private JRadioButton rbInterfaceTabbarPosition2;
	private JRadioButton rbInterfaceTabbarPosition3;
	private JRadioButton rbInterfaceTabbarPosition4;
	private JCheckBox cbInterfaceToolbarVisible;
	private JCheckBox cbInterfaceStatubarVisible;
	private JCheckBox cbInterfaceConsoleVisible;
	private JCheckBox cbInterfaceNavVisible;
	private JCheckBox cbTabbarClosebuttonsVisible;
	private JCheckBox cbTabbarDocumenticonsVisible;
	
	// tab Editor components
	private JCheckBox cbEditorLineNumbers;
	private JCheckBox cbEditorLineWrap;
	private JSpinner spEditorTabsSize;
	private JRadioButton rbEditorTabsKeeptabs;
	private JRadioButton rbEditorTabsInsertspaces;
	private JSpinner spEditorNavDepth;
	private JCheckBox cbEditorNavSorted;
	
	// tab Appearance components
	private JButton butDefFont;
	private JButton butConsFont;
	private JButton butNavFont;
	
	private Font defFont;
	private Font consFont;
	private Font navFont;
	private Color defFontColor;
	private Color consFontColor;
	private Color navFontColor;
	
	// tab Colors components
	private ColorSelector csColorsEditorBackground;
	private ColorSelector csColorsEditorCursor;
	private ColorSelector csColorsEditorSelection;
	private ColorSelector csColorsEditorSeltext;
	private ColorSelector csColorsLinenumbersBackgr;
	private ColorSelector csColorsLinenumbersForegr;
	private ColorSelector csColorsConsoleBackgr;
	private ColorSelector csColorsNavigatorBackgr;
	private ColorSelector csColorsNavigatorSelection;
	private ColorSelector csColorsNavigatorSeltext;
	
	
	@Override
	protected void refresh() {
		// tab General
		cbGeneralSplashVisible.setSelected(Parameters.GENERAL_SPLASHSCREEN_VISIBLE);
		cbGeneralRecentfilesEnable.setSelected(Parameters.GENERAL_RECENTFILES_ENABLE);
		spGeneralRecentfilesNumber.setValue(Parameters.GENERAL_RECENTFILES_NUMBER);
		cbGeneralRemembersessionEnable.setSelected(Parameters.GENERAL_REMEMBERSESSION_ENABLE);
		cbGeneralAutodetectEnable.setSelected(Parameters.GENERAL_AUTODETECTTYPE_ENABLE);
		
		// tab Interface
		cbInterfaceToolbarVisible.setSelected(Parameters.INTERFACE_TOOLBAR_VISIBLE);
		cbInterfaceStatubarVisible.setSelected(Parameters.INTERFACE_STATUSBAR_VISIBLE);
		cbInterfaceConsoleVisible.setSelected(Parameters.INTERFACE_CONSOLEPANE_VISIBLE);
		cbInterfaceNavVisible.setSelected(Parameters.INTERFACE_NAVIGATOR_VISIBLE);
		switch (Parameters.INTERFACE_TABBAR_POSITION) {
		case 0:
			rbInterfaceTabbarPosition1.setSelected(true);
			break;
		case 1:
			rbInterfaceTabbarPosition2.setSelected(true);
			break;
		case 2:
			rbInterfaceTabbarPosition3.setSelected(true);
			break;
		case 3:
			rbInterfaceTabbarPosition4.setSelected(true);
			break;
		}
		cbTabbarClosebuttonsVisible.setSelected(Parameters.INTERFACE_TABBAR_CLOSEBUTTONS_VISIBLE);
		cbTabbarDocumenticonsVisible.setSelected(Parameters.INTERFACE_TABBAR_DOCUMENTICONS_VISIBLE);
		
		// tab Editor
		cbEditorLineNumbers.setSelected(Parameters.EDITOR_LINENUMBERS_VISIBLE);
		cbEditorLineWrap.setSelected(Parameters.EDITOR_LINEWRAP_ENABLE);
		spEditorTabsSize.setValue(Parameters.EDITOR_TAB_SIZE);
		rbEditorTabsKeeptabs.setSelected(true);
		rbEditorTabsInsertspaces.setSelected(Parameters.EDITOR_TAB_INSERTSPACES_ENABLE);
		spEditorNavDepth.setValue(Parameters.NAVIGATOR_DEPTH_VALUE);
		cbEditorNavSorted.setSelected(Parameters.NAVIGATOR_SORTED_ENABLE);
		
		// tab Fonts
		defFont = Parameters.EDITOR_FONT_APPEARANCE;
		consFont = Parameters.CONSOLE_FONT_APPEARANCE;
		navFont = Parameters.NAVIGATOR_FONT_APPEARANCE;
		defFontColor = Parameters.EDITOR_FONT_COLOR;
		consFontColor = Parameters.CONSOLE_FONT_COLOR;
		navFontColor = Parameters.NAVIGATOR_FONT_COLOR;
		butDefFont.setText(defFont.getFamily()
				+ "," + defFont.getSize()
				+ (defFont.isBold()?",bold":"")
				+ (defFont.isItalic()?",italic":"")
				+ ",rgb{" + defFontColor.getRed() + "," + defFontColor.getGreen() + "," + defFontColor.getBlue() + "}"
				);
		butConsFont.setText(consFont.getFamily()
				+ "," + consFont.getSize()
				+ (consFont.isBold()?",bold":"")
				+ (consFont.isItalic()?",italic":"")
				+ ",rgb{" + consFontColor.getRed() + "," + consFontColor.getGreen() + "," + consFontColor.getBlue() + "}"
				);
		butNavFont.setText(navFont.getFamily()
				+ "," + navFont.getSize()
				+ (navFont.isBold()?",bold":"")
				+ (navFont.isItalic()?",italic":"")
				+ ",rgb{" + navFontColor.getRed() + "," + navFontColor.getGreen() + "," + navFontColor.getBlue() + "}"
				);
		
		// tab Colors
		csColorsEditorBackground.setSelectedColor(Parameters.EDITOR_BACKGR_COLOR);
		csColorsEditorCursor.setSelectedColor(Parameters.EDITOR_CURSOR_COLOR);
		csColorsEditorSelection.setSelectedColor(Parameters.EDITOR_SELECTION_COLOR);
		csColorsEditorSeltext.setSelectedColor(Parameters.EDITOR_SELECTEDTEXT_COLOR);
		csColorsLinenumbersBackgr.setSelectedColor(Parameters.EDITOR_LINENUMBERS_BACKGR_COLOR);
		csColorsLinenumbersForegr.setSelectedColor(Parameters.EDITOR_LINENUMBERS_FOREGR_COLOR);
		csColorsConsoleBackgr.setSelectedColor(Parameters.CONSOLE_BACKGR_COLOR);
		csColorsNavigatorBackgr.setSelectedColor(Parameters.NAVIGATOR_BACKGR_COLOR);
		csColorsNavigatorSelection.setSelectedColor(Parameters.NAVIGATOR_SELECTION_COLOR);
		csColorsNavigatorSeltext.setSelectedColor(Parameters.NAVIGATOR_SELECTEDTEXT_COLOR);
	}
	
	@Override
	protected void apply() {
		// tab General
		Parameters.GENERAL_SPLASHSCREEN_VISIBLE = cbGeneralSplashVisible.isSelected();
		Parameters.GENERAL_RECENTFILES_ENABLE = cbGeneralRecentfilesEnable.isSelected();
		Parameters.GENERAL_RECENTFILES_NUMBER = (Integer)spGeneralRecentfilesNumber.getValue();
		Parameters.GENERAL_REMEMBERSESSION_ENABLE = cbGeneralRemembersessionEnable.isSelected();
		Parameters.GENERAL_AUTODETECTTYPE_ENABLE = cbGeneralAutodetectEnable.isSelected();
		
		// tab Interface
		Parameters.INTERFACE_TOOLBAR_VISIBLE = cbInterfaceToolbarVisible.isSelected();
		Parameters.INTERFACE_STATUSBAR_VISIBLE = cbInterfaceStatubarVisible.isSelected();
		Parameters.INTERFACE_CONSOLEPANE_VISIBLE = cbInterfaceConsoleVisible.isSelected();
		Parameters.INTERFACE_NAVIGATOR_VISIBLE = cbInterfaceNavVisible.isSelected();
		if (rbInterfaceTabbarPosition1.isSelected())
			Parameters.INTERFACE_TABBAR_POSITION = 0;
		else
			if (rbInterfaceTabbarPosition2.isSelected()) 
				Parameters.INTERFACE_TABBAR_POSITION = 1;
			else
				if (rbInterfaceTabbarPosition3.isSelected())
					Parameters.INTERFACE_TABBAR_POSITION = 2;
				else
					Parameters.INTERFACE_TABBAR_POSITION = 3;
		
		Parameters.INTERFACE_TABBAR_CLOSEBUTTONS_VISIBLE = cbTabbarClosebuttonsVisible.isSelected();
		Parameters.INTERFACE_TABBAR_DOCUMENTICONS_VISIBLE = cbTabbarDocumenticonsVisible.isSelected();
		
		// tab Editor
		Parameters.EDITOR_LINENUMBERS_VISIBLE = cbEditorLineNumbers.isSelected();
		Parameters.EDITOR_LINEWRAP_ENABLE = cbEditorLineWrap.isSelected();
		Parameters.EDITOR_TAB_SIZE = (Integer)spEditorTabsSize.getValue();
		Parameters.EDITOR_TAB_INSERTSPACES_ENABLE = rbEditorTabsInsertspaces.isSelected();
		Parameters.NAVIGATOR_DEPTH_VALUE = (Integer)spEditorNavDepth.getValue();
		Parameters.NAVIGATOR_SORTED_ENABLE = cbEditorNavSorted.isSelected();
		
		// tab Fonts
		Parameters.EDITOR_FONT_APPEARANCE = defFont;
		Parameters.CONSOLE_FONT_APPEARANCE = consFont;
		Parameters.NAVIGATOR_FONT_APPEARANCE = navFont;
		Parameters.EDITOR_FONT_COLOR = defFontColor;
		Parameters.CONSOLE_FONT_COLOR = consFontColor;
		Parameters.NAVIGATOR_FONT_COLOR = navFontColor;
		
		// tab Colors
		Parameters.EDITOR_BACKGR_COLOR = csColorsEditorBackground.getSelectedColor();
		Parameters.EDITOR_CURSOR_COLOR = csColorsEditorCursor.getSelectedColor();
		Parameters.EDITOR_SELECTION_COLOR = csColorsEditorSelection.getSelectedColor();
		Parameters.EDITOR_SELECTEDTEXT_COLOR = csColorsEditorSeltext.getSelectedColor();
		Parameters.EDITOR_LINENUMBERS_BACKGR_COLOR = csColorsLinenumbersBackgr.getSelectedColor();
		Parameters.EDITOR_LINENUMBERS_FOREGR_COLOR = csColorsLinenumbersForegr.getSelectedColor();
		Parameters.CONSOLE_BACKGR_COLOR = csColorsConsoleBackgr.getSelectedColor();
		Parameters.NAVIGATOR_BACKGR_COLOR = csColorsNavigatorBackgr.getSelectedColor();
		Parameters.NAVIGATOR_SELECTION_COLOR = csColorsNavigatorSelection.getSelectedColor();
		Parameters.NAVIGATOR_SELECTEDTEXT_COLOR = csColorsNavigatorSeltext.getSelectedColor();
		
		// apply parameters
		owner.getPreferences().apply();
	}
	
	@Override
	protected Component createComponentPanel() {
		pane = new JTabbedPane();
		// create tabs
		createGeneralTab();
		createInterfaceTab();
		createEditorTab();
		createFontsTab();
		createColorsTab();
		return pane;
	}
	
	/**
	 * Creates tab "General".
	 */
	private void createGeneralTab() {
		JPanel generalTab = new JPanel();
		GridBagLayout layout = new GridBagLayout();
		generalTab.setLayout(layout);
		
		// init components
		cbGeneralSplashVisible = new JCheckBox("Show splash screen");
		spGeneralRecentfilesNumber = new JSpinner(new SpinnerNumberModel(10, 1, 20, 1));
		cbGeneralRecentfilesEnable = new JCheckBox("Enable");
		cbGeneralRemembersessionEnable = new JCheckBox("Remember the current session for next launch");
		cbGeneralAutodetectEnable = new JCheckBox("Autodetect document type by file extension");
		
		// panel "Recent files list"
		JPanel panelRecentFiles = new JPanel();
		panelRecentFiles.setLayout(layout);
		panelRecentFiles.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), " Recent files list "));
		
		// create constraints
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		
		// put components on the tab
		addComponentToGrid(cbGeneralRecentfilesEnable, constraints, 0, 0, 1, 1, 100, panelRecentFiles, layout);
		addComponentToGrid(new JLabel("Max items number:"), constraints, 0, 1, 1, 1, 100, panelRecentFiles, layout);
		addComponentToGrid(spGeneralRecentfilesNumber, constraints, 1, 1, 1, 1, 30, panelRecentFiles, layout);
		
		addComponentToGrid(cbGeneralSplashVisible, constraints, 0, 0, 1, 1, 100, generalTab, layout);
		addComponentToGrid(panelRecentFiles, constraints, 0, 1, 1, 1, 100, generalTab, layout);
		addComponentToGrid(cbGeneralRemembersessionEnable, constraints, 0, 2, 1, 1, 100, generalTab, layout);
		addComponentToGrid(cbGeneralAutodetectEnable, constraints, 0, 3, 1, 1, 100, generalTab, layout);
		
		// add tab into the tabbed pane
		pane.add("Global", generalTab);
	}
	
	/**
	 * Creates tab "Interface".
	 */
	private void createInterfaceTab() {
		JPanel interfaceTab = new JPanel();
		GridBagLayout layout = new GridBagLayout();
		interfaceTab.setLayout(layout);
		
		// init components
		cbInterfaceToolbarVisible = new JCheckBox("Show toolbar");
		cbInterfaceStatubarVisible = new JCheckBox("Show status bar");
		cbInterfaceConsoleVisible = new JCheckBox("Show console pane");
		cbInterfaceNavVisible = new JCheckBox("Show navigator");
		rbInterfaceTabbarPosition1 = new JRadioButton("Top");
		rbInterfaceTabbarPosition2 = new JRadioButton("Bottom");
		rbInterfaceTabbarPosition3 = new JRadioButton("Left");
		rbInterfaceTabbarPosition4 = new JRadioButton("Right");
		cbTabbarClosebuttonsVisible = new JCheckBox("Show close buttons");
		cbTabbarDocumenticonsVisible = new JCheckBox("Show document icons");
		
		// panel "Position"
		JPanel panelTabbarPosition = new JPanel();
		panelTabbarPosition.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), " Position "));
		ButtonGroup gr1 = new ButtonGroup();
		gr1.add(rbInterfaceTabbarPosition1);
		gr1.add(rbInterfaceTabbarPosition2);
		gr1.add(rbInterfaceTabbarPosition3);
		gr1.add(rbInterfaceTabbarPosition4);
		panelTabbarPosition.add(rbInterfaceTabbarPosition1);
		panelTabbarPosition.add(rbInterfaceTabbarPosition2);
		panelTabbarPosition.add(rbInterfaceTabbarPosition3);
		panelTabbarPosition.add(rbInterfaceTabbarPosition4);
		
		// panel "Tab bar"
		JPanel panelTabbar = new JPanel();
		panelTabbar.setLayout(layout);
		panelTabbar.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), " Tab bar "));
		
		// create constraints
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		
		// put components on the tab
		addComponentToGrid(cbTabbarClosebuttonsVisible, constraints, 0, 0, 1, 1, 100, panelTabbar, layout);
		addComponentToGrid(cbTabbarDocumenticonsVisible, constraints, 0, 1, 1, 1, 100, panelTabbar, layout);
		addComponentToGrid(panelTabbarPosition, constraints, 0, 2, 1, 1, 100, panelTabbar, layout);
		
		addComponentToGrid(cbInterfaceToolbarVisible, constraints, 0, 0, 1, 1, 100, interfaceTab, layout);
		addComponentToGrid(cbInterfaceStatubarVisible, constraints, 0, 1, 1, 1, 100, interfaceTab, layout);
		addComponentToGrid(cbInterfaceConsoleVisible, constraints, 0, 2, 1, 1, 100, interfaceTab, layout);
		addComponentToGrid(cbInterfaceNavVisible, constraints, 0, 3, 1, 1, 100, interfaceTab, layout);
		addComponentToGrid(panelTabbar, constraints, 0, 4, 1, 1, 100, interfaceTab, layout);
		
		// add tab into the tabbed pane
		pane.add("Interface", interfaceTab);
	}
	
	/**
	 * Creates tab "Editor".
	 */
	private void createEditorTab() {
		JPanel editorTab = new JPanel();
		GridBagLayout layout = new GridBagLayout();
		editorTab.setLayout(layout);
		
		// init components
		cbEditorLineNumbers = new JCheckBox("Show line numbers");
		cbEditorLineWrap = new JCheckBox("Line wrap");
		spEditorTabsSize = new JSpinner(new SpinnerNumberModel(10, 1, 20, 1));
		rbEditorTabsKeeptabs = new JRadioButton("Keep tabs");
		rbEditorTabsInsertspaces = new JRadioButton("Insert spaces");
		
		spEditorNavDepth = new JSpinner(new SpinnerNumberModel(0, -1, 5, 1));
		cbEditorNavSorted = new JCheckBox("Sorted");
		
		// panel "Tabs"
		JPanel panelTabs = new JPanel();
		panelTabs.setLayout(layout);
		panelTabs.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), " Tabs "));
		ButtonGroup gr1 = new ButtonGroup();
		gr1.add(rbEditorTabsKeeptabs);
		gr1.add(rbEditorTabsInsertspaces);
		
		// panel "Navigator"
		JPanel panelNavigator = new JPanel();
		panelNavigator.setLayout(layout);
		panelNavigator.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), " Navigator "));
		
		// create constraints
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		
		// put components on the tab
		addComponentToGrid(new JLabel("Tab size:"), constraints, 0, 0, 1, 1, 100, panelTabs, layout);
		addComponentToGrid(spEditorTabsSize, constraints, 1, 0, 1, 1, 30, panelTabs, layout);
		addComponentToGrid(rbEditorTabsKeeptabs, constraints, 0, 1, 1, 1, 100, panelTabs, layout);
		addComponentToGrid(rbEditorTabsInsertspaces, constraints, 0, 2, 1, 1, 100, panelTabs, layout);
		
		addComponentToGrid(new JLabel("Depth:"), constraints, 0, 0, 1, 1, 100, panelNavigator, layout);
		addComponentToGrid(spEditorNavDepth, constraints, 1, 0, 1, 1, 30, panelNavigator, layout);
		addComponentToGrid(cbEditorNavSorted, constraints, 0, 1, 1, 1, 100, panelNavigator, layout);
		
		addComponentToGrid(cbEditorLineNumbers, constraints, 0, 0, 1, 1, 100, editorTab, layout);
		addComponentToGrid(cbEditorLineWrap, constraints, 0, 1, 1, 1, 100, editorTab, layout);
		addComponentToGrid(panelTabs, constraints, 0, 2, 1, 1, 100, editorTab, layout);
		addComponentToGrid(panelNavigator, constraints, 0, 3, 1, 1, 100, editorTab, layout);
		
		// add tab into the tabbed pane
		pane.add("Editor", editorTab);
	}
	
	/**
	 * Creates tab "Fonts".
	 */
	private void createFontsTab() {
		JPanel fontsTab = new JPanel();
		GridBagLayout layout = new GridBagLayout();
		fontsTab.setLayout(layout);
		
		// init components
		butDefFont = new JButton("Change");
		butDefFont.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				FontChooser fontChooser = new FontChooser(owner, "Editor default font", owner.getIconImage());
				fontChooser.addFontChooserListener(new FontChooserListener() {
					@Override
					public void buttonPressed(FontChooserEvent evt) {
						if ( (evt.getButton() == AbstractDialogEvent.OK_BUTTON) || (evt.getButton() == AbstractDialogEvent.APPLY_BUTTON) ) {
							defFont = evt.getDialog().getSelectedFont();
							defFontColor = evt.getDialog().getSelectedFontColor();
							butDefFont.setText(defFont.getFamily() 
									+ "," + defFont.getSize() 
									+ (defFont.isBold()?",bold":"")
									+ (defFont.isItalic()?",italic":"")
									+ ",rgb{" + defFontColor.getRed() + "," + defFontColor.getGreen() + "," + defFontColor.getBlue() + "}"
									);
						}
					}
				});
				fontChooser.showFontDialog(defFont, defFontColor);
			}
		});
		butConsFont = new JButton("Change");
		butConsFont.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				FontChooser fontChooser = new FontChooser(owner, "Console font", owner.getIconImage());
				fontChooser.addFontChooserListener(new FontChooserListener() {
					@Override
					public void buttonPressed(FontChooserEvent evt) {
						if ( (evt.getButton() == AbstractDialogEvent.OK_BUTTON) || (evt.getButton() == AbstractDialogEvent.APPLY_BUTTON) ) {
							consFont = evt.getDialog().getSelectedFont();
							consFontColor = evt.getDialog().getSelectedFontColor();
							butConsFont.setText(consFont.getFamily() 
									+ "," + consFont.getSize() 
									+ (consFont.isBold()?",bold":"")
									+ (consFont.isItalic()?",italic":"")
									+ ",rgb{" + consFontColor.getRed() + "," + consFontColor.getGreen() + "," + consFontColor.getBlue() + "}"
									);
						}
					}
				});
				fontChooser.showFontDialog(consFont, consFontColor);
			}
		});
		
		butNavFont = new JButton("Change");
		butNavFont.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				FontChooser fontChooser = new FontChooser(owner, "Navigator font", owner.getIconImage());
				fontChooser.addFontChooserListener(new FontChooserListener() {
					@Override
					public void buttonPressed(FontChooserEvent evt) {
						if ( (evt.getButton() == AbstractDialogEvent.OK_BUTTON) || (evt.getButton() == AbstractDialogEvent.APPLY_BUTTON) ) {
							navFont = evt.getDialog().getSelectedFont();
							navFontColor = evt.getDialog().getSelectedFontColor();
							butNavFont.setText(navFont.getFamily() 
									+ "," + navFont.getSize() 
									+ (navFont.isBold()?",bold":"")
									+ (navFont.isItalic()?",italic":"")
									+ ",rgb{" + navFontColor.getRed() + "," + navFontColor.getGreen() + "," + navFontColor.getBlue() + "}"
									);
						}
					}
				});
				fontChooser.showFontDialog(navFont, navFontColor);
			}
		});
		
		// panel Default font
		JPanel panelFont = new JPanel();
		panelFont.setLayout(layout);
		panelFont.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), " Editor (default font) "));
		
		// panel Console pane
		JPanel panelConsole = new JPanel();
		panelConsole.setLayout(layout);
		panelConsole.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), " Console pane "));
		
		// panel Navigator
		JPanel panelNavigator = new JPanel();
		panelNavigator.setLayout(layout);
		panelNavigator.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), " Navigator "));
		
		// create constraints
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		
		// put components on the tab
		addComponentToGrid(butDefFont, constraints, 0, 0, 1, 1, 100, panelFont, layout);
		addComponentToGrid(butConsFont, constraints, 0, 0, 1, 1, 100, panelConsole, layout);
		addComponentToGrid(butNavFont, constraints, 0, 0, 1, 1, 100, panelNavigator, layout);
		
		addComponentToGrid(panelFont, constraints, 0, 0, 1, 1, 100, fontsTab, layout);
		addComponentToGrid(panelConsole, constraints, 0, 1, 1, 1, 100, fontsTab, layout);
		addComponentToGrid(panelNavigator, constraints, 0, 2, 1, 1, 100, fontsTab, layout);
		
		// add tab into the tabbed pane
		pane.add("Fonts", fontsTab);
	}
	
	/**
	 * Creates tab "Colors".
	 */
	private void createColorsTab() {
		JPanel editorTab = new JPanel();
		GridBagLayout layout = new GridBagLayout();
		editorTab.setLayout(layout);
		
		// init components
		csColorsEditorBackground = new ColorSelector();
		csColorsEditorCursor = new ColorSelector();
		csColorsEditorSelection = new ColorSelector();
		csColorsEditorSeltext = new ColorSelector();
		csColorsLinenumbersBackgr = new ColorSelector();
		csColorsLinenumbersForegr = new ColorSelector();
		csColorsConsoleBackgr = new ColorSelector();
		csColorsNavigatorBackgr = new ColorSelector();
		csColorsNavigatorSelection = new ColorSelector();
		csColorsNavigatorSeltext = new ColorSelector();
		
		// panel "Editor"
		JPanel panelEditor = new JPanel();
		panelEditor.setLayout(layout);
		panelEditor.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), " Editor "));
		
		// panel "Line numbers"
		JPanel panelLinenumbers = new JPanel();
		panelLinenumbers.setLayout(layout);
		panelLinenumbers.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), " Line numbers pane "));
		
		// panel "Console pane"
		JPanel panelConsole = new JPanel();
		panelConsole.setLayout(layout);
		panelConsole.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), " Console pane "));
		
		// panel "Navigator"
		JPanel panelNavigator = new JPanel();
		panelNavigator.setLayout(layout);
		panelNavigator.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), " Navigator "));
		
		// create constraints
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		
		// put components on the tab
		addComponentToGrid(new JLabel("Background:"), constraints, 0, 0, 1, 1, 100, panelEditor, layout);
		addComponentToGrid(new JLabel("Cursor:"), constraints, 0, 1, 1, 1, 100, panelEditor, layout);
		addComponentToGrid(new JLabel("Selection:"), constraints, 0, 2, 1, 1, 100, panelEditor, layout);
		addComponentToGrid(new JLabel("Selected text:"), constraints, 0, 3, 1, 1, 100, panelEditor, layout);
		
		addComponentToGrid(csColorsEditorBackground, constraints, 1, 0, 1, 1, 30, panelEditor, layout);
		addComponentToGrid(csColorsEditorCursor, constraints, 1, 1, 1, 1, 30, panelEditor, layout);
		addComponentToGrid(csColorsEditorSelection, constraints, 1, 2, 1, 1, 30, panelEditor, layout);
		addComponentToGrid(csColorsEditorSeltext, constraints, 1, 3, 1, 1, 30, panelEditor, layout);
		
		addComponentToGrid(new JLabel("Background:"), constraints, 0, 0, 1, 1, 100, panelLinenumbers, layout);
		addComponentToGrid(new JLabel("Foreground:"), constraints, 0, 1, 1, 1, 100, panelLinenumbers, layout);
		addComponentToGrid(csColorsLinenumbersBackgr, constraints, 1, 0, 1, 1, 30, panelLinenumbers, layout);
		addComponentToGrid(csColorsLinenumbersForegr, constraints, 1, 1, 1, 1, 30, panelLinenumbers, layout);
		
		addComponentToGrid(new JLabel("Background:"), constraints, 0, 0, 1, 1, 100, panelConsole, layout);
		addComponentToGrid(csColorsConsoleBackgr, constraints, 1, 0, 1, 1, 30, panelConsole, layout);
		
		addComponentToGrid(new JLabel("Background:"), constraints, 0, 0, 1, 1, 100, panelNavigator, layout);
		addComponentToGrid(new JLabel("Selection:"), constraints, 0, 1, 1, 1, 100, panelNavigator, layout);
		addComponentToGrid(new JLabel("Selected text:"), constraints, 0, 2, 1, 1, 100, panelNavigator, layout);
		
		addComponentToGrid(csColorsNavigatorBackgr, constraints, 1, 0, 1, 1, 30, panelNavigator, layout);
		addComponentToGrid(csColorsNavigatorSelection, constraints, 1, 1, 1, 1, 30, panelNavigator, layout);
		addComponentToGrid(csColorsNavigatorSeltext, constraints, 1, 2, 1, 1, 30, panelNavigator, layout);
		
		addComponentToGrid(panelEditor, constraints, 0, 0, 1, 1, 100, editorTab, layout);
		addComponentToGrid(panelLinenumbers, constraints, 0, 1, 1, 1, 100, editorTab, layout);
		addComponentToGrid(panelConsole, constraints, 0, 2, 1, 1, 100, editorTab, layout);
		addComponentToGrid(panelNavigator, constraints, 0, 3, 1, 1, 100, editorTab, layout);
		
		// add tab into the tabbed pane
		pane.add("Colors", editorTab);
	}
	
	/**
	 * Adds component to the layout grid.
	 * 
	 * @param c Component
	 * @param constraints GridBagConstraints
	 * @param x X-coordinate in grid
	 * @param y Y-coordinate in grid
	 * @param w The number of cells in a row in the component's display area
	 * @param h The number of cells in a column in the component's display area
	 * @param weightx The weightx property
	 * @param target Container
	 * @param layout GridBagLayout
	 */
	private void addComponentToGrid(Component c, GridBagConstraints constraints, int x, int y, int w, int h, int weightx, Container target, GridBagLayout layout) {
		constraints.gridx = x;
		constraints.gridy = y;
		constraints.gridwidth = w;
		constraints.gridheight = h;
		constraints.weightx = weightx;
		layout.setConstraints(c, constraints);
		target.add(c);
	}
	
}
