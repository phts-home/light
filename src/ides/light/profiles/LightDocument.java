package ides.light.profiles;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import java.awt.Color;
import java.util.*;

import ides.light.EditorTab;
import editors.*;
import editors.highlight.*;
import utilities.*;


/**
 * <code>Light</code>'s document implemented <code>CodeEditorDocument</code> and
 * <code>DocumentHighlighter</code>.
 * 
 * @author Phil Tsarik
 *
 */
public class LightDocument implements CodeEditorDocument, DocumentHighlighter {
	
	/**
	 * Constructs a new instance of the <code>LightDocument</code> class.
	 * 
	 * @param owner <code>EditorTab</code>'s instance
	 * @param conf <code>Light</code>'s config
	 * @param type Document type to initialize
	 * @param console The <code>ProgramConsole</code> component
	 */
	public LightDocument(EditorTab owner, ProfilesConfig conf, String type, ProgramConsole console) {
		this.owner = owner;
		this.conf = conf;
		this.profile = null;
		this.console = console;
		setType(type);
	}
	
	/*
	 * @see editors.CodeEditorDocument#run()
	 */
	@Override
	public void run() {
		if (profile != null) {
			if (owner.save()) {
				profile.doRun(owner, console);
			}
		}
		
	}
	
	/*
	 * @see editors.CodeEditorDocument#compile()
	 */
	@Override
	public void compile() {
		if (profile != null) {
			if (owner.save()) {
				profile.doCompile(owner, console);
			}
		}
	}
	
	/*
	 * @see editors.CodeEditorDocument#compileAndRun()
	 */
	@Override
	public void compileAndRun() {
		if (profile != null) {
			if (owner.save()) {
				profile.doCompileAndRun(owner, console);
			}
		}
	}
	
	/*
	 * @see editors.CodeEditorDocument#isCanRun()
	 */
	@Override
	public boolean isCanRun() {
		return (profile != null) ? profile.getCanRun() : false;
	}
	
	/*
	 * @see editors.CodeEditorDocument#isCanCompile()
	 */
	@Override
	public boolean isCanCompile() {
		return (profile != null) ? profile.getCanCompile() : false;
	}
	
	/*
	 * @see editors.CodeEditorDocument#isCanCompileAndRun()
	 */
	@Override
	public boolean isCanCompileAndRun() {
		return (profile != null) ? profile.getCanCompileandrun() : false;
	}
	
	/*
	 * @see editors.CodeEditorDocument#getType()
	 */
	@Override
	public String getType() {
		return (profile != null) ? profile.getName() : "";
	}
	
	/*
	 * @see editors.CodeEditorDocument#getFirstExtension()
	 */
	@Override
	public String getFirstExtension() {
		return ((profile != null) && (profile.getExtensions() != null) && (profile.getExtensions().size() > 0)) 
			? profile.getExtensions().get(0) : "";
	}
	
	/*
	 * @see editors.CodeEditorDocument#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
		profile = conf.getProfileByDocumentType(type);
	}
	
	/*
	 * @see editors.CodeEditorDocument#setTypeByExtension(java.lang.String)
	 */
	@Override
	public void setTypeByExtension(String extension) {
		setType(conf.getTypeByFileExtension(extension));
	}
	
	/*
	 * @see editors.CodeEditorDocument#getIcon()
	 */
	@Override
	public Icon getIcon() {
		return (profile != null) ? profile.getIcon() : null;
	}
	
	/*
	 * @see editors.CodeEditorHighlighter#getDefaultAttributeSet()
	 */
	@Override
	public SimpleAttributeSet getDefaultAttributeSet() {
		if (profile == null) {
			return null;
		}
		SimpleAttributeSet set = new SimpleAttributeSet();
		try {
			StyleConstants.setForeground(set, owner.getCodeEditor().getForeground());
			StyleConstants.setBackground(set, owner.getCodeEditor().getBackground());
			StyleConstants.setFontSize(set, owner.getCodeEditor().getFont().getSize());
			StyleConstants.setFontFamily(set, owner.getCodeEditor().getFont().getFamily());
			StyleConstants.setBold(set, owner.getCodeEditor().getFont().isBold());
			StyleConstants.setItalic(set, owner.getCodeEditor().getFont().isItalic());
		}
		catch (Exception e) {
			return null;
		}
		finally {
			return set;
		}
	}
	
	/*
	 * @see editors.CodeEditorHighlighter#getHighlightedAttributeSet()
	 */
	@Override
	public SimpleAttributeSet getHighlightedAttributeSet() {
		return (profile != null) ? profile.getHighlightedAttributeSet() : null;
	}
	
	/*
	 * @see editors.highlight.DocumentHighlighter#isCaseSensitive()
	 */
	@Override
	public boolean isCaseSensitive() {
		return (profile != null) ? profile.getCaseSensitive() : false;
	}
	
	/*
	 * @see editors.CodeEditorHighlighter#getDividers()
	 */
	@Override
	public List<Character> getDividers() {
		return (profile != null) ? profile.getDividers() : null;
	}
	
	/*
	 * @see editors.CodeEditorHighlighter#getWords()
	 */
	@Override
	public List<String> getWords() {
		return (profile != null) ? profile.getWords() : null;
	}
	
	/**
	 * Gets the profile applied to this document.
	 * 
	 * @return The profile applied to this document
	 */
	public LightDocumentProfile getProfile() {
		return profile;
	}
	
	
	private EditorTab owner;
	private ProfilesConfig conf;
	private LightDocumentProfile profile;
	private ProgramConsole console;
	
}
