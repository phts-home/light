package ides.light.profiles;

import ides.light.*;
import ides.light.actions.ActionNewSomeDocument;
import ides.light.actions.ActionNewSomeDocumentInSelected;
import ides.light.actions.ActionSetDocType;
import ides.light.actions.MainFrameMenuAction;
import documents.*;
import editors.*;
import editors.highlight.*;
import utilities.*;

import java.io.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import org.w3c.dom.*;


/**
 * <code>Light</code>'s profiles config class.
 * 
 * @author Phil Tsarik
 *
 */
public class ProfilesConfig {
	
	/**
	 * Constructs a new instance of the <code>ProfilesConfig</code> class.
	 * 
	 * @param owner The program's main frame
	 * @param path Path of the XML document file
	 * @param rootName Root name of the XML document
	 */
	public ProfilesConfig(MainFrame owner, String path, String rootName) {
		profiles = new ArrayList<LightDocumentProfile>();
		newActions = new ArrayList<MainFrameMenuAction>();
		newinselActions = new ArrayList<MainFrameMenuAction>();
		settypeActions = new ArrayList<MainFrameMenuAction>();
		doc = new SimpleXMLDocument(new File(path));
		
		NodeList list = doc.getRoot().getChildNodes();
		String name;
		Element elem;
		String extensionsString;
		String caption;
		String highlightPath;
		boolean canRun;
		boolean canCompile;
		boolean canCompileandrun;
		String runMethodStr;
		String compileMethodStr;
		String compileAndRunMethodStr;
		int runMethod;
		int compileMethod;
		int compileAndRunMethod;
		String iconPath;
		Icon icon;
		// create default profile
		profiles.add(new LightDocumentProfile(this, "Plain", "Plain Document", false, false,
				false, ExProcess.MULTIPLE_ARGS_METHOD, ExProcess.MULTIPLE_ARGS_METHOD, 
				ExProcess.MULTIPLE_ARGS_METHOD, new ImageIcon("resources/icons/plain.gif"), 
				null, null, null, null, null, null, null, "", null, false, null, null/*, null*/));
		// search profiles
		for (int i = 0; i < list.getLength(); i++) {
			Node node = list.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				elem = (Element)list.item(i);
				name = doc.readStringValue(elem, "name", "");
				if (!name.equals("")) {
					ArrayList<String> extensions = new ArrayList<String>();
					ArrayList<String> runArrgs = null;
					ArrayList<String> runCommands = null;
					ArrayList<String> compileArrgs = null;
					ArrayList<String> compileCommands = null;
					ArrayList<String> compileandrunArrgs = null;
					ArrayList<String> compileandrunCommands = null;
					
					// section "run"
					runMethodStr = doc.readStringValue(doc.findElement(elem, "run"), "method", "run_multiple_args");
					if (runMethodStr.equals("run_single_arg")) {
						runMethod = ExProcess.SINGLE_ARG_METHOD;
					} else {
						if (runMethodStr.equals("open_args")) {
							runMethod = ExProcess.OPEN_ARGS_METHOD;
						} else {
							runMethod = ExProcess.MULTIPLE_ARGS_METHOD;
						}
					}
					runArrgs = doc.readStringArray(elem, "run", "argument", "@error", null);
					runCommands = doc.readStringArray(elem, "run", "command", "@error", null);
					// section "compile"
					compileMethodStr = doc.readStringValue(doc.findElement(elem, "compile"), "method", "run_multiple_args");
					if (compileMethodStr.equals("run_single_arg")) {
						compileMethod = ExProcess.SINGLE_ARG_METHOD;
					} else {
						if (compileMethodStr.equals("open_args")) {
							compileMethod = ExProcess.OPEN_ARGS_METHOD;
						} else {
							compileMethod = ExProcess.MULTIPLE_ARGS_METHOD;
						}
					}
					compileArrgs = doc.readStringArray(elem, "compile", "argument", "@error", null);
					compileCommands = doc.readStringArray(elem, "compile", "command", "@error", null);
					// section "compile and run"
					compileAndRunMethodStr = doc.readStringValue(doc.findElement(elem, "compileandrun"), "method", "run_multiple_args");
					if (compileAndRunMethodStr.equals("run_single_arg")) {
						compileAndRunMethod = ExProcess.SINGLE_ARG_METHOD;
					} else {
						if (compileAndRunMethodStr.equals("open_args")) {
							compileAndRunMethod = ExProcess.OPEN_ARGS_METHOD;
						} else {
							compileAndRunMethod = ExProcess.MULTIPLE_ARGS_METHOD;
						}
					}
					compileandrunArrgs = doc.readStringArray(elem, "compileandrun", "argument", "@error", null);
					compileandrunCommands = doc.readStringArray(elem, "compileandrun", "command", "@error", null);
					// set other properties
					extensionsString = doc.readStringValue(elem, "extensions", "");
					if (extensionsString.length() > 0) {
						if (extensionsString.charAt(0) == '.') {
							int start = 0;
							int end = 0;
							int pos = 1;
							while ( (pos = extensionsString.indexOf('.', pos)) >= 0) {
								end = pos-1;
								extensions.add(extensionsString.substring(start, end+1));
								start = pos;
								pos += 1;
							}
							end = extensionsString.length()-1;
							extensions.add(extensionsString.substring(start, end+1));
						}
					}
					highlightPath = doc.readStringValue(elem, "syntax_scheme", "");
					caption = doc.readStringValue(elem, "caption", name);
					canRun = doc.readBooleanValue(elem, "can_run", false);
					canCompile = doc.readBooleanValue(elem, "can_compile", false);
					canCompileandrun = doc.readBooleanValue(elem, "can_compileandrun", false);
					iconPath = doc.readStringValue(elem, "icon", "resources/icons/plain.gif");
					if ( (new File(iconPath)).exists() ) {
						icon = new ImageIcon(iconPath);
					} else {
						icon = new ImageIcon("resources/icons/plain.gif");
					}
					
					// reading highlight scheme file
					SimpleAttributeSet hSet = null;
					ArrayList<Character> hDev = null;
					ArrayList<String> hWords = null;
					boolean caseSensitive = false;
					File file = new File(highlightPath);
					if (file.exists()) {
						SimpleXMLDocument doc = new SimpleXMLDocument(file);
						// section "appearance"
						elem = doc.findElement(null, "appearance");
						Color bColor = doc.readColorValue(elem, "background", Color.WHITE);
						Color fColor = doc.readColorValue(elem, "foreground", Color.BLACK);
						Font font = doc.readFontValue(elem, "font", new Font("", Font.PLAIN, 14));
						caseSensitive = doc.readBooleanValue(elem, "case_sensitive", false);
						hSet = new SimpleAttributeSet();
						StyleConstants.setBackground(hSet, bColor);
						StyleConstants.setForeground(hSet, fColor);
						StyleConstants.setFontSize(hSet, font.getSize());
						StyleConstants.setFontFamily(hSet, font.getFamily());
						StyleConstants.setBold(hSet, font.isBold());
						StyleConstants.setItalic(hSet, font.isItalic());
						// section "dividers"
						hDev = doc.readCharacterArray(null, "dividers", "divider", '\0', null);
						// section "words"
						hWords = doc.readStringArray(null, "words", "word", "", null);
					}
					
					// create profile and add it to list
					LightDocumentProfile profile = new LightDocumentProfile(this, name, caption, canRun, canCompile,
							canCompileandrun, runMethod, compileMethod, compileAndRunMethod,
							icon, extensions, runArrgs, runCommands, compileArrgs,
							compileCommands, compileandrunArrgs, compileandrunCommands, highlightPath, hSet, caseSensitive, hDev, hWords/*, hRanges*/);
					profiles.add(profile);
					// add to action lists
					MainFrameMenuAction aNewDoc = new ActionNewSomeDocument(owner, caption, icon);
					newActions.add(aNewDoc);
					MainFrameMenuAction aNewinselDoc = new ActionNewSomeDocumentInSelected(owner, caption, icon);
					newinselActions.add(aNewinselDoc);
					MainFrameMenuAction aSetType = new ActionSetDocType(owner, caption, icon);
					settypeActions.add(aSetType);
				}
			}
		}
	}
	
	/**
	 * Gets document profile by specified document type.
	 * 
	 * @param type Document type
	 * @return The document profile
	 */
	public LightDocumentProfile getProfileByDocumentType(String type) {
		for (int i = 0; i < profiles.size(); i++) {
			LightDocumentProfile profile = profiles.get(i);
			if (profile.getName().equals(type)) {
				return profile;
			}
		}
		return null;
	}
	
	/**
	 * Gets document type by specified filename extension.
	 * 
	 * @param ext Filename extension
	 * @return The document type
	 */
	public String getTypeByFileExtension(String ext) {
		for (int i = 0; i < profiles.size(); i++) {
			LightDocumentProfile profile = profiles.get(i);
			List<String> extensions = profile.getExtensions();
			if (extensions == null) {
				continue;
			}
			for (int n = 0; n < extensions.size(); n++) {
				if (extensions.get(n).equalsIgnoreCase(ext)) {
					return profile.getName();
				}
			}
		}
		return profiles.get(0).getName();
	}
	
	/**
	 * Gets document type by specified menu caption.
	 * 
	 * @param caption Menu caption
	 * @return The document type
	 */
	public String getTypeByMenuCaption(String caption) {
		for (int i = 0; i < profiles.size(); i++) {
			LightDocumentProfile profile = profiles.get(i);
			if (profile.getCaption().equals(caption)) {
				return profile.getName();
			}
		}
		return profiles.get(0).getName();
	}
	
	/**
	 * Gets all document types from config file.
	 * 
	 * @return Array of document types
	 */
	public ArrayList <LightDocumentProfile> getProfiles() {
		return profiles;
	}
	
	/**
	 * Gets all actions which create new documents.
	 * 
	 * @return Array of actions
	 */
	public ArrayList <MainFrameMenuAction> getNewActions() {
		return newActions;
	}
	
	/**
	 * Gets all actions which create new documents is selected tab.
	 * 
	 * @return Array of actions
	 */
	public ArrayList <MainFrameMenuAction> getNewInSelActions() {
		return newinselActions;
	}
	
	/**
	 * Gets all actions which set document types.
	 * 
	 * @return Array of actions
	 */
	public ArrayList <MainFrameMenuAction> getSettypeActions() {
		return settypeActions;
	}
	
	/**
	 * Gets XML document as <code>SimpleXMLDocument</code> associated with config file.
	 * 
	 * @return XML document
	 */
	public SimpleXMLDocument getXMLDocument() {
		return doc;
	}
	
	/**
	 * Finds and replaces in the array items all tokens such as "fpath" "fname" "name" ets. by current values.
	 * 
	 * @param arr String array to transform.
	 * @return Transformed array.
	 */
	public List<String> transform(List<String> arr, File file) {
		List<String> newArr = new ArrayList<String>();
		if (arr == null) {
			return null;
		}
		String drive = "";
		String fpath = file.getAbsolutePath();
		String fname = file.getName();
		String name = fname;
		if (fname.indexOf(".") >= 0) {
			name = fname.substring(0, fname.indexOf("."));
		}
		if (fpath.indexOf(":") != -1) {
			drive = fpath.substring(0, fpath.indexOf(":")+1);
		}
		String folder = file.getAbsoluteFile().getParent();
		for (int i = 0; i < arr.size(); i++) {
			String item = arr.get(i);
			item = item.replace("@drive", drive);
			item = item.replace("@fpath", fpath);
			item = item.replace("@fname", fname);
			item = item.replace("@name", name);
			item = item.replace("@folder", folder);
			newArr.add(item);
		}
		return newArr;
	}
	
	
	private SimpleXMLDocument doc;
	private ArrayList<LightDocumentProfile> profiles;
	private ArrayList<MainFrameMenuAction> newActions;
	private ArrayList<MainFrameMenuAction> newinselActions;
	private ArrayList<MainFrameMenuAction> settypeActions;
	
}

