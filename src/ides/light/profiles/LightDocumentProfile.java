package ides.light.profiles;

import ides.light.EditorTab;

import java.util.*;

import javax.swing.*;
import javax.swing.text.*;

import utilities.*;


/**
 * <code>LightDocument</code>'s profile class.
 * 
 * @author Phil Tsarik
 *
 */
public class LightDocumentProfile {
	
	/**
	 * Constructs a new instance of the <code>LightDocumentProfile</code> class.
	 * 
	 * @param name <code>name</code> property
	 * @param caption <code>caption</code> property
	 * @param canRun <code>canRun</code> property
	 * @param canCompile <code>canCompile</code> property
	 * @param canCompileandrun <code>canCompileandrun</code> property
	 * @param runMethod <code>simpleRunMethod</code> property
	 * @param compileMethod <code>compileMethod</code> property
	 * @param compileAndRunMethod <code>compileAndRunMethod</code> property
	 * @param icon <code>icon</code> property
	 * @param extensions <code>extensions</code> property
	 * @param runArgs <code>runArgs</code> property
	 * @param runCommands <code>runCommands</code> property
	 * @param compileArgs <code>compileArgs</code> property
	 * @param compileCommands <code>compileCommands</code> property
	 * @param compileandrunArgs <code>compileandrunArgs</code> property
	 * @param compileandrunCommands <code>compileandrunCommands</code> property
	 * @param highlightPath <code>highlightPath</code> property
	 * @param highlightedAttributeSet <code>highlightedAttributeSet</code> property
	 * @param caseSensitive <code>caseSensitive</code> property
	 * @param highlightDeviders <code>highlightDeviders</code> property
	 * @param highlightedWords <code>highlightedWords</code> property
	 */
	public LightDocumentProfile(ProfilesConfig conf, String name, String caption, boolean canRun,
			boolean canCompile, boolean canCompileandrun,
			int runMethod, int compileMethod, int compileAndRunMethod,
			Icon icon, List<String> extensions, List<String> runArgs,
			List<String> runCommands, List<String> compileArgs, 
			List<String> compileCommands, List<String> compileandrunArgs,
			List<String> compileandrunCommands, String highlightPath,
			SimpleAttributeSet highlightedAttributeSet, boolean caseSensitive,
			List<Character> highlightDeviders, List<String> highlightedWords) {
		this.conf = conf;
		this.name = name;
		this.caption = caption;
		this.canRun = canRun;
		this.canCompile = canCompile;
		this.canCompileandrun = canCompileandrun;
		this.runMethod = runMethod;
		this.compileMethod = compileMethod;
		this.compileAndRunMethod = compileAndRunMethod;
		this.icon = icon;
		this.extensions = extensions;
		this.runArgs = runArgs;
		this.runCommands = runCommands;
		this.compileArgs = compileArgs;
		this.compileCommands = compileCommands;
		this.compileandrunArgs = compileandrunArgs;
		this.compileandrunCommands = compileandrunCommands;
		this.highlightPath = highlightPath;
		this.highlightedAttributeSet = highlightedAttributeSet;
		this.caseSensitive = caseSensitive;
		this.highlightDeviders = highlightDeviders;
		this.highlightedWords = highlightedWords;
	}
	
	/**
	 * Gets the <code>name</code> property.
	 * 
	 * @return The <code>name</code> property
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets the <code>caption</code> property.
	 * 
	 * @return The <code>caption</code> property
	 */
	public String getCaption() {
		return caption;
	}
	
	/**
	 * Gets the <code>canRun</code> property.
	 * 
	 * @return The <code>canRun</code> property
	 */
	public boolean getCanRun() {
		return canRun;
	}
	
	/**
	 * Gets the <code>canCompile</code> property.
	 * 
	 * @return The <code>canCompile</code> property
	 */
	public boolean getCanCompile() {
		return canCompile;
	}
	
	/**
	 * Gets the <code>canCompileandrun</code> property.
	 * 
	 * @return The <code>canCompileandrun</code> property
	 */
	public boolean getCanCompileandrun() {
		return canCompileandrun;
	}
	
	/**
	 * Gets the <code>icon</code> property.
	 * 
	 * @return The <code>icon</code> property
	 */
	public Icon getIcon() {
		return icon;
	}
	
	/**
	 * Gets the <code>extensions</code> property.
	 * 
	 * @return The <code>extensions</code> property
	 */
	public List<String> getExtensions() {
		return extensions;
	}
	
	/**
	 * Gets the <code>runArgs</code> property.
	 * 
	 * @return The <code>runArgs</code> property
	 */
	public List<String> getRunArgs() {
		return runArgs;
	}
	
	/**
	 * Gets the <code>runCommands</code> property.
	 * 
	 * @return The <code>runCommands</code> property
	 */
	public List<String> getRunCommands() {
		return runCommands;
	}
	
	/**
	 * Gets the <code>compileArgs</code> property.
	 * 
	 * @return The <code>compileArgs</code> property
	 */
	public List<String> getCompileArgs() {
		return compileArgs;
	}
	
	/**
	 * Gets the <code>compileCommands</code> property.
	 * 
	 * @return The <code>compileCommands</code> property
	 */
	public List<String> getCompileCommands() {
		return compileCommands;
	}
	
	/**
	 * Gets the <code>compileandrunArgs</code> property.
	 * 
	 * @return The <code>compileandrunArgs</code> property
	 */
	public List<String> getCompileandrunArgs() {
		return compileandrunArgs;
	}
	
	/**
	 * Gets the <code>compileandrunCommands</code> property.
	 * 
	 * @return The <code>compileandrunCommands</code> property
	 */
	public List<String> getCompileandrunCommands() {
		return compileandrunCommands;
	}
	
	/**
	 * Runs the document.
	 * 
	 * @param tab Document's owner
	 * @param console The <code>ProgramConsole</code> component
	 */
	public void doRun(EditorTab tab, ProgramConsole console) {
		if (canRun) {
			console.write("Runing \""+tab.getOpenedFile().getAbsolutePath()+"\"...\n");
			ExProcess proc = new ExProcess(conf.transform(runArgs, tab.getOpenedFile()), console, runMethod);
			if (runMethod != ExProcess.OPEN_ARGS_METHOD) {
				proc.call(conf.transform(runCommands, tab.getOpenedFile()));
			}
			if (proc.isError()) {
				console.write("ERROR: Unable to create process\n\n");
			} else {
				console.write("Done\n\n");
			}
		}
	}
	
	/**
	 * Compiles the document.
	 * 
	 * @param tab Document's owner
	 * @param console The <code>ProgramConsole</code> component
	 */
	public void doCompile(EditorTab tab, ProgramConsole console) {
		if (canCompile) {
			console.write("Compiling \""+tab.getOpenedFile().getAbsolutePath()+"\"...\n");
			ExProcess proc = new ExProcess(conf.transform(compileArgs, tab.getOpenedFile()), console, compileMethod);
			if (compileMethod != ExProcess.OPEN_ARGS_METHOD) {
				proc.call(conf.transform(compileCommands, tab.getOpenedFile()));
			}
			if (proc.isError()) {
				console.write("ERROR: Unable to create process\n\n");
			} else {
				console.write("Done\n\n");
			}
		}
	}
	
	/**
	 * Compiles and runs the document.
	 * 
	 * @param tab Document's owner
	 * @param console The <code>ProgramConsole</code> component
	 */
	public void doCompileAndRun(EditorTab tab, ProgramConsole console) {
		if (canCompileandrun) {
			console.write("Compiling and run \""+tab.getOpenedFile().getAbsolutePath()+"\"...\n");
			ExProcess proc = new ExProcess(conf.transform(compileandrunArgs, tab.getOpenedFile()), console, compileAndRunMethod);
			if (compileAndRunMethod != ExProcess.OPEN_ARGS_METHOD) {
				proc.call(conf.transform(compileandrunCommands, tab.getOpenedFile()));
			}
			if (proc.isError()) {
				console.write("ERROR: Unable to create process\n\n");
			} else {
				console.write("Done\n\n");
			}
		}
	}
	
	/**
	 * Gets the <code>highlightPath</code> property.
	 * 
	 * @return The <code>highlightPath</code> property
	 */
	public String getHighlightPath() {
		return highlightPath;
	}
	
	/**
	 * Gets the <code>caseSensitive</code> property.
	 * 
	 * @return The <code>caseSensitive</code> property
	 */
	public boolean getCaseSensitive() {
		return caseSensitive;
	}
	
	/**
	 * Gets the <code>highlightedAttributeSet</code> property.
	 * 
	 * @return The <code>highlightedAttributeSet</code> property
	 */
	public SimpleAttributeSet getHighlightedAttributeSet() {
		return highlightedAttributeSet;
	}
	
	/**
	 * Gets dividers to provide text highlight.
	 * 
	 * @return The dividers list
	 */
	public List<Character> getDividers() {
		return highlightDeviders;
	}
	
	/**
	 * Gets words to highlight to provide text highlight.
	 * 
	 * @return The HighlightedWord list
	 */
	public List<String> getWords() {
		return highlightedWords;
	}
	
	
	private ProfilesConfig conf;
	private String name;
	private String caption;
	private boolean canRun;
	private boolean canCompile;
	private boolean canCompileandrun;
	private int runMethod;
	private int compileMethod;
	private int compileAndRunMethod;
	private Icon icon;
	private List<String> extensions;
	private List<String> runArgs;
	private List<String> runCommands;
	private List<String> compileArgs;
	private List<String> compileCommands;
	private List<String> compileandrunArgs;
	private List<String> compileandrunCommands;
	private String highlightPath;
	private SimpleAttributeSet highlightedAttributeSet;
	private boolean caseSensitive;
	private List<Character> highlightDeviders;
	private List<String> highlightedWords;
	
}

