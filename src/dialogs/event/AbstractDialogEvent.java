package dialogs.event;

import dialogs.*;


/**
 * Event for an abstract dialog.
 * 
 * @author Phil Tsarik
 * @version 1.0.0
 *
 */
public class AbstractDialogEvent {
	
	public static final int OK_BUTTON = 0;
	public static final int CANCEL_BUTTON = 1;
	public static final int APPLY_BUTTON = 2;
	
	/**
	 * Constructs a new instance of the <code>DialogEvent</code> class.
	 * 
	 * @param dialog Abstract dialog
	 * @param button Button which has been pressed
	 */
	public AbstractDialogEvent(AbstractDialog dialog, int button) {
		this.dialog = dialog;
		this.button = button;
	}
	
	/**
	 * Gets dialog.
	 * 
	 * @return Dialog
	 */
	public AbstractDialog getDialog() {
		return dialog;
	}
	
	/**
	 * Gets the pressed button.
	 * 
	 * @return The pressed button
	 */
	public int getButton() {
		return button;
	}
	
	protected AbstractDialog dialog;
	protected int button;
	
}
