package dialogs.event;

import dialogs.*;


/**
 * Event for the FontChooser dialog.
 * 
 * @author Phil Tsarik
 * @version 1.0.0
 *
 */
public class FontChooserEvent extends AbstractDialogEvent {
	
	/**
	 * Constructs a new instance of the <code>FontChooserEvent</code> class.
	 * 
	 * @param dialog FontChooser dialog
	 * @param button Button which has been pressed
	 */
	public FontChooserEvent(FontChooser dialog, int button) {
		super(dialog, button);
		this.dialog = dialog;
		this.button = button;
	}
	
	/**
	 * Gets FontChooser dialog.
	 * 
	 * @return Dialog
	 */
	public FontChooser getDialog() {
		return (FontChooser)dialog;
	}
	
}
