package dialogs.event;

import java.util.EventListener;


/**
 * Listener for the FontChooser dialog which provides to do some actions when
 * "Ok", "Cancel" or "Apply" button is pressed.
 * 
 * @author Phil Tsarik
 *
 */
public interface FontChooserListener extends EventListener {
	
	/**
	 * Invokes when some dialog button has been pressed.
	 * 
	 * @param evt FontChooser dialog event
	 */
	public void buttonPressed(FontChooserEvent evt);
	
}
