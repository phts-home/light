package dialogs;

import dialogs.event.*;

import java.awt.*;

import javax.swing.event.EventListenerList;


/**
 * The font chooser dialog.
 * 
 * @author Phil Tsarik
 * @version 1.0.0
 *
 */
public class FontChooser extends AbstractFontChooser {
	
	/**
	 * Constructs a font chooser.
	 * 
	 * @param owner The dialog's owner
	 * @param title Dialog title
	 * @param imageIcon Dialog icon
	 */
	public FontChooser(Frame owner, String title, Image imageIcon) {
		super(owner, title, imageIcon, 400, 300);
		fontChooserListenerList = new EventListenerList();
		super.addAbstractDialogListener(new AbstractDialogListener() {
			@Override
			public void buttonPressed(AbstractDialogEvent evt) {
				fireFontChooserButtonPressed((FontChooser)evt.getDialog(), evt.getButton());
			}
		});
	}
	
	/**
	 * Adds the given observer to begin receiving notifications when font chooser's buttons were pressed.
	 * 
	 * @param l The observer to register
	 */
	public void addFontChooserListener(FontChooserListener l) {
		fontChooserListenerList.add(FontChooserListener.class, l);
	}
	
	/**
	 * Unregisters the given observer so it will no longer receive events.
	 * 
	 * @param l The observer to unregister
	 */
	public void removeFontChooserListener(FontChooserListener l) {
		fontChooserListenerList.remove(FontChooserListener.class, l);
	}
	
	/**
	 * Gets all listeners that have registered.
	 * 
	 * @return all listeners that have registered
	 */
	public FontChooserListener[] getFontChooserListeners() {
		return fontChooserListenerList.getListeners(FontChooserListener.class);
	}
	
	/**
	 * Gets selected font.
	 * 
	 * @return The selected font
	 */
	public Font getSelectedFont() {
		return font;
	}
	
	/**
	 * Gets selected font color.
	 * 
	 * @return The selected font color
	 */
	public Color getSelectedFontColor() {
		return fontColor;
	}
	
	/**
	 * Shows the dialog.
	 * 
	 * @param font Initial font value
	 * @param fontColor Initial font color value
	 */
	public void showFontDialog(Font font, Color fontColor) {
		this.font = font;
		this.fontColor = fontColor;
		setVisible(true);
	}
	
	
	@Override
	protected void apply() {
		int style = 0;
		switch (comboboxStyle.getSelectedIndex()) {
		case 0:
			style = Font.PLAIN;
			break;
		case 1:
			style = Font.BOLD;
			break;
		case 2:
			style = Font.ITALIC;
			break;
		case 3:
			style = Font.BOLD | Font.ITALIC;;
			break;
		}
		font = new Font((String)comboboxFonts.getSelectedItem(), style, (Integer)spSize.getValue());
		fontColor = csForegr.getSelectedColor();
	}
	
	@Override
	protected void refresh() {
		if (font == null) {
			return;
		}
		comboboxFonts.setSelectedItem(font.getFamily());
		if (font.isBold()) {
			if (font.isItalic()) {
				comboboxStyle.setSelectedItem("Bold Italic");
			} else {
				comboboxStyle.setSelectedItem("Bold");
			}
		} else {
			if (font.isItalic()) {
				comboboxStyle.setSelectedItem("Italic");
			} else {
				comboboxStyle.setSelectedItem("Plain");
			}
		}
		spSize.setValue(font.getSize());
		csForegr.setSelectedColor(fontColor);
	}
	
	
	private EventListenerList fontChooserListenerList;
	private Font font;
	private Color fontColor;
	
	
	private void fireFontChooserButtonPressed(FontChooser dialog, int button) {
		// guaranteed to return a non-null array
		Object[] listeners = fontChooserListenerList.getListenerList();
		for (Object i : listeners) {
			if (i instanceof FontChooserListener) {
				((FontChooserListener)i).buttonPressed(new FontChooserEvent(dialog, button));
			}
		}
	}
	
}
