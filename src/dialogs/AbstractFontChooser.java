package dialogs;

import java.awt.*;
import javax.swing.*;

import components.ColorSelector;



/**
 * Abstract font chooser dialog.
 * 
 * @author Phil Tsarik
 * @version 1.0.0
 *
 */
public abstract class AbstractFontChooser extends AbstractDialog {
	
	/**
	 * Constructs a font chooser.
	 * 
	 * @param owner The dialog's owner
	 * @param title Dialog title
	 * @param imageIcon Dialog icon
	 * @param w Dialog width
	 * @param h Dialog height
	 */
	public AbstractFontChooser(Frame owner, String title, Image imageIcon, int w, int h) {
		super(owner, title, imageIcon, w, h);
	}
	
	
	protected JComboBox comboboxFonts;
	protected JComboBox comboboxStyle;
	protected JSpinner spSize;
	protected ColorSelector csForegr;
	
	
	@Override
	protected Component createComponentPanel() {
		GridBagLayout layout = new GridBagLayout();
		JPanel componentPanel = new JPanel(layout);
		
		// init components
		comboboxFonts = new JComboBox();
		comboboxStyle = new JComboBox();
		spSize = new JSpinner(new SpinnerNumberModel(10, 1, 100, 1));
		csForegr = new ColorSelector();
		JLabel l1 = new JLabel("Name:");
		JLabel l2 = new JLabel("Style:");
		JLabel l3 = new JLabel("Size:");
		JLabel l4 = new JLabel("Color:");
		
		// fill Font list
		String fontList[] = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		for (int i = 0; i < fontList.length; i++)
			comboboxFonts.addItem(fontList[i]);
		
		// fill Style list
		comboboxStyle.addItem( "Plain" );
		comboboxStyle.addItem( "Bold" );
		comboboxStyle.addItem( "Italic" );
		comboboxStyle.addItem( "Bold Italic" );
		
		// create constraints
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.insets = new Insets(5, 15, 10, 15);
		
		// place components onto the panel
		addComponentToGrid(l1, constraints, 0, 0, 1, 1, 100, componentPanel, layout);
		addComponentToGrid(l2, constraints, 0, 1, 1, 1, 100, componentPanel, layout);
		addComponentToGrid(l3, constraints, 0, 2, 1, 1, 100, componentPanel, layout);
		addComponentToGrid(l4, constraints, 0, 3, 1, 1, 100, componentPanel, layout);
		
		addComponentToGrid(comboboxFonts, constraints, 1, 0, 1, 1, 10, componentPanel, layout);
		addComponentToGrid(comboboxStyle, constraints, 1, 1, 1, 1, 10, componentPanel, layout);
		addComponentToGrid(spSize, constraints, 1, 2, 1, 1, 10, componentPanel, layout);
		addComponentToGrid(csForegr, constraints, 1, 3, 1, 1, 10, componentPanel, layout);
		return componentPanel;
	}
	
	/**
	 * Adds component to the layout grid.
	 * 
	 * @param c Component
	 * @param constraints GridBagConstraints
	 * @param x X-coordinate in grid
	 * @param y Y-coordinate in grid
	 * @param w The number of cells in a row in the component's display area
	 * @param h The number of cells in a column in the component's display area
	 * @param weightx The weightx property
	 * @param target Container
	 * @param layout GridBagLayout
	 */
	private void addComponentToGrid(Component c, GridBagConstraints constraints, int x, int y, int w, int h, int weightx, Container target, GridBagLayout layout) {
		constraints.gridx = x;
		constraints.gridy = y;
		constraints.gridwidth = w;
		constraints.gridheight = h;
		constraints.weightx = weightx;
		layout.setConstraints(c, constraints);
		target.add(c);
	}
	
}
