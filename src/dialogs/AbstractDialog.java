package dialogs;

import dialogs.event.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.EventListenerList;



/**
 * Abstract dialog.
 * 
 * @author Phil Tsarik
 * @version 1.0.0
 *
 */
public abstract class AbstractDialog extends JDialog {
	
	/**
	 * Constructs a new dialog.
	 * 
	 * @param owner The dialog's owner
	 * @param title Dialog title
	 * @param imageIcon Dialog icon
	 * @param w Dialog width
	 * @param h Dialog height
	 */
	public AbstractDialog(Frame owner, String title, Image imageIcon, int w, int h) {
		super(owner, true);
		this.owner = owner;
		abstractDialogListenerList = new EventListenerList();
		// get screen size
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension scrSize = kit.getScreenSize();
		// set window size
		setSize(w, h);
		// set window position in the center of the screen
		setLocation(scrSize.width/2 - w/2, scrSize.height/2 - h/2);
		// set icon
		setIconImage(imageIcon);
		// set window title
		setTitle(title);
		// set resizable
		setResizable(false);
		// create components
		createComponents();
	}
	
	@Override
	public void show() {
		refresh();
		super.show();
	}
	
	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			refresh();
		}
		super.setVisible(visible);
	}
	
	
	protected Frame owner;
	protected EventListenerList abstractDialogListenerList;
	
	
	/**
	 * Adds the given observer to begin receiving notifications when dialog's buttons were pressed.
	 * 
	 * @param l The observer to register
	 */
	protected void addAbstractDialogListener(AbstractDialogListener l) {
		abstractDialogListenerList.add(AbstractDialogListener.class, l);
	}
	
	/**
	 * Unregisters the given observer so it will no longer receive events.
	 * 
	 * @param l The observer to unregister
	 */
	protected void removeAbstractDialogListener(AbstractDialogListener l) {
		abstractDialogListenerList.remove(AbstractDialogListener.class, l);
	}
	
	/**
	 * Gets all listeners that have registered.
	 * 
	 * @return all listeners that have registered
	 */
	protected AbstractDialogListener[] getAbstractDialogListeners() {
		return abstractDialogListenerList.getListeners(AbstractDialogListener.class);
	}
	
	/**
	 * Refreshes components on open dialog.
	 */
	protected abstract void refresh();
	
	/**
	 * Applies changes.
	 */
	protected abstract void apply();
	
	/**
	 * Creates components on the frame.
	 */
	protected void createComponents() {
		// create components panel
		Component compPanel = createComponentPanel();
		Component butPanel = createButtonPanel();
		
		// place components on frame
		Container contentPane = getContentPane();
		contentPane.add(compPanel, BorderLayout.CENTER);
		contentPane.add(butPanel, BorderLayout.SOUTH);
	}
	
	/**
	 * Creates button panel.
	 * 
	 * @return Panel
	 */
	protected Component createButtonPanel() {
		JButton butOK = new JButton("OK");
		butOK.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				apply();
				fireButtonPressed(AbstractDialog.this, AbstractDialogEvent.OK_BUTTON);
				AbstractDialog.this.dispose();
			}
		});
		JButton butCancel = new JButton("Cancel");
		butCancel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				fireButtonPressed(AbstractDialog.this, AbstractDialogEvent.CANCEL_BUTTON);
				AbstractDialog.this.dispose();
			}
		});
		JButton butApply = new JButton("Apply");
		butApply.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				apply();
				fireButtonPressed(AbstractDialog.this, AbstractDialogEvent.APPLY_BUTTON);
			}
		});
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.add(butOK);
		buttonsPanel.add(butCancel);
		buttonsPanel.add(butApply);
		return buttonsPanel;
	}
	
	/**
	 * Creates component panel.
	 * 
	 * @return Panel
	 */
	protected abstract Component createComponentPanel();
	
	/**
	 * Notifies all listeners that have registered interest for notification on
	 * this event type.
	 */
	protected void fireButtonPressed(AbstractDialog dialog, int button) {
		// guaranteed to return a non-null array
		Object[] listeners = abstractDialogListenerList.getListenerList();
		for (Object i : listeners) {
			if (i instanceof AbstractDialogListener) {
				((AbstractDialogListener)i).buttonPressed(new AbstractDialogEvent(dialog, button));
			}
		}
	}
	
}
