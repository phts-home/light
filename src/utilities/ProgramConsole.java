package utilities;


/**
 * Interface for the program console.
 * 
 * @author Phil Tsarik
 *
 */
public interface ProgramConsole {
	
	/**
	 * Writes text to the console.
	 * 
	 * @param line Text to write
	 */
	public void write(String line);
	
}
