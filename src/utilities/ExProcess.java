package utilities;

import java.awt.*;
import java.io.*;
import java.util.List;


/**
 * <code>ExProcess</code> class.<br><br>
 * 
 * @author Phil Tsarik
 * @version 0.8.0
 *
 */
public class ExProcess {
	
	/**
	 * determines that a process should be created by 
	 * <code>Runtime.getRuntime().exec(arguments.get(0))</code> and it can have only one argument.
	 */
	public final static int SINGLE_ARG_METHOD = 1;
	
	/**
	 * determines that a process should be created 
	 * by <code>new ProcessBuilder(arguments).start()</code>.
	 */
	public final static int MULTIPLE_ARGS_METHOD = 2;
	
	/**
	 * determines that files given in <code>arguments</code> should 
	 * be opened with default application associated with this file type.
	 */
	public final static int OPEN_ARGS_METHOD = 3;
	
	/**
	 * Constructs a new instance of the <code>ExProcess</code> class.
	 * 
	 * @param arguments Process arguments
	 * @param console The <code>ProgramConsole</code> component
	 * @param method The process creation method 
	 */
	public ExProcess(List<String> arguments, ProgramConsole console, int method) {
		if ( (arguments == null) || (arguments.size() == 0) ) {
			return;
		}
		this.error = false;
		switch (method) {
		case SINGLE_ARG_METHOD:
			try {
				proc = Runtime.getRuntime().exec(arguments.get(0));
			}
			catch (Exception e) {
				error = true;
			}
			if (proc == null) {
				error = true;
				return;
			}
			break;
		case MULTIPLE_ARGS_METHOD:
			try {
				proc = new ProcessBuilder(arguments).start();
			}
			catch(Exception e) {
				error = true;
			}
			if (proc == null) {
				error = true;
				return;
			}
			break;
		case OPEN_ARGS_METHOD:
			Desktop desktop = Desktop.getDesktop();
			for (int i = 0; i < arguments.size(); i++) {
				try {
					desktop.open(new File(arguments.get(i)));
				}
				catch (Exception e) {
					error = true;
				}
			}
			break;
		}
		this.inputStream = proc.getInputStream();
		this.outputStream = proc.getOutputStream();
		this.errorStream = proc.getErrorStream();
		if (console != null) {
			new Spooler(inputStream, console).start();
			new Spooler(errorStream, console).start();
		}
	}
	
	/**
	 * Calls some commands.
	 * 
	 * @param commands Commands to call
	 */
	public void call(List<String> commands) {
		if (proc == null) {
			error = true;
			return;
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(this.outputStream));
		for(int i = 0; i < commands.size(); i++) {
			try {
				writer.write(commands.get(i));
				writer.newLine();
				writer.flush();
			}
			catch(IOException e) {}
		}
	}
	
	/**
	 * Tests whether an error is occupied.
	 * 
	 * @return <code>true</code> if an error is occupied; <code>false</code> otherwise
	 */
	public boolean isError() {
		return error;
	}
	
	
	private InputStream inputStream;
	private OutputStream outputStream;
	private InputStream errorStream;
	private Process proc;
	private boolean error;
	
	
	private static class Spooler extends Thread {
		public Spooler(InputStream in, ProgramConsole console) {
			super("SpoolerThread");
			this.in = in;
			this.console = console;
		}
		@Override
		public void run() {
			Reader reader = new InputStreamReader(in);
			char[] buffer = new char[1024];
			int read;
			try {
				while( (read = reader.read(buffer)) != -1) {
					console.write(new String(buffer, 0, read));
				}
			}
			catch(IOException e) {
			}
		}
		private InputStream in;
		private ProgramConsole console;
	}
	
}

