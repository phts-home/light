
Light

v1.0

================================================================================

Thank you for using Light. We hope you enjoy this program. :)

================================================================================

1)  About Program
2)  System Requirements
3)  Contact Information

================================================================================

1)  About Program
-----------------

  This program is advanced text editor which aimed at work with source files of 
various programming languages, web-pages etc.
  The main feature is that you can add and edit your own document profiles to 
adapt the program functional to your needs.
  Also not the least of the facts is that this program is written on Java and so
it does not depend on the platform when you execute the program.

================================================================================

2)  System Requirements
-----------------------

  -  Java Runtime Environment 1.6
  -  Intel� Pentium�/Celeron�/Xeon�, AMD K6/Athlon�/Duron� 400 MHz or higher
  -  64 MB RAM

================================================================================

3)  Contact Information
-----------------------

  Program authors are Phil Tsarik and Sergey Tuchina.
  To contact them over e-mail: philip-s@yandex.ru and lazynov@yandex.ru

================================================================================

Light � 2008 by Phil Tsarik and Sergey Tuchina
